// Jenkinsfile for Hiring Portal Backend
def COLOR_MAP = [
    'SUCCESS': 'good',
    'FAILURE': 'danger',
    'UNSTABLE': 'warning'
]

def MESSAGES_MAP = [
    'SUCCESS': "success",
    'FAILURE': "failed",
    'UNSTABLE': "unstable"
]

pipeline {
    agent any
    options {
        // fail build if takes more than 30 minutes, mark reason as build timeout
        timeout(time: 10, unit: 'MINUTES')
        // Keep the 10 most recent builds
        buildDiscarder(logRotator(numToKeepStr: '10'))
        // Don't run any stage if found to be unstable
        skipStagesAfterUnstable()
    }
    environment {
        DISPLAY_SERVICE_NAME    = 'Nickelfox Hiring Portal Backend'
        BRANCH_NAME             = sh(script: "echo ${env.BRANCH_NAME}", returnStdout: true).trim()
    }
    stages {
        stage('SCM Checkout') {
            steps {
                checkout scm
            }
        }
        stage("Environment Variables") {
            steps {
                sh "printenv"
            }
        }
        stage('Deploy to Digital Ocean') {
            parallel {
                stage('Deploying to Environment') {
                    when {
                        expression { return BRANCH_NAME == 'develop' || BRANCH_NAME == 'qa' }
                    }
                    steps {
                        sshagent(credentials : ['nhp-backend-digital-ocean']) {
                            sh 'ssh -o StrictHostKeyChecking=no ubuntu@nhp.foxlabs.in uptime'
                            sh """ ssh ubuntu@nhp.foxlabs.in "cd projects/hiring-portal/${BRANCH_NAME}/; git pull origin ${BRANCH_NAME} && ./build.sh" """
                        }
                    }
                }
                stage('Deploying to Master Environment') {
                    when {
                        expression { return BRANCH_NAME == 'master' }
                    }
                    steps {
                        sshagent(credentials : ['nhp-backend-master-digital-ocean']) {
                            sh 'ssh -o StrictHostKeyChecking=no ubuntu@134.209.147.53 uptime'
                            sh """ ssh ubuntu@134.209.147.53 "cd projects/hiring-portal/${BRANCH_NAME}/; git pull origin ${BRANCH_NAME} && ./build.sh" """
                        }
                    }
                }
            }
        }
    }
}