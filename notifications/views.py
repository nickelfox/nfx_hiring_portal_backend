from rest_framework import status, views, generics, filters, parsers
from rest_framework.response import Response

from apps.user import models as notification_models
from notifications import serializers as notification_serializer, service, notification_service
from common.constants import ApplicationMessages, Constants
from common import permissions, pagination


class NotificationLogAPIView(views.APIView):
    """this is generic list api view for notification logs"""

    serializer_class = notification_serializer.NotificationLogSerializer
    model = notification_models.NotificationLog
    permission_classes = (permissions.IsExaminer | permissions.IsSuperAdmin,)

    def get(self, request, *args, **kwargs):
        """return list of notification logs"""
        queryset = self.model.notification_filter({"receiver": request.user, "sender__deleted_at__isnull": True})
        serializer = self.serializer_class(queryset, many=True).data
        return Response(serializer, status=status.HTTP_200_OK)


class NotificationMarkAsReadAPIView(views.APIView):
    """make notification mark as read, send notification id in an array and make all those notification
    mark as read = True"""

    serializer_class = notification_serializer.NotificationMarkAsReadSerializer
    model = notification_models.NotificationLog
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer, )

    def patch(self, request, *args, **kwargs):
        """check serializer if data is valid or not"""
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            service.MarkAsRead.mark_as_read(serializer.validated_data.get("logs"))
            return Response(ApplicationMessages.SUCCESS, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
