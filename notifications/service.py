from apps.user import models as user_model


class NotificationObjectCreate:
    """save notification for user"""

    @staticmethod
    def create_notification_log_object(users, title, payload, notification_type):
        # icon = IconDataset(notification_title).icons()
        for user in users:
            try:
                data = {
                    "user": user,
                    "title": title,
                    "notification_type": notification_type,
                    "payload": payload,
                    # "icon": icon
                }
                user_model.NotificationLog.create_instance(data)
            except Exception as ex:
                # here pass error because this job will run in background so it should not raise error
                pass


class MarkAsRead:
    """take id's and make mark as read all notifications"""

    @staticmethod
    def mark_as_read(data):
        user_model.NotificationLog.notification_filter({"id__in": data}).update(mark_as_read=True)
        return True
