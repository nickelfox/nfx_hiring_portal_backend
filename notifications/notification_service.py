import json
import requests
from rest_framework.exceptions import APIException
from apps.user import models as user_models
from common.constants import ApplicationMessages, Constants
from nhp import settings


class TriggerFCMNotification:
    """Main firebase push notification service, here we get the whole payload and pass into fcm url"""

    def __init__(self, payload):
        self.payload = payload
        self.url = settings.FCM_URL
        self.headers = {
            "Content-Type": "application/json",
            'Authorization': 'key=' + settings.FCM_SERVER_KEY,
        }

    def trigger_notification(self):
        try:
            response = requests.post(self.url, headers=self.headers, data=json.dumps(self.payload))
            return response
        except (
                requests.ConnectionError,
                requests.ConnectTimeout,
                requests.TooManyRedirects,
                APIException,
        ):
            return False


class PreparePayload:
    """Prepare payload with device tokens, and notification data"""

    def __init__(self, device_tokens, notification: dict, data):
        self.device_tokens = device_tokens
        self.notification = notification
        self.data = data
        self.priority = 10

    def prepare_payload(self):
        combined_message = {
            "registration_ids": self.device_tokens,
            "data": self.data,
            "priority": self.priority,
            "notification": self.notification,
        }

        return combined_message


class GenericPushNotification:
    """This class Assembles the data required to
    send notifications on Android
    """

    def __init__(self, notification_type=None, sender=None, users=None, kwargs=None):
        self.users = users
        self.kwargs = kwargs
        self.notification_type = notification_type
        self.sender = sender

    def notify(self):
        """Generic notified method to assemble the data for
        sending notifications on Android
        """
        sender_user = user_models.User.get_instance_or_none({"id": self.sender, "deleted_at__isnull": True})
        tokens = UserService.get_device_tokens(self.users)

        content = PayloadData(self.notification_type, self.kwargs).payload_content()
        payload = PreparePayload(tokens, content, self.data_payload()).prepare_payload()

        for user_id in self.users:
            payload['notification'].update(self.kwargs)

            user_models.NotificationLog.objects.create(
                sender=sender_user,
                receiver=user_models.User.objects.get(id=user_id),
                title=payload['notification']['body'],
                notification_type=Constants.NOTIFY,
                payload=payload
            )

        notified = TriggerFCMNotification(payload).trigger_notification()

        return notified

    def data_payload(self, kwargs=None):
        """Add data in payload, this is one some meta data which we need to pass while sending notification"""

        data = {}
        return data


class UserService:

    @staticmethod
    def get_device_tokens(users):
        """get users with device tokens"""
        # raise Exception(users)
        data = list(user_models.Session.objects.filter(user_id__in=users).values_list('device_token', flat=True))
        tokens = list(set(data))
        return tokens


class PayloadData:
    """ This class registers Payload for notification"""

    def __init__(self, notification_type=None, kwargs=None):
        self.kwargs = kwargs
        self.notification_type = notification_type

    def data_payload(self):
        """Get Payload Data"""

        data = {
            "category": self.notification_type,
            "action_ids": self.kwargs
        }

        return data

    def payload_content(self):
        """This function registers title & body attributes,
        which required to send specific notification
        """

        notification_content = {
            Constants.CANDIDATE_EXAM_SUBMIT: {
                "title": ApplicationMessages.CANDIDATE_EXAM_SUBMIT,
                "body": "{} has submitted the Exam  : {}".format(self.kwargs.get("candidate_name"),
                                                                 self.kwargs.get("exam_name")),
                "type": Constants.EXAM
            },
            Constants.CANDIDATE_EXAM_ATTEMPTED: {
                "title": ApplicationMessages.CANDIDATE_EXAM_ATTEMPTED,
                "body": "{} has attempted the Exam  : {}".format(self.kwargs.get("candidate_name"),
                                                                 self.kwargs.get("exam_name")),
                "type": Constants.EXAM
            },
            Constants.CANDIDATE_EXAM_DISQUALIFIED: {
                "title": ApplicationMessages.CANDIDATE_EXAM_DISQUALIFIED,
                "body": "{} has been disqualified for the Exam  : {}".format(self.kwargs.get("candidate_name"),
                                                                             self.kwargs.get("exam_name")),
                "type": Constants.EXAM
            },
            Constants.CANDIDATE_EXAM_LINK_EXPIRE: {
                "title": ApplicationMessages.CANDIDATE_EXAM_LINK_EXPIRE,
                "body": "{} has not attempted the exam. Its link has expired on {}. "
                        "Exam ID is # {}".format(self.kwargs.get("candidate_name"),
                                                 self.kwargs.get("link_expiry_time"),
                                                 self.kwargs.get("exam_id")),
                "type": Constants.EXAM
            },
            Constants.CUSTOM_NOTIFICATION: {
                "title": self.kwargs.get("title"),
                "body": self.kwargs.get("body"),
                # "image": IconDataset(self.notification_type).icons(),
                "type": self.kwargs.get("type"),
            }
        }
        return notification_content[self.notification_type]
