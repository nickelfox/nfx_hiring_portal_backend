from notifications import tasks
from common.constants import Constant


def custom_notification(request, payload_data):
    """
    Notifying Users (Designers and Customers) with Custom Notification
    """

    notification_type = Constant.CUSTOM_NOTIFICATION
    tasks.push_notification(notification_type, payload_data["receiver"], payload_data)
