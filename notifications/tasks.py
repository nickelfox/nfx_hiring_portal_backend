from rest_framework.exceptions import ValidationError
from notifications import notification_service


# @shared_task(name="push_notification")
def push_notification(notification_type: str, sender, users: list = None, kwargs=None):
    """this is the core of push notification,it will take user filters and notification_type as a
    parameter
    1. Notification type: type of notification e.g. ORDER_PENDING, ORDER_ACCEPTED etc..
    2. Users: List of user ids
    3. kwargs: Parameters e.g. order_id, order_status etc..
    # Usage: push_notification.delay('ORDER_ACCEPTED', ['6d41b591-57b5-4619-b143-11530bc4769f',
    'f2fc057c-d0e9-11eb-b8bc-0242ac130003'], {'order_id': 123, 'order_status': 'shipped'})
    """
    try:
        return notification_service.GenericPushNotification(notification_type, sender, users, kwargs).notify()
    except Exception as ex:
        raise ValidationError(ex)
