from notification.tasks import push_notifications
from common.constants import Constants

payload_data = {
    'title': 'Custom Notification Title',
    'body': 'Custom Notification Body',
    'type': Constants.JOB,
    'reciever': ['504d15e6-a7b1-4c82-9057-7a6aa1a52526']    # User UUID
}

push_notifications.custom_notification(payload_data)