from rest_framework import serializers
from apps.user import models as user_models


class NotificationLogSerializer(serializers.ModelSerializer):
    """Notification log serializer, this will return logs data for individual user"""

    class Meta:
        model = user_models.NotificationLog
        exclude = ("receiver", )


class NotificationMarkAsReadSerializer(serializers.Serializer):
    """make notification mark as read, get id in list"""

    logs = serializers.ListField(required=True, child=serializers.UUIDField())
