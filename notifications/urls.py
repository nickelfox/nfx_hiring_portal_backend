from django.urls import path, include
from notifications import views as notification_views


urlpatterns = [
    path('logs', notification_views.NotificationLogAPIView.as_view(), name="notification-list"),
    path('mark-as-read', notification_views.NotificationMarkAsReadAPIView.as_view(),
         name='mark-as-read'),
]