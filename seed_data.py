"""Data Seeding"""
import os
import django
import uuid
from django.core.management import call_command

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nhp.settings")
django.setup()

from random import randint, choices
from django_seed import Seed
from faker import Faker

fake = Faker(['en_US'])

from apps.user.models import User, Role, Department
from apps.exam.models import Exam, ExamQuestion
from apps.job.models import Job
from apps.questiontag.models import QuestionTag
from apps.tag.models import Tag
from apps.question.models import Question, QuestionAnswer
from apps.candidate.models import Candidate, CandidateExam, CandidateExamStatus, ExamStatus, CandidateAnswer

call_command('loaddata', 'fixtures/roles.json', verbosity=0)
call_command('loaddata', 'fixtures/departments.json', verbosity=0)
call_command('loaddata', 'fixtures/users.json', verbosity=0)
call_command('loaddata', 'fixtures/job.json', verbosity=0)
call_command('loaddata', 'fixtures/exam.json', verbosity=0)
call_command('loaddata', 'fixtures/tag.json', verbosity=0)
call_command('loaddata', 'fixtures/question.json', verbosity=0)
call_command('loaddata', 'fixtures/candidate.json', verbosity=0)
call_command('loaddata', 'fixtures/examstatus.json', verbosity=0)
call_command('loaddata', 'fixtures/permission.json', verbosity=0)

seeder = Seed.seeder('en_US')


# Functions for generating random data..

def random_user(x): return User.objects.order_by('?')[0]


def random_department(x): return Department.objects.order_by('?')[0]


def random_role(x): return Role.objects.order_by('?')[0]


def random_exam(x): return Exam.objects.order_by('?')[0]


def random_candidate_exam(x): return CandidateExam.objects.order_by('?')[0]


seeder.add_entity(User, 50, {
    'full_name': lambda x: fake.name(),  # Use "lambda x: " before any dynamic field for random results
    'designation': lambda x: fake.job(),
    'password': 'e160dcbe95c5dc3a0d20ea5e4318e01206bc7722d32d911cea4e6aebde48a2ea',  # nhp@123
    'department': random_department,
    'role': random_role,
    'is_active': True,
    'deleted_at': None
})

seeder.add_entity(Job, 15, {
    'job_title': lambda x: fake.job(),
    'job_level': lambda x: choices(['senior', 'junior', 'internship'])[0],
    'status': "PUBLISH",
    'experience': lambda x: fake.random_digit(),
    'department': random_department,
    # 'exam_associated': random_exam,
    'deleted_at': None
})

seeder.add_entity(Exam, 15, {
    'exam_name': lambda x: fake.sentence(),
    'experience_level': lambda x: choices(['senior', 'junior', 'internship'])[0],
    'status': "PUBLISH",
    'created_by': random_user,
    'duration': 60,
    'department': random_department,
    'deleted_at': None
})

seeder.add_entity(Question, 15, {
    'question_name': lambda x: fake.sentence(),
    'department': random_department,
    'experience_level': lambda x: choices(['senior', 'junior', 'internship'])[0],
    'question_status': "PUBLISH",
    'time_duration': 60,
    'deleted_at': None
})

seeder.add_entity(QuestionTag, 5, {
    'tag_id': lambda x: Tag.objects.order_by('?')[0],
    'deleted_at': None
})

seeder.add_entity(Candidate, 20, {
    "job": lambda x: Job.objects.order_by('?')[0],
    "experience": lambda x: fake.random_digit(),
    "examiner": lambda x: User.objects.filter(role__name='EXAMINER').order_by('?')[0],
    "user": lambda x: User.objects.filter(role__name='CANDIDATE').order_by('?')[0],
    'deleted_at': None
})

seeder.add_entity(ExamQuestion, 15, {
    'deleted_at': None
})

seeder.add_entity(QuestionAnswer, 15, {
    'answer_text': lambda x: fake.sentence(),
    'deleted_at': None
})

seeder.add_entity(CandidateExam, 15, {
    'exam_link': lambda x: fake.url(),
    'exam_remaining_time': lambda x: randint(100, 300),
    'disqualify_count': 1,
    'deleted_at': None
})

# seeder.add_entity(CandidateExamStatus, 15, {
#     'exam_status': lambda x: ExamStatus.objects.order_by('?')[0],
#     'deleted_at': None
# })

# seeder.add_entity(CandidateAnswer, 50, {
#     'question_remaining_time': lambda x: randint(100, 300),
#     'answer_status': lambda x: choices(['Correct', 'Wrong', 'Unattempted'])[0],
#     'deleted_at': None
# })

inserted_pks = seeder.execute()

# python manage.py flush --noinput && python seed_data.py
print(list(inserted_pks))
