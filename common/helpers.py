import string
import random


class RandomPassword:
    """
    Generate random password for user
    """

    def __init__(self):
        self.string_length = 8

    def generate_string(self):
        """Generate random string"""
        letter_digits = string.ascii_letters + string.digits
        return ''.join((random.choice(letter_digits) for data in range(self.string_length)))
