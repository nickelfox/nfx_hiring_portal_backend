from rest_framework.permissions import BasePermission
from django.contrib.auth.models import AnonymousUser
from common.constants import Constants, ApplicationMessages


class IsVerified(BasePermission):
    """
    Check if user is verified or not
    """

    def has_permission(self, request, view):
        """
        If user is active, it means they are authenticated then return True else return False
        :param request:
        :param view:
        :return:
        """
        if request.user.is_active:
            return True
        else:
            return False
        pass


# ACL list for methods and db mapping

class IsAnonymous(BasePermission):
    """User should be a Admin and Active, check user.role_id.name == 'ADMIN' and user.is_active"""
    """
    Object level permission also added
    Replace admin and super admin with own user permissions
    """

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser):
            return True


class IsSuperAdmin(BasePermission):
    """User should be a Admin and Active, check user.role_id.name == 'ADMIN' and user.is_active"""
    """
    Object level permission also added
    Replace admin and super admin with own user permissions
    """

    def has_permission(self, request, view):
        if (not isinstance(request.user, AnonymousUser)) and (request.user.role.name == Constants.ADMIN or
                                                              request.user.role.name == Constants.SUBADMIN) \
                and request.user.is_active:
            return True


class IsSubAdmin(BasePermission):
    """User should be a Sub-Admin and Active, check user.role_id.name == 'SUB-ADMIN' and user.is_active"""
    """
    Object level permission also added
    Replace super-admin and sub admin with own user permissions
    """

    def has_permission(self, request, view):
        if (not isinstance(request.user, AnonymousUser)) and request.user.role.name == Constants.SUBADMIN and request.user.is_active:
            return True


class IsExaminer(BasePermission):
    """User should be an Examiner and Active, check user.role_id.name == 'EXAMINER' and user.is_active"""
    """
    Object level permission also added
    Replace admin and super admin with own user permissions
    """

    def has_permission(self, request, view):
        if (not isinstance(request.user, AnonymousUser)) and request.user.role.name == Constants.EXAMINER and request.user.is_active:
            return True


class IsCandidate(BasePermission):
    """User should be a Candidate and Active, check user.role_id.name == 'CANDIDATE' and user.is_active"""
    """
    Object level permission also added
    Replace admin and super admin with own user permissions
    """

    def has_permission(self, request, view):
        if (not isinstance(request.user, AnonymousUser)) and request.user.role.name == Constants.CANDIDATE and request.user.is_active:
            return True
