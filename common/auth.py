from common import constants
from apps.user import models as user_models
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.token_blacklist.management.commands import flushexpiredtokens
from rest_framework_simplejwt.token_blacklist.models import BlacklistedToken, OutstandingToken
import socket
from common import utils
from common.constants import Constants, ApplicationMessages
from django.utils import timezone
from apps.user.serializers import RolePermissionSerializer


Users = get_user_model()


def create_user_session(device, user):
    """get data and create session"""
    try:
        access_token = OutstandingToken.objects.filter(user=user).order_by("-created_at")[0]
        user_session_info = {
            # 'ip': socket.gethostbyname(socket.gethostname()),
            'user': user,
            'token': access_token
        }
        if device:
            user_session_info['device_token'] = device.get("device_token")
            user_session_info['device_type'] = device.get("device_type")
            user_session_info['is_safari'] = device.get("is_safari")
        user_models.Session.create_instance(user_session_info)
        session_response = {
            'device_token': user_session_info['device_token'],
            'device_type': user_session_info['device_type'],
            'is_safari': user_session_info['is_safari'],
        }
        return session_response
    except Exception as ex:
        raise ValidationError(ApplicationMessages.SOMETHING_WENT_WRONG)


class EmailAuthManager:
    """This class deals with all type of users
    for Login and SignUp

    login function will return the token once user get Authenticated and they are static as well
    """
    model = Users

    @staticmethod
    def login_user_email(user_instance, password, data):
        """User login via email and password and return token"""
        if not user_instance.is_active:
            raise ValidationError(constants.ApplicationMessages.USER_NOT_ACTIVE, status.HTTP_401_UNAUTHORIZED)
        if utils.make_password(password) == user_instance.password:
            user_instance.last_login = timezone.now()
            token = user_instance.tokens()
            user_instance.save()
            # raise Exception(user_instance.id)
            user_session = create_user_session(data.get("device"), user_instance)
            response = {
                "message": ApplicationMessages.SUCCESS,
                "tokens": token,
                "email": user_instance.email,
                "id": user_instance.id,
                "full_name": user_instance.full_name,
                "role": user_instance.role.name,
                "session": user_session,

            }
            if user_instance.role.name == Constants.SUBADMIN:
                rp = user_models.RolePermission.get_instance({'user': user_instance.id})
                response["permissions"] = RolePermissionSerializer(rp).data

            if user_instance.role.name == Constants.EXAMINER:
                response["department"] = user_instance.department.id

            return response
        else:
            raise ValidationError(ApplicationMessages.INVALID_PASSWORD)


def logout_all_session(user):
    """ clear all the sessions of user
    Here also delete the all user session info model data"""
    user_models.Session.objects.filter(user=user).delete()
    OutstandingToken.objects.filter(user=user).delete()
