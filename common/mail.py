from django.core.mail import EmailMultiAlternatives
from nhp import settings
from django.template import loader
from common.constants import Constants, ApplicationMessages


class Mailable:
    """
    Base email hold the properties and send the email
    """

    def __init__(self, subject, template_file, template_params, to_mail, from_email=settings.FROM_EMAIL, files=None):
        """
        get email parameters
        :param subject:
        :param to_mail:
        :param from_email
        :param files
        """
        self.subject = subject
        self.template_file = template_file
        self.template_params = template_params
        self.to_mail = [to_mail] if isinstance(to_mail, str) else to_mail
        self.from_mail = from_email
        self.files = files

        if self.files is None:
            self.files = []

    def send(self):
        """
        This method uses EmailMultiAlternatives which is core package for using mail backend
        send email
        :return:
        """
        html_message = loader.render_to_string(self.template_file, self.template_params)
        mail = EmailMultiAlternatives(self.subject, html_message, self.from_mail, self.to_mail, headers={})
        mail.attach_alternative(html_message, "text/html")

        # attach all the files first
        for file in self.files:
            mail.attach_file(file)
        return mail.send(settings.FAIL_SILENTLY)


class ResetPasswordMail(Mailable):
    """Admin reset password using email and send check user exist or not and send email"""

    def __init__(self, params):
        """initiate the subject, email, params"""
        self.subject = ApplicationMessages.RESET_EMAIL_SUBJECT
        self.template_file = 'common/reset_email.html'
        self.template_params = {
            "user": params.get("user"),
            "name": "User",
            "url": params.get("BASE_URL") + "reset-password/" + "{}".format(params.get("reset_token"))
       }
        self.to_email = params.get('email')
        self.from_email = settings.FROM_EMAIL
        super().__init__(self.subject, self.template_file, self.template_params, self.to_email)
        obj = Mailable
        obj.send(self)
        del obj


class InviteExaminerMail(Mailable):
    """Invite Examiner using email and containing temporary password"""

    def __init__(self, params):
        """ initiate the subject, email, params """
        self.subject = ApplicationMessages.INVITE_EMAIL_SUBJECT
        self.template_file = 'common/invite_examiner.html'
        self.template_params = {
            "user": params.get("user"),
            "password": params.get("new_password"),
            "url": params.get("BASE_URL") + "login"
       }
        self.to_email = params.get('email')
        self.from_email = settings.FROM_EMAIL
        super().__init__(self.subject, self.template_file, self.template_params, self.to_email)
        obj = Mailable
        obj.send(self)
        del obj


class InviteSubAdminMail(Mailable):
    """Invite SubAdmin using email and containing temporary password"""

    def __init__(self, params):
        """ initiate the subject, email, params """
        self.subject = ApplicationMessages.INVITE_EMAIL_SUBJECT
        self.template_file = 'common/invite_subadmin.html'
        self.template_params = {
            "user": params.get("user"),
            "password": params.get("new_password"),
            "url": params.get("BASE_URL") + "login"
       }
        self.to_email = params.get('email')
        self.from_email = settings.FROM_EMAIL
        super().__init__(self.subject, self.template_file, self.template_params, self.to_email)
        obj = Mailable
        obj.send(self)
        del obj


class InviteCandidateMail(Mailable):
    """Invite Examiner using email and containing temporary password"""

    def __init__(self, params):
        """ initiate the subject, email, params """
        self.subject = ApplicationMessages.INVITE_CANDIDATE_SUBJECT
        self.template_file = 'common/invite_candidate.html'
        self.template_params = {
            "user": params.get("user"),
            "exam_link": params.get("exam_link"),
            "expiry_time": params.get("expiry_time"),
            "url": params.get("exam_link"),
            "job": params.get("job"),
            "attempt_level": params.get("attempt_level"),
            "flag": params.get("flag")
       }
        self.to_email = params.get('email')
        self.from_email = settings.FROM_EMAIL
        super().__init__(self.subject, self.template_file, self.template_params, self.to_email)
        obj = Mailable
        obj.send(self)
        del obj
