from django.contrib import admin
from apps.user import models as user_model
from common.models import Permission


admin.site.register(Permission)
admin.site.register(user_model.RolePermission)
admin.site.register(user_model.NotificationLog)
