from apps.questiontag import models as qtag_models
from apps.tag import models as tag_model
from apps.question import models as question_model
from rest_framework import status, parsers, generics, views
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from common.constants import Constants, ApplicationMessages
from apps.questiontag import serializers
from common import permissions
from django.utils import timezone


class QTagCreateListAPIVIew(generics.ListAPIView):
    """
    Render the Question-Tag list
    """
    serializer_class = serializers.QTagListSerializer
    parser_classes = (parsers.JSONParser, )
    model = qtag_models.QuestionTag
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    filter_backends = (SearchFilter, )

    search_fields = ['question_id', 'tag_id', ]

    def get_queryset(self):
        """Retrieve the queryset of all Question-Tag data """

        return self.model.filter_instance({'deleted_at__isnull': True})

    def get(self, request, *args, **kwargs):
        """
        Get the list of the Question-Tag
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(filtered_queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("Question-Tag"),
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, question_id, *args, **kwargs):

        """Tag names are provided from Front-end, if the tag name already exists,
        that tag will be associated with the particular question,
        else a new Tag will be created by that name and then it will be associated with the Question. """
        question = question_model.Question.objects.get(id=question_id, deleted_at__isnull=True)
        if question:
            serializer_class = serializers.QTagCreateSerializer
            for tag in request.data:
                new_tag, created = tag_model.Tag.objects.get_or_create(tag_name=tag['tag_name'])
                tag.update({"question_id": question_id, "tag_id": new_tag.id})
            serializer = serializer_class(data=request.data, context={"request": request}, many=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format('ID'), status=status.HTTP_400_BAD_REQUEST)


class QTagDeleteAPIView(views.APIView):
    """
    Delete a specific QuestionTag by this API
    """
    serializer_class = serializers.QTagListSerializer
    model = qtag_models.QuestionTag
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer, )

    def get_queryset(self, id=None):
        if id:
            return qtag_models.QuestionTag.filter_instance({'id': id, 'deleted_at__isnull': True})

    def delete(self, request, id, *args, **kwargs):

        """ Delete specific QuestionTag data by ID"""

        try:
            qtag = self.model.get_instance({'id': id, 'deleted_at__isnull': True})
            qtag.delete()
            return Response(ApplicationMessages.QTAG_DELETED, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DELETE_NOT_DONE, status=status.HTTP_400_BAD_REQUEST)