from django.urls import path
from apps.questiontag import views

urlpatterns = [
    path('', views.QTagCreateListAPIVIew.as_view(), name="list-question-tag"),
    path('<uuid:question_id>', views.QTagCreateListAPIVIew.as_view(), name="create-question-tag"),
    path('<uuid:id>/delete/', views.QTagDeleteAPIView.as_view(), name="delete-question-tag"),
]
