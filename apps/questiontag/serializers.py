from rest_framework import serializers
from apps.questiontag import models as qtag_models
from apps.tag import models as tag_model


class QTagListSerializer(serializers.ModelSerializer):
    """ QuestionTag List Serializer used to List all QuestionTags """
    tag = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = qtag_models.QuestionTag
        fields = '__all__'

    def get_tag(self, obj):
        if obj.tag_id:
            return obj.tag_id.tag_name


class QTagCreateSerializer(serializers.ModelSerializer):
    """ QuestionTag Create Serializer to create new QuestionTag data"""

    class Meta:
        model = qtag_models.QuestionTag
        fields = '__all__'
