from django.contrib import admin
from .models import QuestionTag

# Register your models here.
admin.site.register(QuestionTag)