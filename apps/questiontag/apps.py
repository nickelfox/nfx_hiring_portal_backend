from django.apps import AppConfig


class QuestiontagConfig(AppConfig):
    name = 'questiontag'
