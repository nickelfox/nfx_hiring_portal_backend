from django.db import models
from common.models import BaseModel
from apps.question.models import Question
from apps.tag.models import Tag
from rest_framework.exceptions import ValidationError


class QuestionTag(BaseModel):
    """Holds information about question id , answers id"""

    question_id = models.ForeignKey(Question, related_name='questiontag',on_delete=models.CASCADE)
    tag_id = models.ForeignKey(Tag, related_name='questiontag', on_delete=models.CASCADE)

    def __str__(self):
        """ String Representation of QuestionTag"""
        return "{}-{}".format(self.question_id, self.tag_id)

    class Meta:
        """ Verbose name and plural Verbose name"""
        verbose_name = "QuestionTag"
        verbose_name_plural = "QuestionTag"
        unique_together = ('question_id', 'tag_id',)
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create data in QuestionTag model """
        try:
            return QuestionTag.objects.create(**data)
        except QuestionTag.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Gets the QuestionTag data """
        try:
            return QuestionTag.objects.get(**data)
        except Tag.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Filters the QuestionTag"""
        return QuestionTag.objects.filter(**filters)



