from django.utils import timezone


class DataSet:
    """The Dataset"""

    @staticmethod
    def questiontag_data():

        """QuestionTag dataset"""

        data = {
            "id": "5ea8bbe3-9e0c-4548-94b8-ede11a1a2830",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
        }
        return data
