from apps.user import models as user_models
from apps.question import models as question_models
from apps.tag import models as tag_models
from apps.questiontag import models as qtag_models
from apps.question.tests import data as q_data
from apps.tag.tests import data as t_data
from apps.questiontag.tests import data as qt_data
from rest_framework.test import APITestCase


class QuestionModel(APITestCase):
    """Generic QuestionTag model test setup"""

    def setUp(self):
        # create department, question, tag, and question-tag

        self.department = user_models.Department.objects.create(name='HR')
        self.question = question_models.Question.objects.create(department=self.department,
                                                                **q_data.DataSet.question_data())
        self.tag = tag_models.Tag.objects.create(**t_data.DataSet.tag_data())
        self.question_tag = qtag_models.QuestionTag.objects.create(question_id=self.question, tag_id=self.tag,
                                                                   **qt_data.DataSet.questiontag_data())

    def test_create_qtag_model(self):
        """test model"""
        self.assertEqual(str(self.question_tag.id), qt_data.DataSet.questiontag_data().get("id"))

    def test_qtag_count(self):
        """test count model"""

        qt = qtag_models.QuestionTag.objects.all().count()
        self.assertEqual(qt, 1)

    def test_delete_qtag_count(self):
        """test delete count"""

        self.question_tag.delete()
        qt = qtag_models.QuestionTag.objects.all().count()
        self.assertEqual(qt, 0)
