from django.urls import path
from apps.dashboard.views import admin_views, examiner_views

urlpatterns = [
    path('values/', admin_views.DashBoardStatisticsAPIView.as_view(), name='hr-dashboard-values'),
    path('examiner/values/', examiner_views.ExaminerDashBoardStatisticsAPIView.as_view(),
         name='examiner-dashboard-values'),
    path('summary/', examiner_views.DashBoardCandidateSummaryAPIView.as_view(),
         name='candidate-dashboard-summary')
]