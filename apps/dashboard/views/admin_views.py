import datetime
from django.db.models import Q

from apps.job import models as job_models
from apps.question import models as question_models
from apps.exam import models as exam_models
from apps.user import models as user_models
from apps.candidate import models as candidate_models

from rest_framework.response import Response
from rest_framework import views, status
from common.permissions import IsSuperAdmin


class DashBoardStatisticsAPIView(views.APIView):
    """ statistics values for HR dashboard"""

    permission_classes = [IsSuperAdmin, ]

    def get(self, request, *args, **kwargs):
        """ return dashboard values"""
        data = {
            'active_jobs': job_models.Job.objects.filter(status='publish', deleted_at__isnull=True).count(),
            'all_jobs': job_models.Job.objects.filter(deleted_at__isnull=True).count(),

            'new_questions_added': question_models.Question.objects.filter
            (created_at__lte=datetime.datetime.today(),
             created_at__gt=datetime.datetime.today()-datetime.timedelta(days=15), deleted_at__isnull=True).count(),

            'new_examiners_onboarded': user_models.User.objects.filter(
                Q(role__name='EXAMINER') & Q(created_at__lte=datetime.datetime.today(),
                                             created_at__gt=datetime.datetime.today()-datetime.timedelta(days=15),
                                             deleted_at__isnull=True)).count(),

            'exams_submitted': candidate_models.CandidateExamStatus.objects.filter(
                exam_status__status='EXAM_SUBMITTED', deleted_at__isnull=True).count(),

        }

        return Response(data=data, status=status.HTTP_200_OK)




