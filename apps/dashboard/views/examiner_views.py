import datetime
from django.db.models import Q
from django.utils import timezone

from django.db.models.functions import TruncMonth, ExtractMonth, ExtractYear
from django.db.models import Count
from apps.question import models as question_models
from apps.exam import models as exam_models
from apps.user import models as user_models
from apps.candidate import models as candidate_models

from rest_framework.response import Response
from rest_framework import views, status
from common.permissions import IsExaminer, IsSuperAdmin


class ExaminerDashBoardStatisticsAPIView(views.APIView):
    """ statistics values for Examiner dashboard"""

    permission_classes = [IsExaminer, ]

    def get(self, request, *args, **kwargs):
        """ return dashboard values"""
        data = {
            'Candidates_added': candidate_models.Candidate.objects.filter(Q(created_at__lte=datetime.datetime.today(),
                                                                            created_at__gt=datetime.datetime.today() -
                                                                                           datetime.timedelta(
                                                                                               days=15),
                                                                            deleted_at__isnull=True,
                                                                            examiner=request.user)).count(),
            'all_candidates': candidate_models.Candidate.objects.filter(deleted_at__isnull=True,
                                                                        examiner=request.user).count(),

            'Active_Exams': exam_models.Exam.objects.filter(status='publish', deleted_at__isnull=True,
                                                            department=request.user.department).count(),

            'New_Questions_Added': question_models.Question.objects.filter
            (created_at__lte=datetime.datetime.today(),
             created_at__gt=datetime.datetime.today() - datetime.timedelta(days=15), deleted_at__isnull=True,
             department__id=request.user.department.id).count(),

            'Exams_Submitted': candidate_models.CandidateExamStatus.objects.filter(
                exam_status__status='EXAM_SUBMITTED', deleted_at__isnull=True, candidate_exam__candidate__examiner=request.user).count(),

        }

        return Response(data=data, status=status.HTTP_200_OK)


class DashBoardCandidateSummaryAPIView(views.APIView):
    """ Examiner can view Candidate Summary """
    permission_classes = [IsSuperAdmin | IsExaminer, ]

    def get(self, request, *args, **kwargs):
        """ get candidate summary monthly wise"""
        today = timezone.now()
        if request.user.role.name == "ADMIN":
            data = {
                'candidate summary': candidate_models.CandidateExamStatus.objects.filter(
                    Q(exam_status__status='PASSED') & Q(updated_at__year=today.year, updated_at__month=today.month)
                    & Q(candidate_exam__candidate__deleted_at__isnull=True))
                .annotate(month=ExtractMonth('updated_at'), year=ExtractYear('updated_at'))
                .values('month', 'year')
                .annotate(total=Count('candidate_exam__candidate', distinct=True))
                .values('month', 'total', 'year', )
                .order_by('updated_at__year', 'updated_at__month')

            }
        else:

            data = {
                'candidate summary': candidate_models.CandidateExamStatus.objects.filter(
                    Q(exam_status__status='PASSED') & Q(updated_at__year=today.year, updated_at__month=today.month) &
                    Q(candidate_exam__candidate__deleted_at__isnull=True) &
                    Q(candidate_exam__candidate__examiner=request.user))
                .annotate(month=ExtractMonth('updated_at'), year=ExtractYear('updated_at'))
                .values('month', 'year')
                .annotate(total=Count('candidate_exam__candidate', distinct=True))
                .values('month', 'total', 'year', )
                .order_by('updated_at__year', 'updated_at__month')

            }

        return Response(data=data, status=status.HTTP_200_OK)
