from django.urls import path
from apps.department import views


urlpatterns = [
    path('', views.DepartmentListAPIVIew.as_view(), name="list-create-department"),
    path('<uuid:id>/', views.DepartmentEditDeleteAPIVIew.as_view(), name="edit-delte-department"),
    path('<uuid:id>/undo/', views.DepartmentUndoDeleteAPIView.as_view(), name="undo-delte-department"),
]
