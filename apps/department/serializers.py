from rest_framework import serializers
from apps.user import models as user_models
from common.constants import Constants


class DepartmentListSerializer(serializers.ModelSerializer):
    """Department List Serializer"""

    class Meta:
        model = user_models.Department
        fields = '__all__'

    def update(self, instance, validated_data):
        """Update department data"""
        name = validated_data.pop("name", None)
        # if name is not None:
        #     validated_data['name'] = name.upper()
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance

    def create(self, validated_data):
        """ Department create method"""
        # validated_data['name'] = validated_data['name'].upper()
        dept, created = user_models.Department.objects.get_or_create(name=validated_data['name'])
        dept.deleted_at = None
        dept.save()
        return dept


class DepartmentSerializer(serializers.ModelSerializer):
    """Department List Serializer"""
    status = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = user_models.Department
        fields = '__all__'

    def get_status(self, obj):
        if obj.is_active:
            return Constants.ACTIVE
        else:
            return Constants.INACTIVE