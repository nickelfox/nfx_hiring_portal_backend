from apps.user import models as user_models
from rest_framework import status, parsers, generics
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from common.constants import Constants, ApplicationMessages
from apps.department import serializers
from apps.department import filters as dept_filters
from common import permissions, pagination
from django.utils import timezone

from apps.user import models as user_model
from apps.exam import models as exam_model
from apps.job import models as job_model
from apps.question import models as question_model
from django.db.models import Q


class DepartmentListAPIVIew(generics.ListAPIView):
    """
    Render the department list
    """
    serializer_class = serializers.DepartmentListSerializer
    parser_classes = (parsers.JSONParser,)
    model = user_models.Department
    pagination_class = pagination.DefaultPagination
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    filter_backends = (SearchFilter, dept_filters.DepartmentFilterByStatus)
    search_fields = ['name', ]

    def get_queryset(self):
        return self.model.objects.exclude(Q(deleted_at__isnull=False) | (Q(name__contains='SUB-ADMIN') |
                                                                         Q(name__contains='ADMIN')))

    def get(self, request, *args, **kwargs):
        """
        Get the details of the Departments
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = serializers.DepartmentSerializer(self.paginate_queryset(filtered_queryset), many=True,
                                                          context={"request": request})
            return Response(self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("Department"),
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        """Add a new department to the list"""

        serializer = self.serializer_class(data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DepartmentEditDeleteAPIVIew(generics.ListAPIView):
    """
    Render the department list
    """
    serializer_class = serializers.DepartmentListSerializer
    parser_classes = (parsers.JSONParser,)
    model = user_models.Department
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)

    def patch(self, request, id, *args, **kwargs):
        """ Update the Department """
        try:
            queryset = self.model.objects.get(id=id, deleted_at__isnull=True)
            serializer = self.serializer_class(queryset, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("Department"),
                            status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        """ Delete an exam"""
        try:
            dept = self.model.get_instance({'id': id, 'deleted_at__isnull': True})
            user_dept = user_model.User.filter_instance({'department': dept})
            user_count = user_dept.count()
            job_dept = job_model.Job.get_jobs({'department': dept})
            job_count = job_dept.count()
            exam_dept = exam_model.Exam.filter_instance({'department': dept})
            exam_count = exam_dept.count()
            question_dept = question_model.Question.filter_instance({'department': dept})
            question_count = question_dept.count()
            if user_dept or job_dept or exam_dept:
                return Response(ApplicationMessages.DEPARTMENT_DEPEND.format(user_count, job_count,
                                                                             exam_count, question_count),
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)
            dept.deleted_at = timezone.now()
            dept.is_active = False
            dept.save()
            return Response(ApplicationMessages.DEPARTMENT_DELETED, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DELETE_NOT_DONE, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self, id):
        """
        Get Department query_set
        """
        try:
            return self.model.get_instance({'id': id, 'deleted_at__isnull': True})
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.JOB_ALREADY_DELETED,
                            status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, id, *args, **kwargs):
        """
        Get the details of the Department
        :param request:
        :param id
        :param args:
        :param kwargs:
        :return:
        """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset(id=id))
            serializer = serializers.DepartmentSerializer(filtered_queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("DEPARTMENT"),
                            status=status.HTTP_400_BAD_REQUEST)


class DepartmentUndoDeleteAPIView(generics.ListAPIView):
    serializer_class = serializers.DepartmentListSerializer
    user_model = user_models.Department
    parser_class = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin, )
    pagination_class = pagination.DefaultPagination

    def patch(self, request, id, *args, **kwargs):
        """
        User details of DEPARTMENT retrieved by ID, checks whether deleted != NULL, undo the delete
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            dept_list = self.user_model.get_instance({"id": id, "deleted_at__isnull": False})
            serializer = self.serializer_class(dept_list, data=request.data, context={"request": request},
                                               partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DEPARTMENT_NOT_YET_DELETED,
                            status=status.HTTP_400_BAD_REQUEST)
