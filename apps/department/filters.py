from rest_framework.filters import BaseFilterBackend


class DepartmentFilterByName(BaseFilterBackend):
    """Filter department by its name"""

    param = "department"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(department__name__in=param)
        return queryset


class DepartmentFilterByID(BaseFilterBackend):
    """Filter department by its ID"""

    param = "department"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)

        if param:
            return queryset.filter(department__in=param)
        return queryset


class DepartmentFilterByStatus(BaseFilterBackend):
    """ Filter Department data with respect to status """

    param = "status"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.get(self.param, None)
        if param == 'Active':
            return queryset.filter(is_active=True)
        elif param == 'Inactive':
            return queryset.filter(is_active=False)
        return queryset
