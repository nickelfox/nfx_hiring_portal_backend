from django.urls import path
from apps.tag import views

urlpatterns = [
    path('', views.TagCreateListAPIVIew.as_view(), name="list-create-tag"),
]
