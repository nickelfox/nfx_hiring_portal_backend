from rest_framework import serializers
from apps.tag import models as tag_models


class TagListSerializer(serializers.ModelSerializer):
    """ Tag List Serializer used to List all Tags and create new Tag"""

    class Meta:
        model = tag_models.Tag
        fields = '__all__'
