from apps.tag import models as tag_models
from rest_framework import status, parsers, generics
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from common.constants import Constants, ApplicationMessages
from apps.tag import serializers
from common import permissions


class TagCreateListAPIVIew(generics.ListAPIView):
    """
    Render the Tag list
    """
    serializer_class = serializers.TagListSerializer
    parser_classes = (parsers.JSONParser, )
    model = tag_models.Tag
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    filter_backends = (SearchFilter, )

    search_fields = ['tag_name', ]

    def get_queryset(self):
        """Retrieve the queryset of all Tags """

        return self.model.filter_instance({'deleted_at__isnull': True})

    def get(self, request, *args, **kwargs):
        """
        Get the list of the Tag
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(filtered_queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("Tag"),
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        """Add a new tag to the list"""

        serializer = self.serializer_class(data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
