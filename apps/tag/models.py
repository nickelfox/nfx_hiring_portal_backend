from django.db import models
from common.models import BaseModel
from rest_framework.exceptions import ValidationError

# Create your models here.


class Tag(BaseModel):
    """
    Holds the name and id of different tags associated
    with each question (like: DBMS, Data Structures, java, C++ etc) (static) .
    """
    tag_name = models.CharField(max_length=255, blank=False, null=False, unique=True)

    def __str__(self):
        """
        String representation of Tag
        :return:
        """
        return self.tag_name

    class Meta:
        """
        Verbose name and verbose names
        """
        verbose_name = "Tag"
        verbose_name_plural = "Tag"
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Enter data to Tag model """
        try:
            return Tag.objects.create(**data)
        except Tag.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Gets the Tag data """
        try:
            return Tag.objects.get(**data)
        except Tag.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Filters the Tag with respected to name """
        return Tag.objects.filter(**filters)

