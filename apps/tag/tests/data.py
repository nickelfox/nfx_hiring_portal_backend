from django.utils import timezone


class DataSet:
    """The Dataset"""

    @staticmethod
    def tag_data():

        """Tag dataset"""

        data = {
            "id": "35c9c4ce-cdc8-4d3f-acab-416d5ed2dbe5",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "tag_name": "TEST TAG",
        }
        return data