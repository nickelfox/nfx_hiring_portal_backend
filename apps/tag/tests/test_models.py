from apps.tag import models as tag_models
from apps.tag.tests import data
from rest_framework.test import APITestCase


class TagModel(APITestCase):
    """Generic Tag model test setup"""

    def setUp(self):
        # create tag
        self.tag = tag_models.Tag.objects.create(**data.DataSet.tag_data())

    def test_create_tag_model(self):
        """test model"""
        self.assertEqual(str(self.tag.id), data.DataSet.tag_data().get("id"))

    def test_tag_count(self):
        """test count model"""

        tags = tag_models.Tag.objects.all().count()
        self.assertEqual(tags, 1)

    def test_delete_tag_count(self):
        """test delete count"""

        self.tag.delete()
        tags = tag_models.Tag.objects.all().count()
        self.assertEqual(tags, 0)