from django.utils import timezone
import datetime


class DataSet:
    """The Dataset"""

    @staticmethod
    def registration_data():
        """user dataset"""
        data = {
            "id": "f9d1d2c6-fc1f-4af2-8896-171f55a1ea4a",
            "full_name": "Testing Name Examiner",
            "email": "testing@nickelfox.com",
            "password": "testing@1234",
            "designation": "Testing Designation",
            "phone_number": "1111111111",
            "is_active": True,
            "is_admin": False,
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
        }
        return data

    @staticmethod
    def examiner_data():
        """user dataset"""
        data = {
            "id": "7565af11-01f5-4865-a1b3-a7e8b9b7f8ee",
            "full_name": "Testing Name Examiner",
            "email": "examiner@nickelfox.com",
            "password": "testing@1234",
            "designation": "Testing Designation",
            "phone_number": "1111111111",
            "is_active": True,
            "is_admin": False,
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
        }
        return data

    @staticmethod
    def role_data():
        """role dataset"""
        data = {
            "id": "fabbdda0-63cd-4e38-8fd9-4c49d0f80628",
            "name": "Testing ADMIN",
            "is_active": True,
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
        }
        return data

    @staticmethod
    def department_data():
        """department dataset"""
        data = {
            "id": "36b599ed-d51a-47ee-a607-4d8a7d0ae5a3",
            "name": "Testing DEPARTMENT",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
        }
        return data

    @staticmethod
    def signup_admin_data():
        """admin dataset"""
        data = {
            'id': '0f8318cd-c256-468a-9e99-9e2cf139e2d8',
            "full_name": "Testing Name Examiner",
            "email": "examiner@nickelfox.com",
            "password": "testing@1234",
            "designation": "Testing Designation",
            "phone_number": "1111111111",
            "is_active": True,
            "is_admin": True,
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
        }
        return data

    @staticmethod
    def data_invite_no_data():
        data = {}
        return data

    @staticmethod
    def data_invite_examiner_correct():
        data = {
            "full_name": "TESTING NAME",
            "email": "TESTING@nickelfox.co",
            "department": "36b599ed-d51a-47ee-a607-4d8a7d0ae5a3",
            "phone_number": "9800980098",
            "designation": "Finance Manager"
        }
        return data