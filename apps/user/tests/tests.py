import json
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from apps.user.models import User, Session, Department, Role
from common import utils, constants
from apps.user.tests import data as user_data
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken


class InviteExaminerTestCase(APITestCase):

    def setUp(self):
        self.role = Role.objects.create(name=constants.Constants.ADMIN, is_active=True)
        self.department = Department.objects.create(name='HR', is_active=True)
        self.user = User.objects.create(role=self.role, department=self.department, **user_data.DataSet.signup_admin_data())
        self.outstanding = OutstandingToken.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + self.outstanding)

    def test_examiner_invite_with_no_data_test_case(self):
        self.client.force_authenticate(user=None)
        response = self.client.post(reverse("admin-invite-examiner"), user_data.DataSet.data_invite_no_data())
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_invite_sent(self):
        response = self.client.post(reverse("admin-invite-examiner"), user_data.DataSet.data_invite_examiner_correct())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def test_unique_field_registration(self):
    #     user = User.objects.create(**RegistrationData.data_user_objects_create())
    #     response = self.client.post(reverse("signup"), RegistrationData.data_registration_model_unique())
    #
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    #     self.assertEqual(json.loads(response.content),
    #                      {"success": False, "code": 400, "error": {"code": 400, "message": [
    #                          "user with this email already exists."
    #                      ]
    #                                                                }})
    #
