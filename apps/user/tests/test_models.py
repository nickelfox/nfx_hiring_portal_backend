from apps.user import models as user_models
from apps.user.tests import data
from rest_framework.test import APITestCase
from common.constants import Constants


class UserModel(APITestCase):
    """Generic User model test setup"""

    def setUp(self):
        # create role and department
        self.role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')

    def test_create_user_model(self):
        """test model"""

        users = user_models.User.objects.create(role=self.role, department=self.department,
                                                **data.DataSet.registration_data())
        self.assertEqual(str(users.id), data.DataSet.registration_data().get("id"))

    def test_user_count(self):
        """test count model"""
        user_models.User.objects.create(role=self.role, department=self.department,
                                        **data.DataSet.registration_data())
        users = user_models.User.objects.all().count()
        self.assertEqual(users, 1)

    def test_delete_user_count(self):
        """test delete count"""
        instance = user_models.User.objects.create(role=self.role, department=self.department,
                                                   **data.DataSet.registration_data())
        instance.delete()
        users = user_models.User.objects.all().count()
        self.assertEqual(users, 0)


class RoleModel(APITestCase):
    """Generic Role model test setup"""

    def setUp(self):
        # create role and department
        self.role = user_models.Role.objects.create(**data.DataSet.role_data())

    def test_create_role_model(self):
        """test model"""

        self.assertEqual(str(self.role.id), data.DataSet.role_data().get("id"))

    def test_role_count(self):
        """test count model"""
        roles = user_models.Role.objects.all().count()
        self.assertEqual(roles, 1)

    def test_delete_role_count(self):
        """test delete count"""
        self.role.delete()
        roles = user_models.Role.objects.all().count()
        self.assertEqual(roles, 0)


class DepartmentModel(APITestCase):
    """Generic Role model test setup"""

    def setUp(self):
        # create role and department
        self.department = user_models.Department.objects.create(**data.DataSet.department_data())

    def test_create_department_model(self):
        """test model"""

        self.assertEqual(str(self.department.id), data.DataSet.department_data().get("id"))

    def test_department_count(self):
        """test count model"""
        dept = user_models.Department.objects.all().count()
        self.assertEqual(dept, 1)

    def test_delete_department_count(self):
        """test delete count"""
        self.department.delete()
        dept = user_models.Department.objects.all().count()
        self.assertEqual(dept, 0)
