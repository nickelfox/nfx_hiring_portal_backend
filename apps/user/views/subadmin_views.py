from rest_framework.response import Response
from rest_framework import views, status, parsers, generics
from apps.user import serializers, helpers
from apps.user import models as user_models
from common import utils, mail, permissions, pagination
from common.constants import Constants, ApplicationMessages
from rest_framework.filters import SearchFilter
from rest_framework import status, generics, mixins, views, parsers, filters
from apps.department import filters as department_filter
from apps.user import filters as user_filter
from django.utils import timezone


class SubAdminListAPIView(generics.ListAPIView):
    """
    The list of SUBADMIN in the portal can be viewed
    """
    serializer_class = serializers.UserSerializer
    pagination_class = pagination.DefaultPagination
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User
    permission_classes = (permissions.IsSuperAdmin,)
    filter_backends = (SearchFilter, department_filter.DepartmentFilterByID, user_filter.UserFilterByStatus)
    search_fields = ['full_name', 'designation', 'email', 'updated_at', 'department__name', ]

    def get_queryset(self):
        """ Retrieve the details of all the SUBADMIN """
        return user_models.User.filter_instance({"role__name": Constants.SUBADMIN, "deleted_at__isnull": True})

    def get(self, request, *args, **kwargs):
        """
        Get the details of the EXAMINERS
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(self.paginate_queryset(filtered_queryset), many=True,
                                               context={"request": request})
            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("ROLE"),
                            status=status.HTTP_400_BAD_REQUEST)


class SubAdminProfileAPIView(generics.ListAPIView):
    """
    SUBADMIN's profile information can be viewed, edited and deleted
    """

    serializer_class = serializers.SubAdminListSerializer
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User
    permission_classes = (permissions.IsSuperAdmin,)

    def get_queryset(self, id=None):
        """Retrieve data of SUBADMIN with the specific ID """
        return self.user_model.get_instance({"id": id, "role__name": Constants.SUBADMIN, "deleted_at__isnull": True})

    def get(self, request, id, *args, **kwargs):
        """
        Get the details of the Users with id sent from request
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            queryset = self.get_queryset(id)
            serializer = self.serializer_class(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id, *args, **kwargs):
        """
        Edit User details of SUBADMIN retrieved by ID, checks whether the role is SUBADMIN, patches the details
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """

        user_list = self.user_model.get_instance({"id": id, "role__name": Constants.SUBADMIN,
                                                  "deleted_at__isnull": True})
        serializer = serializers.AddSubAdminSerializer(user_list, data=request.data, context={"request": request},
                                                       partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        """
        Delete specific SUBADMIN details retrieved by ID
        """

        try:
            user = self.user_model.get_instance(
                {"id": id, "role__name": Constants.SUBADMIN, 'deleted_at__isnull': True})
            user.deleted_at = timezone.now()
            user.is_active = False
            user.save()

            # Permission list attached to the particular sub-admin needs to be deleted as well

            rp = user_models.RolePermission.get_instance({'user': user})
            if rp:
                rp.deleted_at = timezone.now()
                rp.save()
            return Response(ApplicationMessages.USER_DELETED, status=status.HTTP_200_OK)

        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)


class SubAdminUndoDeleteAPIView(generics.ListAPIView):
    serializer_class = serializers.SubAdminListSerializer
    user_model = user_models.User
    parser_class = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin, )
    pagination_class = pagination.DefaultPagination

    def patch(self, request, id, *args, **kwargs):
        """
        User details of SUBADMIN is retrieved by ID, checks whether the role is SUBADMIN, deleted != NULL, undo the delete
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user_list = self.user_model.get_instance({"id": id, "role__name": Constants.SUBADMIN,
                                                      "deleted_at__isnull": False})
            serializer = self.serializer_class(user_list, data=request.data, context={"request": request},
                                               partial=True)
            if serializer.is_valid():
                serializer.save()
                rp = user_models.RolePermission.get_instance({'user': user_list})
                if rp:
                    rp.deleted_at = None
                    rp.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.EXAMINER_NOT_YET_DELETED,
                            status=status.HTTP_400_BAD_REQUEST)