from rest_framework.response import Response
from rest_framework.validators import ValidationError
from rest_framework import views, status, parsers, generics
from apps.user import serializers, helpers
from apps.user import models as user_models
from common import utils, mail, permissions, auth
from common.constants import ApplicationMessages, Constants
from rest_framework.permissions import IsAuthenticated


class LoginAPIView(views.APIView):

    serializer_class = serializers.LoginSerializer

    def get_queryset(self, email):
        """ Returns the User instance if exist """
        try:
            user_instance = user_models.User.get_instance({'email': email, 'is_active': True})
            return user_instance
        except Exception as e:
            raise ValidationError("User with this Email Does not exist")

    def post(self, request, *args, **kwargs):
        """ Login as well as create a session"""

        request.data['email'] = request.data.get('email').lower()
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.is_valid():
            user_instance = self.get_queryset(request.data.get('email'))
            if not user_instance:
                return Response(ApplicationMessages.USER_NOT_ACTIVE, status=status.HTTP_401_UNAUTHORIZED)
            data = auth.EmailAuthManager.login_user_email(user_instance, password=serializer.data.get('password'),
                                                          data=serializer.validated_data)
            if data:
                return Response(data, status=status.HTTP_200_OK)
            else:
                return Response(ApplicationMessages.EMAIL_PASSWORD_INCORRECT,
                                status=status.HTTP_401_UNAUTHORIZED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangePasswordApiView(views.APIView):
    """Change password API view"""

    serializer_class = serializers.ChangePasswordSerializer
    parser_classes = (parsers.JSONParser,)
    model = user_models.User
    permission_classes = (IsAuthenticated,)

    def patch(self, request, *args, **kwargs):
        """
        Update password
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        serializer = self.serializer_class(data=request.data, context={"request": request})
        if serializer.is_valid():
            self.model.update_password(user_id=request.user.id,
                                       new_password=serializer.validated_data.get("new_password"),
                                       old_password=serializer.validated_data.get("current_password")
                                       )
            auth.logout_all_session(request.user)
            return Response(ApplicationMessages.PASSWORD_CHANGE, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogoutAPIView(views.APIView):
    """
    Logout  api view
    """
    user_model = user_models.User

    def delete(self, request, *args, **kwargs):
        """
        Delete session
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user = self.user_model.get_instance({"email": request.user.email})
            auth.logout_all_session(user)
            return Response(ApplicationMessages.LOGOUT_SUCCESSFULLY, status=status.HTTP_200_OK)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.LOGOUT_FAILED, status=status.HTTP_400_BAD_REQUEST)


class ForgotPasswordAPIView(views.APIView):
    """
    Link for Forgot password confirmation page sent via email
    """

    serializer_class = serializers.ForgotPasswordSerializer
    parser_classes = (parsers.JSONParser,)
    model = user_models.VerificationToken

    def post(self, request, *args, **kwargs):
        """
        Post request for forgot password
        User will enter email and a link will send to user email
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(ApplicationMessages.EMAIL_SENT, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordAPIView(views.APIView):
    """
    After clicking on the link from forgot password email this api view will
    reset password using token, to set new password for a user
    """

    serializer_class = serializers.ResetPasswordSerializer
    model = user_models.VerificationToken
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User

    def patch(self, request, token, *args, **kwargs):
        """
        Get email from body parameter and token from url param and reset the password
        1. Check token is used or not
        2. Check token is RESET_PASSWORD token
        3. Check if token email same as user email entered
        :param request:
        :param token:
        :param args:
        :param kwargs:
        :return:
        """

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            try:
                token_instance = self.model.get_instance({"token": token,
                                                          "token_type": Constants.RESET_PASSWORD_TOKEN,
                                                          "token_used": False})
                user = self.user_model.get_instance({"email": token_instance.user.email})

                new_password = utils.make_password(serializer.validated_data.get("new_password"))
                user.password = new_password
                token_instance.token_used = True
                token_instance.save()
                user.save()
                auth.logout_all_session(user)
                return Response(ApplicationMessages.PASSWORD_SET, status=status.HTTP_200_OK)
            except self.model.DoesNotExist:
                return Response(ApplicationMessages.DOES_NOT_EXISTS.format("User"),
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserAPIView(generics.ListAPIView):
    """ API view for edit and delete activity related to a USER """

    serializer_class = serializers.UserSerializer
    model = user_models.User
    parser_class = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin,)

    def patch(self, request, id, *args, **kwargs):
        """
        Edit User details of EXAMINER / ADMIN both retrieved by ID
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user_list = self.model.get_instance({"id": id})
            serializer = self.serializer_class(user_list, data=request.data, context={"request": request},
                                               partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("User"),
                            status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Retrieve the list of all USERS """
        return self.model.filter_instance({})

    def delete(self, request, id, *args, **kwargs):
        """
        Delete specific job opening
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            user = self.model.get_instance({"id": id})
            user.delete()
            return Response(ApplicationMessages.USER_DELETED, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)


class CreateUserAPIView(generics.ListAPIView):
    """Create a new USER"""

    serializer_class = serializers.UserSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsSubAdmin | permissions.IsAnonymous,)
    model = user_models.User

    def post(self, request, *args, **kwargs):
        """
        Add Candidate/Examiner information, by ADMIN
        """

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FilterByRoleAPIView(views.APIView):
    """
    The Users can be filtered with respect to their role_id i.e ADMIN, EXAMINER, CANDIDATE, SUB-ADMIN
    """

    serializer_class = serializers.FilterBySerializer
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User

    def get_queryset(self, role_name=None):
        """Retrieve the list of all USERS with the particular role_name """
        role = user_models.Role.get_instance({"name": role_name})
        return self.user_model.filter_instance({"role_id": role})

    def get(self, request, role_name, *args, **kwargs):
        """
        Get the details of the Users with role sent from request
        :param request:
        :param role_name:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            queryset = self.get_queryset(role_name)
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("ROLE"),
                            status=status.HTTP_400_BAD_REQUEST)
