from django.core.checks import messages
from rest_framework.response import Response
from django.utils import timezone
from rest_framework import views, status, parsers, generics
from apps.user import serializers, helpers
from apps.user import models as user_models
from common import utils, mail, permissions, pagination
from common.constants import Constants, ApplicationMessages
from rest_framework.filters import SearchFilter
from rest_framework import status, generics, mixins, views, parsers, filters
from apps.department import filters as department_filter
from apps.candidate import models as candidate_models


class ExaminerListAPIView(generics.ListAPIView):
    """
    The list of EXAMINERS in the portal can be viewed
    """
    serializer_class = serializers.UserSerializer
    pagination_class = pagination.DefaultPagination
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User
    permission_classes = (permissions.IsSuperAdmin, )
    filter_backends = (SearchFilter, department_filter.DepartmentFilterByID)
    search_fields = ['full_name', 'designation', 'email', 'phone_number', 'updated_at', 'department__name', ]

    def get_queryset(self):
        """ Retrieve queryset of all EXAMINERS """
        return user_models.User.filter_instance({"role__name": Constants.EXAMINER, "deleted_at__isnull": True})

    def get(self, request, *args, **kwargs):
        """
        Get the details of the EXAMINERS
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(self.paginate_queryset(filtered_queryset), many=True,
                                               context={"request": request})
            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("ROLE"),
                            status=status.HTTP_400_BAD_REQUEST)


class ExaminerAPIView(generics.ListAPIView):
    serializer_class = serializers.UserUpdateSerializer
    user_model = user_models.User
    parser_class = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer, )
    pagination_class = pagination.DefaultPagination

    def patch(self, request, id, *args, **kwargs):
        """
        Edit User details of EXAMINER retrieved by ID, checks whether the role is EXAMINER, patches the details
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user_list = self.user_model.get_instance({"id": id, "role__name": Constants.EXAMINER,
                                                      "deleted_at__isnull": True})
            serializer = self.serializer_class(user_list, data=request.data, context={"request": request},
                                               partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        """
        Delete specific EXAMINER details retrieved by ID
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user = self.user_model.get_instance({"id": id, "role__name": Constants.EXAMINER, 'deleted_at__isnull': True})

            # Check if there is a candidate associated with the examiner
            candidate = candidate_models.Candidate.filter_instance({'examiner': user})
            count = candidate.count()
            if candidate:
                return Response(ApplicationMessages.EXAMINER_CANDIDATE_DEPEND.format(count),
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)

            user.deleted_at = timezone.now()
            user.save()
            return Response(ApplicationMessages.USER_DELETED, status=status.HTTP_200_OK)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)


class ExaminerUndoDeleteAPIView(generics.ListAPIView):
    serializer_class = serializers.UserUpdateSerializer
    user_model = user_models.User
    parser_class = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin, )
    pagination_class = pagination.DefaultPagination

    def patch(self, request, id, *args, **kwargs):
        """
        User details of EXAMINER retrieved by ID, checks whether the role is EXAMINER, deleted != NULL, undo the delete
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user_list = self.user_model.get_instance({"id": id, "role__name": Constants.EXAMINER,
                                                      "deleted_at__isnull": False})
            serializer = self.serializer_class(user_list, data=request.data, context={"request": request},
                                               partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.EXAMINER_NOT_YET_DELETED,
                            status=status.HTTP_400_BAD_REQUEST)


class ExaminerProfileAPIView(views.APIView):

    """
    EXAMINER's profile information can be viewed and edited
    """
    serializer_class = serializers.UserSerializer
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer, )
    pagination_class = pagination.DefaultPagination

    def get_queryset(self, id=None):
        """ Retrieve the queryset of the EXAMINER with the specific ID """
        return self.user_model.get_instance({"id": id, "role__name": Constants.EXAMINER, "deleted_at__isnull": True})

    def get(self, request, id, *args, **kwargs):
        """
        Get the details of the Users with id sent from request
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        if request.user.id != id and request.user.role.name == Constants.EXAMINER:
            return Response(ApplicationMessages.NOT_AUTHORIZE.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                queryset = self.get_queryset(id)
                serializer = self.serializer_class(queryset)
                return Response(serializer.data, status=status.HTTP_200_OK)
            except self.user_model.DoesNotExist:
                return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                                status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id, *args, **kwargs):
        """
        Update the details of the Users with id sent from request
        :param request:
        :param id:
        :param args:
        :param kwargs:
        :return:
        """
        if request.user.id != id and request.user.role.name == Constants.EXAMINER:
            return Response(ApplicationMessages.NOT_AUTHORIZE.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                user_list = self.user_model.get_instance({"id": id, "deleted_at__isnull": True})
                serializer = self.serializer_class(user_list, data=request.data, context={"request": request},
                                                   partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except self.user_model.DoesNotExist:
                return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                                status=status.HTTP_400_BAD_REQUEST)

