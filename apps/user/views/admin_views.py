from rest_framework.response import Response
from rest_framework import views, status, parsers, generics
from apps.user import serializers, helpers
from apps.user import models as user_models
from common import models as common_models
from common import utils, mail, permissions, pagination
from common.constants import Constants, ApplicationMessages


class AdminProfileAPIView(views.APIView):
    """
    ADMIN's profile information can be viewed
    """
    serializer_class = serializers.UserSerializer
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User
    permission_classes = (permissions.IsSuperAdmin,)

    def get_queryset(self, id=None):
        role = user_models.Role.get_instance({"name": Constants.ADMIN})
        return self.user_model.get_instance({"id": id, "role": role, "deleted_at__isnull": True})

    def get(self, request, id, *args, **kwargs):
        """
        get the details of the Users with id sent from request
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            queryset = self.get_queryset(id)
            serializer = self.serializer_class(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.user_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("USER"),
                            status=status.HTTP_400_BAD_REQUEST)


class AdminInviteExaminerAPIView(views.APIView):
    """
    The EXAMINER and CANDIDATE can be invited for registering at the portal to provide services
    """
    serializer_class = serializers.InviteUserSerializer
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User
    permission_classes = (permissions.IsSuperAdmin, )

    def post(self, request, *args, **kwargs):
        """
        Invite EXAMINER by ADMIN
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AdminAddSubAdminAPIView(views.APIView):
    """
    The SUBADMIN details can be added via this API by an ADMIN
    """
    serializer_class = serializers.AddSubAdminSerializer
    parser_classes = (parsers.JSONParser,)
    user_model = user_models.User
    permission_classes = (permissions.IsSuperAdmin, )

    def post(self, request, *args, **kwargs):
        """
        Add details of a SUBADMIN by ADMIN
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(ApplicationMessages.SUBADMIN_NOT_ADDED, status=status.HTTP_400_BAD_REQUEST)


class PermissionListAPIVIew(generics.ListAPIView):
    """
    Create job List using job_title,  department,  job_level, status, experience and
    also check if job already exists or not
    """
    serializer_class = serializers.PermissionSerializer
    parser_classes = (parsers.JSONParser, )
    model = common_models.Permission
    permission_classes = (permissions.IsSuperAdmin,)

    search_fields = ['job_title', 'job_level', 'status', 'created_at', ]

    def get_queryset(self):
        """Retrieve queryset of all permission data """
        return self.model.filter_instance({})

    def get(self, request, *args, **kwargs):
        """
        Get the details of the PERMISSION
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(filtered_queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("PERMISSION"),
                            status=status.HTTP_400_BAD_REQUEST)
