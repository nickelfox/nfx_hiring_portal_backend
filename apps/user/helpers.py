import socket
from django.utils import timezone
from apps.user import models
from common import utils
from . import serializers


class SessionManager:
    """Session Manager class handler issuing a new session destroying user session and other stuff"""
    model = models.Session
    serializer = serializers.SessionSerializer

    def __init__(self, user: models.User, request: object):
        """Initialize the session class with the user"""
        self.user = user
        self.request = request

    def create(self):
        """Create a new session for the user"""
        tuples = {
            'session_id': utils.make_secret(str(timezone.now())),
            'user_agent': self.request.META['HTTP_USER_AGENT'],
            'ip_address': socket.gethostbyname(socket.gethostname()),
            'expiry': timezone.now() + timezone.timedelta(days=365)
        }
        return self.user.session_set.create(**tuples)

    def get(self):
        """Get a active session for the user"""

        try:
            session = self.model.objects.get(user=self.user, expiry__get=timezone.now())
            session_data = self.serializer(session).data
            return session_data
        except self.model.DoesNotExist:
            return {}
