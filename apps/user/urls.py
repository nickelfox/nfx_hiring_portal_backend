from django.urls import path, include
from apps.user.views import user_views
from apps.user.views import examiner_views
from apps.user.views import admin_views
from apps.user.views import subadmin_views
from apps.candidate.views import CandidateLoginAPIView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
import uuid

"""
Login level URLs
"""

urlpatterns = [
    path('login', user_views.LoginAPIView.as_view(), name="login"),
    path('change-password', user_views.ChangePasswordApiView.as_view(), name="change-password"),
    path('logout', user_views.LogoutAPIView.as_view(), name="logout"),
    path('forgot-password', user_views.ForgotPasswordAPIView.as_view(), name="reset-password"),
    path('reset-password/<str:token>', user_views.ResetPasswordAPIView.as_view(), name="update-password"),
    path('candidate/login', CandidateLoginAPIView.as_view(), name='candidate-login')
]

"""
JWT level URLs
"""

urlpatterns += [
    path('gettoken/', TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path('refreshtoken/', TokenRefreshView.as_view(), name="token_refresh"),
    path('verifytoken/', TokenVerifyView.as_view(), name="token_verify"),
]

"""
USER level URLs, for common functionalities like create, edit, delete and view information from user model
"""

urlpatterns += [
    path('', user_views.CreateUserAPIView.as_view(), name='user-create'),
    path('<str:id>', user_views.UserAPIView.as_view(), name='user-details'),
    path('role/<str:role_name>', user_views.FilterByRoleAPIView.as_view(), name='filter-by-role'),
]

"""
EXAMINER level URLs
"""

urlpatterns += [
    path('examiner/', include([
        path('', examiner_views.ExaminerListAPIView.as_view(), name="examiner-list-search"),
        path('<uuid:id>', examiner_views.ExaminerAPIView.as_view(), name="examiner-edit-delete"),
        path('profile/<uuid:id>', examiner_views.ExaminerProfileAPIView.as_view(), name="examiner-profile"),
        path('<uuid:id>/undo/', examiner_views.ExaminerUndoDeleteAPIView.as_view(), name="examiner-undo-delete"),
    ])),
]

"""
ADMIN level URLs
"""

urlpatterns += [
    path('admin/', include([
        path('profile/<uuid:id>', admin_views.AdminProfileAPIView.as_view(), name="admin-view"),
        path('invite/', admin_views.AdminInviteExaminerAPIView.as_view(), name="admin-invite-examiner"),
        path('subadmin/', admin_views.AdminAddSubAdminAPIView.as_view(), name="admin-add-subadmin"),
    ])),
]

"""
SUBADMIN level URLs
"""

urlpatterns += [
    path('subadmin/', include([
        path('', subadmin_views.SubAdminListAPIView.as_view(), name="subadmin-get-list"),
        path('<uuid:id>/', subadmin_views.SubAdminProfileAPIView.as_view(), name="subadmin-profile-edit-delete"),
        path('<uuid:id>/undo/', subadmin_views.SubAdminUndoDeleteAPIView.as_view(), name="subadmin-undo-delete"),
    ])),
]

"""
PERMISSION level URLs
"""

urlpatterns += [
    path('permission/', include([
        path('', admin_views.PermissionListAPIVIew.as_view(), name="permission-list"),
    ])),
]

