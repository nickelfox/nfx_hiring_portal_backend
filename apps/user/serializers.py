from rest_framework import serializers, status
from apps.user import models as user_models
from common import utils, mail
from common import models as common_models
from common.constants import Constants, ApplicationMessages
from django.utils import timezone
from rest_framework.validators import ValidationError
from nhp import settings


class SessionSerializer(serializers.ModelSerializer):
    """
    Session serializer validates the request and stores the session
    """
    device_choice = (
        (Constants.IOS, Constants.IOS),
        (Constants.ANDROID, Constants.ANDROID),
        (Constants.WEB, Constants.WEB)
    )

    device_token = serializers.CharField()
    device_type = serializers.ChoiceField(choices=device_choice)
    is_safari = serializers.BooleanField(default=True)

    class Meta:
        model = user_models.Session
        fields = ('device_token', 'device_type', 'is_safari')

    def get_user(self, obj):
        user = self.context.get('user')
        serializer = UserSerializer(user, context={'user': self.context.get('user')})
        return serializer.data


class LoginSerializer(serializers.Serializer):
    """
    Login serializer
    """
    email = serializers.EmailField(max_length=255, required=True)
    password = serializers.CharField(max_length=255, required=True)
    device = SessionSerializer(write_only=True, required=False)

    def validate(self, attrs):
        """ will check email and password"""
        attrs["email"] = attrs.get("email").lower()
        return attrs


class ChangePasswordSerializer(serializers.Serializer):
    """ User can change password """

    current_password = serializers.CharField(max_length=200, required=True)
    new_password = serializers.CharField(max_length=200, required=True)
    confirm_password = serializers.CharField(max_length=200, required=True)

    def validate(self, data):
        """Validate password with above conditions"""
        # add check if current and new password is same or not, if same then raise error
        if data['current_password'] == data['new_password']:
            raise serializers.ValidationError(ApplicationMessages.CURRENT_NEW_PASSWORD_NOT_SAME)
        if data['confirm_password'] != data['new_password']:
            raise serializers.ValidationError(ApplicationMessages.NEW_PASSWORD_RETYPE_PASSWORD_NOT_SAME)
        # check if current saved password is equal to current password or not
        user_current_password = self.context.get("request").user.password
        if utils.make_password(data['current_password']) != user_current_password:
            raise serializers.ValidationError(ApplicationMessages.CURRENT_PASSWORD_INCORRECT)
        return {'current_password': data['current_password'], 'new_password': data['new_password']}


class ForgotPasswordSerializer(serializers.Serializer):
    """
    Take email as a parameter
    """

    email = serializers.EmailField(required=True)

    def validate(self, attr):
        """Validate user exists with same account or not"""

        attr["email"] = attr.get("email").lower()
        user = user_models.User.get_instance({'email': attr.get("email")})
        if user.is_active:
            return attr
        else:
            raise ValidationError(ApplicationMessages.USER_NOT_EXISTS)

    def create(self, validated_data):
        """ create and mail the token to the user"""

        user = user_models.User.get_instance({'email': validated_data.get("email")})
        token = utils.make_secret(validated_data.get("email"))
        data = {'user': user,
                'token': token,
                'expiry_time': timezone.now() + timezone.timedelta(hours=Constants.EMAIL_LINK_EXPIRY),
                }
        if data:
            instance = user_models.VerificationToken.create_password_token(data)
            BASE_URL = settings.FRONTEND_BASE_URL
            mail.ResetPasswordMail({'BASE_URL': BASE_URL, 'reset_token': token, 'email': validated_data.get("email")})
        else:
            return ValidationError(ApplicationMessages.BAD_REQUEST)
        return instance


class ResetPasswordSerializer(serializers.Serializer):
    """
    Confirm Reset password after the forgot password link sent
    """
    new_password = serializers.CharField(required=True)
    retype_password = serializers.CharField(required=True)

    def validate(self, attrs):
        """
        Check if new password is not equal to retyped password
        :param attrs:
        :return:
        """
        if attrs['new_password'] != attrs['retype_password']:
            raise serializers.ValidationError(ApplicationMessages.NEW_PASSWORD_RETYPE_PASSWORD_NOT_SAME)
        return {
            "new_password": attrs["new_password"],
            "retype_password": attrs["retype_password"],
        }


class InviteUserSerializer(serializers.ModelSerializer):
    """
    Take email as a parameter, and send invite via email
    """

    class Meta:
        model = user_models.User
        exclude = ('password', )
        extra_kwargs = {'department': {'required': True}}

    def create(self, validated_data):
        """ Create a new examiner with the details and invite them via email """
        if len(validated_data['phone_number']) != 10:
            raise ValidationError(ApplicationMessages.PHONE_NO_LENGTH)
        validated_data['role'] = user_models.Role.get_instance({"name": Constants.EXAMINER})
        new_password = utils.random_chars(10)
        validated_data['password'] = utils.make_password(new_password)
        user = user_models.User.create_instance(validated_data)

        user.save()
        if user:
            BASE_URL = settings.FRONTEND_BASE_URL
            mail.InviteExaminerMail({'BASE_URL': BASE_URL, 'user': user.full_name, 'new_password': new_password,
                                     'email': validated_data.get("email")})
        else:
            return ValidationError(ApplicationMessages.BAD_REQUEST)
        return user


class UserSerializer(serializers.ModelSerializer):
    """ List all Users Serializer"""

    department = serializers.SerializerMethodField(read_only=False)
    role = serializers.SerializerMethodField(read_only=False)
    status = serializers.SerializerMethodField(read_only=False)

    class Meta:
        model = user_models.User
        exclude = ('password',)

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance

    def get_department(self, obj):
        if obj.department:
            return obj.department.name

    def get_role(self, obj):
        if obj.role:
            return obj.role.name

    def get_status(self, obj):
        if obj.is_active:
            return Constants.ACTIVE
        else:
            return Constants.INACTIVE


class UserUpdateSerializer(serializers.ModelSerializer):
    """ User update and create serializer """

    class Meta:
        model = user_models.User
        exclude = ('password',)

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance


class FilterBySerializer(serializers.ModelSerializer):
    """
    Filter the User details on the basis of Department
    """
    full_name = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    phone_number = serializers.CharField(required=False)
    department = serializers.SerializerMethodField(read_only=False)
    role = serializers.SerializerMethodField(read_only=False)

    class Meta:
        model = user_models.User
        exclude = ('password',)

    def get_department(self, obj):
        if obj.department:
            return obj.department.name

    def get_role(self, obj):
        if obj.role:
            return obj.role.name


class AddSubAdminSerializer(serializers.ModelSerializer):
    """
    Take email as a parameter, and send invite to the sub-admin via email
    """
    permission = serializers.ListField(required=False)

    class Meta:
        model = user_models.User
        fields = ('id', 'full_name', 'email', 'department', 'is_active', 'role', 'designation', 'permission')

    def create(self, validated_data):
        """ Create a sub-admin with their corresponding permissions """
        permission = validated_data.pop("permission", None)
        validated_data['department'] = user_models.Department.get_instance({"name": Constants.SUBADMIN})
        validated_data['role'] = user_models.Role.get_instance({"name": Constants.SUBADMIN})
        new_password = utils.random_chars(10)
        validated_data['password'] = utils.make_password(new_password)
        # validated_data['password'] = "e160dcbe95c5dc3a0d20ea5e4318e01206bc7722d32d911cea4e6aebde48a2ea"  # nhp@123
        user = user_models.User.create_instance(validated_data)
        user.save()
        permisssion_dict = {}

        permisssion_dict['user'] = user
        permisssion_dict['permission'] = permission
        user_models.RolePermission.update_or_create_instance(permisssion_dict)
        if user:
            BASE_URL = settings.FRONTEND_BASE_URL
            mail.InviteSubAdminMail({'BASE_URL': BASE_URL, 'user': user.full_name, 'new_password': new_password,
                                     'email': validated_data.get("email")})
        else:
            return ValidationError(ApplicationMessages.BAD_REQUEST)
        return user

    def update(self, instance, validated_data):
        """ Update a sub-admin with or without their corresponding permissions """
        permission = validated_data.pop("permission", None)
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        if permission is not None:
            permisssion_dict = {}

            permisssion_dict['user'] = instance
            permisssion_dict['permission'] = permission
            user_models.RolePermission.update_or_create_instance(permisssion_dict)
            # instance.permission.clear()
            # instance.permission.add(*permission)
        return instance


class PermissionSerializer(serializers.ModelSerializer):
    """ User update and create serializer """

    class Meta:
        model = common_models.Permission
        fields = '__all__'


class RolePermissionSerializer(serializers.ModelSerializer):
    """ RolePermission list serializer """

    permission = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = user_models.RolePermission
        fields = '__all__'


class SubAdminListSerializer(serializers.ModelSerializer):
    """ Subadmin list serializer """

    permission = serializers.SerializerMethodField(read_only=True)
    department = serializers.SerializerMethodField(read_only=False)
    role = serializers.SerializerMethodField(read_only=False)
    status = serializers.SerializerMethodField(read_only=False)

    class Meta:
        model = user_models.User
        exclude = ('password',)

    def get_permission(self, obj):
        rp = user_models.RolePermission.get_instance({'user': obj.id})
        return RolePermissionSerializer(rp).data if obj.id else {}

    def get_department(self, obj):
        return obj.department.name if obj.department else {}

    def get_role(self, obj):
        return obj.role.name if obj.role else {}

    def get_status(self, obj):
        if obj.is_active:
            return Constants.ACTIVE
        else:
            return Constants.INACTIVE
