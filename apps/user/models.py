from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.db import models
from common.models import BaseModel, Permission
from common.constants import Constants, ApplicationMessages
from common import utils

from rest_framework.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken


class MyUserManager(BaseUserManager):
    """The Custom BaseManager Class"""

    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        """
        Creates and saves a superuser with the given email
        """
        user = self.create_user(email, password=password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class Role(BaseModel):
    """
    Role model is to specify an id and name to the roles of users
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        """
        String representation
        :return:
        """
        return "{}-{}".format(self.id, self.name)

    class Meta:
        """
        Verbose name and verbose plural
        """
        verbose_name = "Role"
        verbose_name_plural = "Role"
        ordering = ['-created_at']

    @staticmethod
    def get_instance(data):
        """ Retrieve Role instance with respect to ID"""
        try:
            return Role.objects.get(**data)
        except Department.DoesNotExist:
            raise ValidationError('error', status.HTTP_400_BAD_REQUEST)


class Department(BaseModel):
    """
    Department model is to specify the name of departments
    """
    name = models.CharField(max_length=255, null=False, blank=False, unique=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        """
        String representation
        :return:
        """
        return "{}-{}".format(self.name, self.is_active)

    class Meta:
        """
        Verbose name and verbose plural
        """
        verbose_name = "Department"
        verbose_name_plural = "Department"
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """Create department instance"""
        try:
            instance = Department.objects.create(**data)
            return instance
        except Department.DoesNotExist:
            raise ValidationError(ApplicationMessages.DOES_NOT_EXISTS.format("Department"),
                                  status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def get_instance(data):
        """Retrieve department instance with respect to its ID"""

        try:
            return Department.objects.get(**data)
        except Department.DoesNotExist:
            raise ValidationError('error', status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def filter_instance(filters):
        """Filter department with respect to data"""

        try:
            queryset = Department.objects.filter(**filters)
            return queryset
        except Department.DoesNotExist as ex:
            return ValidationError(str(ex))


class User(AbstractBaseUser, BaseModel):
    """
    User model with email and password as a login credentials
    """

    full_name = models.CharField(max_length=255, blank=False, null=False)
    email = models.EmailField(unique=True, blank=False, null=False)
    phone_number = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(max_length=255, blank=False, null=True)
    designation = models.CharField(max_length=255, blank=False, null=True)
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True)
    role = models.ForeignKey(Role, on_delete=models.SET_NULL, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()
    USERNAME_FIELD = 'email'

    def __str__(self):
        """
        String representation of name
        :return:
        """
        return "{}-{}".format(self.email, self.id)

    def has_perm(self, perm, obj=None):
        """Does the user have a specific permission?"""
        return True

    def has_module_perms(self, app_label):
        """Does the user have permissions to view the app `app_label`?"""
        return True

    @property
    def is_staff(self):
        """Is the user a member of staff?"""
        # All admins are staff
        return self.is_admin

    class Meta:
        """
        Verbose name and verbose plural
        """
        verbose_name = "User"
        verbose_name_plural = "User"
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """
        To create a new instance of User model
        """
        try:
            instance = User.objects.create(**data)
            return instance
        except User.DoesNotExist:
            raise ValidationError(ApplicationMessages.DOES_NOT_EXISTS.format("User"),
                                  status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def get_instance(data):
        """
        View the data of a particular type of user by the id
        """
        try:
            instance = User.objects.get(**data)
            return instance
        except User.DoesNotExist:
            raise ValidationError(ApplicationMessages.DOES_NOT_EXISTS.format("ID"),
                                  status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def filter_instance(filters):
        """
        Filter instances according to fields
        """
        return User.objects.filter(**filters)

    def tokens(self):
        """
        For retrieving tokens from simple-jwt
        """
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }

    @staticmethod
    def update_password(user_id, new_password, old_password=None):
        """
        For updating the existing password of a user
        """
        try:
            obj = User.objects.get(id=user_id)
            if obj.password == utils.make_password(old_password):
                obj.password = utils.make_password(new_password)
                obj.save()
                return Response(str(ApplicationMessages.PASSWORD_CHANGE),
                                status=status.HTTP_200_OK)
            else:
                raise ValidationError(ApplicationMessages.CURRENT_PASSWORD_INCORRECT,
                                      status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            raise ValidationError(ApplicationMessages.USER_NOT_EXISTS,
                                  status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def get_instance_or_none(data):
        """
        View the data of a particular type of user by the id else none
        """
        try:
            instance = User.objects.get(**data)
            return instance
        except User.DoesNotExist:
            raise None


class Session(BaseModel):
    """Session table store data with session id"""

    device = (
        (Constants.WEB, Constants.WEB),
        (Constants.ANDROID, Constants.ANDROID),
        (Constants.IOS, Constants.IOS)
    )

    token = models.ForeignKey(OutstandingToken, on_delete=models.CASCADE, null=True)
    device_token = models.TextField(null=True)
    device_type = models.CharField(max_length=100, null=True, choices=device)
    is_safari = models.BooleanField(default=False)
    ip_address = models.GenericIPAddressField(protocol="both", unpack_ipv4=False, blank=True, null=True)
    user = models.ForeignKey(User, related_name="session_set", on_delete=models.CASCADE, verbose_name="user")

    def __str__(self):
        """
        String representation of session
        :return:
        """
        return "{}".format(self.user)

    class Meta:
        """
        Verbose name and verbose name plural
        """
        verbose_name = "Session"
        verbose_name_plural = "Session"
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """create user info session data as a dict"""

        try:
            instance = Session.objects.create(**data)
            return instance
        except Exception as ex:
            raise ValidationError(str(ex))


class VerificationToken(BaseModel):
    """
    1. Save password reset verification token
    2. Save user verification token
    On the basis of token type
    """
    PASSWORD_RESET = 'PASSWORD_RESET'
    VERIFICATION = 'VERIFICATION'
    INVITATION = 'INVITATION'

    TYPES = (
        (PASSWORD_RESET, 'Password Reset'),
        (VERIFICATION, 'Account Verification'),
        (INVITATION, 'Examiner Invitation'),
    )
    user = models.ForeignKey(User, related_name="verification_token", on_delete=models.CASCADE)
    token_type = models.CharField(max_length=100, blank=False, null=False, choices=TYPES)
    token = models.CharField(max_length=255, blank=False, null=False)
    expiry_time = models.DateTimeField(null=True, blank=True)
    token_used = models.BooleanField(default=False)

    def __str__(self):
        """
        String  representation of token
        :return:
        """
        return "{}-{}-{}-{}".format(self.user.email, self.token, self.token_type, self.token_used)

    class Meta:
        """
        Verbose name and verbose name plural
        """
        verbose_name = "VerificationToken"
        verbose_name_plural = "VerificationToken"
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """
        To create a new instance of VerificationToken model
        """

        try:
            instance = VerificationToken.objects.create(**data)
            return instance
        except VerificationToken.DoesNotExist:
            raise ValidationError(ApplicationMessages.DOES_NOT_EXISTS.format("Verification-Token"),
                                  status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def get_instance(data):
        """
        View the data of a particular token
        """

        try:
            instance = VerificationToken.objects.get(**data)
            return instance
        except VerificationToken.DoesNotExist:
            raise ValidationError(ApplicationMessages.DOES_NOT_EXISTS.format("ID"),
                                  status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def filter_instance(filters):
        """
        Filter instances according to fields
        """

        return VerificationToken.objects.filter(**filters)

    @staticmethod
    def create_password_token(kwargs):
        """
        Creates a Token and return user obj
        """

        obj = VerificationToken(**kwargs)
        obj.token_type = Constants.RESET_PASSWORD_TOKEN
        obj.save()
        return obj


class RolePermission(BaseModel):
    """
    RolePermission model is used to map a permission with particular User (with a particular role)
    """

    permission = models.ManyToManyField(Permission, blank=False)
    user = models.ForeignKey(User, related_name='user_role', on_delete=models.CASCADE)

    def __str__(self):
        """
        print: {id}/role-name/user-name
        :return:
        """
        return "{}-{}-{}".format(self.id, self.user.full_name, self.user.role.name)

    @staticmethod
    def create_instance(data):
        """Create RolePermission instance """

        try:
            instance = RolePermission.objects.create(**data)
            return instance
        except Exception as e:
            raise ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """Get a RolePermission instance with respect to ID """

        try:
            instance = RolePermission.objects.get(**data)
            return instance
        except:
            raise Exception(ApplicationMessages.NOT_ALLOWED)

    @staticmethod
    def filter_instance(filters):
        """Filter RolePermission instance with respect to data provided"""

        try:
            queryset = RolePermission.objects.filter(**filters)
            return queryset
        except RolePermission.DoesNotExist as ex:
            return ValidationError(str(ex))

    @staticmethod
    def update_or_create_instance(data):
        """ Create new RolePermission data with more than one permissions"""

        permission = data.pop("permission", None)
        instance = RolePermission.objects.filter(**data).first()
        if instance is None:
            instance = RolePermission.objects.create(**data)
            instance.permission.add(*permission)
        else:
            instance.permission.clear()
            instance.permission.add(*permission)
        return instance


class NotificationLog(BaseModel):
    """
    NotificationLog model is used maintain the log if notification for a particular user
    """

    notifications = (
        (Constants.JOB, Constants.JOB),
        (Constants.EXAM, Constants.EXAM),
        (Constants.CANDIDATE, Constants.CANDIDATE),
        (Constants.SUBADMIN, Constants.SUBADMIN),
        (Constants.EXAMINER, Constants.EXAMINER),
    )

    title = models.CharField(max_length=255, blank=False, null=False)
    link = models.CharField(max_length=255, blank=True, null=True)
    payload = models.TextField()
    receiver = models.ForeignKey(User, related_name='notification_receiver', on_delete=models.CASCADE)
    sender = models.ForeignKey(User, related_name='notification_sender', on_delete=models.CASCADE, null=True)
    notification_type = models.CharField(max_length=100, choices=notifications, default=Constants.JOB)
    mark_as_read = models.BooleanField(default=False)
    icon = models.CharField(max_length=255, null=True)

    def __str__(self):
        """
        print: {id}/{title}/{user-name}
        :return:
        """
        return "{}-{}-{}".format(self.id, self.title, self.receiver.full_name)

    @staticmethod
    def create_instance(data):
        """ Create a notification instance based """
        try:
            instance = NotificationLog.objects.create(**data)
            return instance
        except Exception as e:
            raise ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get a notification instance based on id """

        try:
            instance = NotificationLog.objects.get(**data)
            return instance
        except Exception as e:
            raise ValidationError(str(e))

    @staticmethod
    def notification_filter(filters):
        """Filter notification instance with respect to data provided"""

        try:
            queryset = NotificationLog.objects.filter(**filters)
            return queryset
        except NotificationLog.DoesNotExist as ex:
            return ValidationError(str(ex))