from django.contrib import admin
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken

from apps.user import models as user_model


class AdminUser(admin.ModelAdmin):
    """ The Admin representation of User """
    def force_delete_user(self, request, queryset):
        """ This action will delete the user in spite of user having active session"""
        users = queryset.values("id")
        OutstandingToken.objects.filter(user__id__in=users).delete()
        queryset.delete()

    actions = ["force_delete_user"]


class AdminUserView(admin.ModelAdmin):
    """The Generic Admin View"""
    ordering = ('-created_at',)


admin.site.register(user_model.Role, AdminUser)
admin.site.register(user_model.Department, AdminUserView)
admin.site.register(user_model.User, AdminUserView)
admin.site.register(user_model.Session, AdminUserView)
admin.site.register(user_model.VerificationToken, AdminUserView)
