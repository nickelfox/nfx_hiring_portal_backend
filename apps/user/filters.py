from rest_framework.filters import BaseFilterBackend


class UserFilterByStatus(BaseFilterBackend):
    """ Filter User data with respect to status """
    param = "status"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.get(self.param, None)
        if param == 'Active':
            return queryset.filter(is_active=True)
        elif param == 'Inactive':
            return queryset.filter(is_active=False)
        return queryset