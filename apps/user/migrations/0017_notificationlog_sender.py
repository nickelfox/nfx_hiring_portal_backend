# Generated by Django 3.0.6 on 2021-12-16 18:33

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0016_auto_20211102_1521'),
    ]

    operations = [
        migrations.AddField(
            model_name='notificationlog',
            name='sender',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='notification_sender', to=settings.AUTH_USER_MODEL),
        ),
    ]
