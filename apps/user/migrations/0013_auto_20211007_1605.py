# Generated by Django 3.0.6 on 2021-10-07 16:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0012_auto_20211001_1058'),
    ]

    operations = [
        migrations.RenameField(
            model_name='notificationlog',
            old_name='body',
            new_name='payload',
        ),
    ]
