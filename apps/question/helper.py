import boto3
from nhp import settings
from botocore.config import Config
from datetime import timedelta, datetime
import environ

s3 = boto3.client('s3', aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                  aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                  region_name=settings.AWS_REGION,
                  config=Config(s3={'addressing_style': 'path'}, signature_version='s3v4'))


def generate_s3_signed_url(key=None):
    url = s3.generate_presigned_url(
        ClientMethod='get_object',
        Params={
            'Bucket': settings.AWS_STORAGE_BUCKET_NAME,
            'Key': '{}'.format(key)
        }
    )
    return url

def check_file_size(file, mb_limit):
	limit = 1024 * 1024 * mb_limit # 1 MB allowed
	if file.size > limit:
		return False
	return True

class S3Object:

    def __init__(self, file, file_type, content_type):
        self.file = file
        self.file_type = file_type
        self.content_type = content_type

    def upload_question_image(self, img_name):
        s_3 = boto3.resource('s3')
        env = environ.Env()('ENVIRONMENT')
        key = env.lower() + "/exam-questions/images/image_{}_{}".format(datetime.now(), img_name)

        s_3.Bucket(settings.AWS_STORAGE_BUCKET_NAME).put_object(
            Key=key, Body=self.file)
        file_url = generate_s3_signed_url(key=key)
        file_url = file_url.split('?')[0]
        return file_url