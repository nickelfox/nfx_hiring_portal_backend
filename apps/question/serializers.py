from rest_framework import serializers

from apps.candidate.models import CandidateAnswer, CandidateExam
from apps.question import models
from apps.exam import models as exam_model
from apps.questiontag import models as qt_model
from apps.questiontag import serializers as qt_serializer
from apps.candidate import services as candidate_services
from rest_framework.exceptions import ValidationError
from common.constants import ApplicationMessages
from apps.candidate import models as candidate_models
from apps.clone import serailizers as clone_serializers


class QuestionListSerializer(serializers.ModelSerializer):
    """ Question List Serializer"""
    department = serializers.SerializerMethodField(read_only=True)
    tags = serializers.SerializerMethodField(read_only=True)
    answers = serializers.SerializerMethodField(read_only=True)
    ongoing_exam = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.Question
        fields = "__all__"

    def update(self, instance, validated_data):
        """Update Question data """
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance

    def get_tags(self, obj):
        question_tag_model = qt_model.QuestionTag
        tag = question_tag_model.filter_instance({'question_id': obj.id})
        qtag_serializer = qt_serializer.QTagListSerializer(tag, many=True)
        return qtag_serializer.data

    def get_department(self, obj):
        if obj.department:
            dept_dict = {
                "name": obj.department.name,
                "id": obj.department.id
            }
            return dept_dict

    def get_answers(self, obj):
        answer_obj = models.QuestionAnswer.objects.filter(question=obj)
        data = QuestionAnswerTextSerializer(answer_obj, many=True).data
        return data

    def get_ongoing_exam(self, obj):
        # raise Exception(obj)
        ongoing_exam = False
        exam = exam_model.ExamQuestion.objects.filter(question=obj.id).first()
        # raise Exception(exam)
        if exam:
            ce = candidate_models.CandidateExamStatus.objects.filter(candidate_exam__exam=exam.exam.id)
            if ce:
                candidate_exam = candidate_models.CandidateExamStatus.objects.filter(candidate_exam__exam=exam.exam.id).latest('created_at')

                if candidate_exam.exam_status.status == "PASSED" or candidate_exam.exam_status.status == "FAILED" or \
                        candidate_exam.exam_status.status == "LINK_NOT_SENT" or \
                        candidate_exam.exam_status.status == "DISQUALIFY":
                    ongoing_exam = False
                else:
                    ongoing_exam = True
        return ongoing_exam


class QuestionAnswerTextSerializer(serializers.ModelSerializer):
    """ Create Question Serializer"""

    class Meta:
        model = models.QuestionAnswer
        fields = '__all__'


class CandidateAnswerDetailsSerializer(serializers.ModelSerializer):
    """ Create Question Serializer"""

    is_selected = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswer
        exclude = ['is_correct']

    def get_is_selected(self, obj):
        candidate_exam = self.context.get("candidate_exam")
        return candidate_services.is_answer_selected(obj.question.id, obj.id, candidate_exam)


class CandidateAnswerResultDetailsSerializer(serializers.ModelSerializer):
    """ Create Question Serializer"""

    is_selected = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswer
        fields = '__all__'

    def get_is_selected(self, obj):
        candidate = CandidateAnswer.filter_instance({'question_id': obj.question.id}).first()
        return candidate_services.is_answer_selected(obj.question.id, obj.id, candidate.candidate_exam_id)


class QuestionCreateSerializer(serializers.ModelSerializer):
    """ Create Question Serializer"""

    class Meta:
        model = models.Question
        fields = "__all__"

    def create(self, validated_data):
        """Create a Question data """
        if validated_data['time_duration'] == 0:
            raise ValidationError(ApplicationMessages.QUESTION_DURATION)
        instance = models.Question.create_instance(validated_data)
        return instance

    def update(self, instance, validated_data):
        """Update Question data """
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance


class FilterByQuestionSerializer(serializers.ModelSerializer):
    """ filter by question serializer"""

    question_name = serializers.CharField(required=False)
    department = serializers.CharField(required=False)
    experience_level = serializers.CharField(required=False)
    time_duration = serializers.IntegerField(read_only=False)
    question_status = serializers.CharField(required=False)

    class Meta:
        model = models.Question
        fields = "__all__"


class QuestionAnswerCreateSerializer(serializers.ModelSerializer):
    """ Create multiple answers for specific Question"""

    id = serializers.UUIDField()

    class Meta:

        model = models.QuestionAnswer
        fields = '__all__'

    def create(self, validated_data):
        answer, created = models.QuestionAnswer.objects.update_or_create(
            id=validated_data.get('id', None),
            defaults=validated_data)
        return answer


class ExamQuestionAnswerListSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswer Table
    """
    questions = serializers.SerializerMethodField(read_only=True)
    exam = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = exam_model.ExamQuestion
        fields = '__all__'

    def get_questions(self, obj):
        question = QuestionListSerializer(obj.question, many=True).data
        return question

    def get_exam(self, obj):
        exam = exam_model.Exam.get_instance({'id': obj.exam_id})
        if exam:
            obj_dict = {
                'exam_id': exam.id,
                'exam_name': exam.exam_name,
                'exam_department': exam.department.name,
                'exam_department_id': exam.department.id,
            }
            return obj_dict


class QuestionAnswerListSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswer Table
    """
    question = serializers.SerializerMethodField(read_only=True)
    answer_text = serializers.SerializerMethodField(read_only=True)
    tags = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswer
        fields = ['question', 'answer_text', 'tags']

    def get_question(self, obj):
        dict_obj = {
            'question_id': obj.question.id,
            'question_name': obj.question.question_name,
            'time_duration': obj.question.time_duration,
            'experience_level': obj.question.experience_level
        }
        return dict_obj if obj else {}

    def get_tags(self, obj):
        question_tag_model = qt_model.QuestionTag
        tag = question_tag_model.filter_instance({'question_id': obj.question.id})
        qtag_serializer = qt_serializer.QTagListSerializer(tag, many=True)
        return qtag_serializer.data

    def get_answer_text(self, obj):
        dict_obj = {
           'answer_id': obj.id,
           'answer_text': obj.answer_text,
           'is_correct': obj.is_correct,
           'is_selected': candidate_services.is_answer_selected(obj.question.id, obj.id)
        }
        return dict_obj if obj else {}


class QuestionAnswerResultSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswer Table
    """
    question = serializers.SerializerMethodField(read_only=True)
    answer_text = serializers.SerializerMethodField(read_only=True)
    tags = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswer
        fields = '__all__'

    def get_question(self, obj):
        dict_obj = {
            'question_id': obj.id,
            'question_name': obj.question_name,
            'time_duration': obj.time_duration,
            'experience_level': obj.experience_level
        }
        return dict_obj if obj else {}

    def get_tags(self, obj):
        question_tag_model = qt_model.QuestionTag
        tag = question_tag_model.filter_instance({'question_id': obj.id})
        qtag_serializer = qt_serializer.QTagListSerializer(tag, many=True)
        return qtag_serializer.data

    def get_answer_text(self, obj):

        dict_obj = {
           'answer_id': obj.id,
           'answers': CandidateAnswerResultDetailsSerializer(obj.answer.all(), many=True).data,
        }
        return dict_obj if obj else {}


class CandidateQuestionAnswerSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswer Table
    """
    question = serializers.SerializerMethodField(read_only=True)
    answer_text = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswer
        fields = ['question', 'answer_text']

    def get_question(self, obj):

        dict_obj = {
            'question_id': obj.question.id,
            'question_name': obj.question.question_name,
            'time_duration': obj.question.time_duration,
            'experience_level': obj.question.experience_level
        }
        return dict_obj if obj else {}

    def get_answer_text(self, obj):
        data = QuestionAnswerTextSerializer(obj, many=True).data
        return data
        dict_obj = {
           'answer_id': obj.id,
           'answer_text': obj.answer_text,
           'is_correct': obj.is_correct,
           'is_selected': candidate_services.is_answer_selected(obj.question.id, obj.id)
        }
        return dict_obj if obj else {}
