from rest_framework.filters import BaseFilterBackend


class QuestionFilterByLevel(BaseFilterBackend):
    """Filter Questions by experience level"""

    param = "experience_level"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(experience_level__in=param)
        return queryset


class QuestionFilterByStatus(BaseFilterBackend):
    """Filter Question by question status"""

    param = "question_status"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(question_status__in=param)
        return queryset


class QuestionDepartmentFilterByID(BaseFilterBackend):
    """Filter department by its ID"""

    param = "department"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(department__in=param)

        return queryset
