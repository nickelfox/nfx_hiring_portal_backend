from django.urls import path
from apps.question import views
from apps.questiontag import views as qtag_views
import uuid

urlpatterns = [

    path('', views.QuestionListCreateAPIView.as_view(), name='list-create-search-filter-question'),
    path('<uuid:id>', views.QuestionEditDeleteAPIView.as_view(), name='updated-delete-question'),
    path('image-upload', views.UploadQuestionImageAPIView.as_view(), name='updated-delete-question'),
    path('undo/<uuid:id>', views.UndoDeleteAPIView.as_view(), name='undo-delete-question'),
    path('<uuid:id>/answers', views.QuestionWithMultipleAnswersAPIView.as_view(), name='answers'),
    path('<uuid:question_id>/tag/', qtag_views.QTagCreateListAPIVIew.as_view(), name="list-create-question-tag"),

  ]
