from django.db import models
from common.models import BaseModel
from apps.user.models import Department
from rest_framework.exceptions import ValidationError


class Question(BaseModel):
    """Hold the question and their id, duration of each question, exam name and department of the examiner"""
    status = [
        ('publish', 'PUBLISH'),
        ('unpublish', 'UNPUBLISH'),
        ('draft', 'DRAFT')
    ]
    level = [
        ('senior', 'SENIOR'),
        ('junior', 'JUNIOR'),
        ('internship', 'INTERNSHIP')
    ]

    question_name = models.CharField(max_length=255, blank=False, null=False)
    department = models.ForeignKey(Department, related_name='question_department', on_delete=models.CASCADE)
    experience_level = models.CharField(max_length=255, blank=False, null=False, choices=level)
    question_status = models.CharField(max_length=255, blank=True, choices=status, null=True)
    time_duration = models.IntegerField(default=60)

    def __str__(self):
        """ String representation of Question """
        return '{}-{}-{}'.format(self.question_name, self.department, self.experience_level)

    class Meta:
        """ Verbose name and plural Verbose name"""
        verbose_name = "Question"
        verbose_name_plural = "Question"
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create Question model """
        try:
            return Question.objects.create(**data)
        except Question.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get Question Details"""
        try:
            return Question.objects.get(**data)
        except Question.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Filters the questions with respected to level , status etc """
        return Question.objects.filter(**filters)


class QuestionAnswer(BaseModel):
    """Holds answers associated with each question, their id, correctness."""
    question = models.ForeignKey(Question, related_name='answer', on_delete=models.CASCADE)
    answer_text = models.TextField()
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        """ String representation of Answer"""
        return "{}-{}-{}-{}".format(self.pk, self.question.question_name, self.answer_text, self.is_correct)

    class Meta:
        """
        Verbose name and Verbose names plural
        """
        verbose_name = 'QuestionAnswer'
        verbose_name_plural = 'QuestionAnswer'
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create answers for the Questions """
        try:
            return QuestionAnswer.objects.create(**data)
        except QuestionAnswer.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get QuestionAnswer data with respect to ID """
        try:
            return QuestionAnswer.objects.get(**data)
        except QuestionAnswer.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Filter the Question with their answers according to question_id,"""
        return QuestionAnswer.objects.filter(**filters)

