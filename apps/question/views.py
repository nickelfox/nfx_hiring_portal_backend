from rest_framework.response import Response
from django.utils import timezone
from rest_framework import views, status, parsers, generics
from . import serializers
from . import models
from common import constants, utils, mail, permissions, pagination
from rest_framework.filters import SearchFilter
from common.constants import ApplicationMessages, Constants
from apps.department import filters as department_filter
from apps.question import filters as question_filter
from apps.question import helper
from apps.questiontag import models as qtag_models
from django.shortcuts import HttpResponse
import json


class QuestionListCreateAPIView(generics.ListAPIView):
    """ Get Question List and Add new Question"""

    serializer_class = serializers.QuestionListSerializer
    parser_classes = (parsers.JSONParser, )
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer, )
    pagination_class = pagination.DefaultPagination
    model = models.Question
    filter_backends = (SearchFilter, department_filter.DepartmentFilterByID, question_filter.QuestionFilterByLevel,
                       question_filter.QuestionFilterByStatus, )
    search_fields = ['id', 'question_name', 'department__id', 'department__name', 'question_status', 'experience_level',
                     'time_duration', 'questiontag__tag_id__tag_name']

    def get_queryset(self,):
        """ Returns list of questions from the Question Table """

        return self.model.filter_instance({'deleted_at__isnull': True})

    def get(self, request, *args, **kwargs):
        """ Return list of Question, filter questions based on level/status, search Question by keyword"""

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(self.paginate_queryset(filtered_queryset), many=True)

            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("QUESTION"),
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        """ Create New Question """

        serializer_class = serializers.QuestionCreateSerializer
        serializer = serializer_class(data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class QuestionEditDeleteAPIView(views.APIView):
    """
    Update, Delete a specific Question
    """

    serializer_class = serializers.QuestionListSerializer
    update_serializer = serializers.QuestionCreateSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    model = models.Question

    def get_queryset(self, id=None):
        """Retrieve a question based on its ID"""

        if id:
            return self.model.get_instance({'id': id})
        else:
            return self.model.get_instance({"Question__details": self.request.Question__question_name, })

    def patch(self, request, id, *args, **kwargs):
        """ Update specific Question based on its ID """

        try:
            question = self.model.objects.get(id=id, deleted_at__isnull=True)
            serializer = self.update_serializer(question, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.QUESTION_NOT_EXIST, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        """ Deleting A specific Question"""

        try:
            question = self.model.objects.get(id=id, deleted_at__isnull=True)
            question_answer = models.QuestionAnswer.filter_instance({'question': question})
            question_tag = qtag_models.QuestionTag.filter_instance({'question_id': question})

            # Delete answers associated with the questions before deleting the question
            for questions in question_answer:
                questions.deleted_at = timezone.now()
                questions.save()

            # Delete tags associated with the questions before deleting the question
            for tags in question_tag:
                tags.deleted_at = timezone.now()
                tags.save()
            question.deleted_at = timezone.now()
            question.save()
            return Response(ApplicationMessages.QUESTION_DELETED, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DELETE_NOT_DONE, status=status.HTTP_400_BAD_REQUEST)


class UndoDeleteAPIView(views.APIView):
    """
    Undo the deletion of a question from database.
    """
    serializer_class = serializers.QuestionListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    model = models.Question

    def get_queryset(self, id=None):
        if id:
            return self.model.get_instance({'id': id})
        else:
            return self.model.get_instance({"Question__details": self.request.Question__question_name, })

    def patch(self, request, id, *args, **kwargs):
        """ Undo the delete action, change the deleted_at field to NULL again  """

        try:
            question = self.model.objects.get(id=id, deleted_at__isnull=False)
            serializer = self.serializer_class(question, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.QUESTION_NOT_EXIST, status=status.HTTP_400_BAD_REQUEST)


class QuestionWithMultipleAnswersAPIView(generics.ListAPIView):
    """ Create multiple Answers for specific Question"""

    serializer_class = serializers.QuestionAnswerCreateSerializer
    serializer_class_list = serializers.QuestionAnswerListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    question_model = models.Question
    pagination_class = pagination.DefaultPagination
    answer_model = models.QuestionAnswer

    def get_queryset(self, id):
        """ Returns question and their answers according question_id """

        return self.answer_model.filter_instance({'deleted_at__isnull': True, 'question': id})

    def get(self, request, id, *args, **kwargs):
        """ Fetch question according the question_id and their all the answers from the QuestionAnswer Table """
        filtered_queryset = self.filter_queryset(queryset=self.get_queryset(id))
        serializer = self.serializer_class_list(filtered_queryset, many=True)

        response = {"question": [], "answers": [], "tags": []}

        for k, v in enumerate(serializer.data):
            response["question"] = v['question']
            response["answers"].append(v['answer_text'])
            response["tags"].append(v['tags'])

        return Response(response, status=status.HTTP_200_OK)

    def post(self, request, id, *arsg, **kwargs):
        """ Create multiple Answers for specific Question """
        models.QuestionAnswer.objects.filter(question=id).delete()
        for answer in request.data:
            if 'answer_id' in answer:
                answer.update({"question": id, "id": answer['answer_id']})
            else:
                answer.update({"question": id})
        answer = self.serializer_class(data=request.data, many=True, partial=True)
        if answer.is_valid():
            answer.save()
            return Response(answer.data, status=status.HTTP_200_OK)
        else:
            return Response(answer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        """ Delete specific answer with primary key """
        try:
            answer = models.QuestionAnswer.objects.get(pk=id, deleted_at__isnull=True)
            answer.deleted_at = timezone.now()
            answer.save()
            return Response(constants.ApplicationMessages.ANSWER_DELETED, status=status.HTTP_200_OK)
        except self.answer_model.DoesNotExist:
            return Response(constants.ApplicationMessages.DELETE_NOT_DONE, status=status.HTTP_404_NOT_FOUND)


class UploadQuestionImageAPIView(views.APIView):

    def post(self, request, *args, **kwargs):

        file = self.request.FILES.get('upload')
        mb_limit = 1
        file_size_validate = helper.check_file_size(file, mb_limit)

        if not file_size_validate:
            return HttpResponse(json.dumps({'error': f'File too large. Size should not exceed {mb_limit} MiB'}), content_type="application/json")

        # Compress Images on the fly...
        # from PIL import Image
        # from io import BytesIO

        # in_mem_file = BytesIO(file.read())
        # image = Image.open(in_mem_file)
        # image.resize((160,300),Image.ANTIALIAS)
        # in_mem_file = BytesIO()
        # image.save(in_mem_file, format="PNG", quality=20, optimize=True)
        # in_mem_file.seek(0)

        img_url = helper.S3Object(file=file, file_type='image', content_type=file.content_type).upload_question_image(
            img_name=file.name
        )

        res = {'url': img_url}

        return HttpResponse(json.dumps(res), content_type="application/json")