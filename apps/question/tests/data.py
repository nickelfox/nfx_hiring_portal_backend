from django.utils import timezone


class DataSet:
    """The Dataset"""

    @staticmethod
    def question_data():

        """question dataset"""

        data = {
            "id": "1318a8f7-dd14-4033-9068-2ce11f483d3f",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "question_name": "TEST QUESTION",
            "experience_level": "senior",
            "question_status": "publish",
            "time_duration": 60
        }
        return data

    @staticmethod
    def question_answer_data():

        """question_answer dataset"""

        data = {
            "id": "65ab1062-7679-4b65-9f7e-2683863a19cb",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "answer_text": "TEST ANSWER",
            "is_correct": True
        }
        return data
