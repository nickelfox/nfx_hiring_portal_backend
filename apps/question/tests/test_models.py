from apps.user import models as user_models
from apps.question import models as question_models
from apps.question.tests import data
from rest_framework.test import APITestCase


class QuestionModel(APITestCase):
    """Generic Question model test setup"""

    def setUp(self):
        # create department and question

        self.department = user_models.Department.objects.create(name='HR')
        self.question = question_models.Question.objects.create(department=self.department,
                                                                **data.DataSet.question_data())

    def test_create_question_model(self):
        """test model"""
        self.assertEqual(str(self.question.id), data.DataSet.question_data().get("id"))

    def test_exam_count(self):
        """test count model"""

        questions = question_models.Question.objects.all().count()
        self.assertEqual(questions, 1)

    def test_delete_exam_count(self):
        """test delete count"""

        self.question.delete()
        questions = question_models.Question.objects.all().count()
        self.assertEqual(questions, 0)


class QuestionAnswerModel(APITestCase):
    """Generic QuestionAnswer model test setup"""

    def setUp(self):
        # create department, question and question_answer

        self.department = user_models.Department.objects.create(name='HR')
        self.question = question_models.Question.objects.create(department=self.department,
                                                                **data.DataSet.question_data())
        self.question_answer = question_models.QuestionAnswer.objects.create(question=self.question,
                                                                             **data.DataSet.question_answer_data())

    def test_create_question_model(self):
        """test model"""
        self.assertEqual(str(self.question_answer.id), data.DataSet.question_answer_data().get("id"))

    def test_exam_count(self):
        """test count model"""

        questions_answers = question_models.QuestionAnswer.objects.all().count()
        self.assertEqual(questions_answers, 1)

    def test_delete_exam_count(self):
        """test delete count"""

        self.question_answer.delete()
        questions_answers = question_models.QuestionAnswer.objects.all().count()
        self.assertEqual(questions_answers, 0)
