from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.QuestionClone)
admin.site.register(models.QuestionAnswerClone)
admin.site.register(models.ExamQuestionClone)
