from rest_framework import serializers
from apps.clone import models
from apps.exam import models as exam_model
from apps.questiontag import models as qt_model
from apps.questiontag import serializers as qt_serializer
from apps.candidate import services as candidate_services
from apps.candidate import models as candidate_models, serializers as candidate_serializer


class QuestionListSerializer(serializers.ModelSerializer):
    """ Question-Clone List Serializer"""
    department = serializers.SerializerMethodField(read_only=True)
    tags = serializers.SerializerMethodField(read_only=True)
    answers = serializers.SerializerMethodField(read_only=True)
    ongoing_exam = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionClone
        fields = "__all__"

    def update(self, instance, validated_data):
        """Update Question data """
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance

    def get_tags(self, obj):
        question_tag_model = qt_model.QuestionTag
        tag = question_tag_model.filter_instance({'question_id': obj.id})
        qtag_serializer = qt_serializer.QTagListSerializer(tag, many=True)
        return qtag_serializer.data

    def get_department(self, obj):
        if obj.department:
            dept_dict = {
                "name": obj.department.name,
                "id": obj.department.id
            }
            return dept_dict

    def get_answers(self, obj):
        answer_obj = models.QuestionAnswer.objects.filter(question=obj)
        data = QuestionAnswerTextSerializer(answer_obj, many=True).data
        return data

    def get_ongoing_exam(self, obj):
        # raise Exception(obj)
        ongoing_exam = False
        exam = exam_model.ExamQuestion.objects.filter(question=obj.id).first()
        # raise Exception(exam)
        if exam:
            ce = candidate_models.CandidateExamStatus.objects.filter(candidate_exam__exam=exam.exam.id)
            if ce:
                candidate_exam = candidate_models.CandidateExamStatus.objects.filter(candidate_exam__exam=exam.exam.id).latest('created_at')

                if candidate_exam.exam_status.status == "PASSED" or candidate_exam.exam_status.status == "FAILED" or \
                        candidate_exam.exam_status.status == "LINK_NOT_SENT" or \
                        candidate_exam.exam_status.status == "DISQUALIFY":
                    ongoing_exam = False
                else:
                    ongoing_exam = True
        return ongoing_exam


class QuestionAnswerTextSerializer(serializers.ModelSerializer):
    """ Create Question Serializer"""

    class Meta:
        model = models.QuestionAnswerClone
        fields = '__all__'


class ExamQuestionAnswerListSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswerClone Table
    """
    questions = serializers.SerializerMethodField(read_only=True)
    exam = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = exam_model.ExamQuestion
        fields = '__all__'

    def get_questions(self, obj):
        question = QuestionListSerializer(obj.question, many=True).data
        return question

    def get_exam(self, obj):
        exam = exam_model.Exam.get_instance({'id': obj.exam_id})
        if exam:
            obj_dict = {
                'exam_id': exam.id,
                'exam_name': exam.exam_name,
                'exam_department': exam.department.name,
                'exam_department_id': exam.department.id,
            }
            return obj_dict


class QuestionAnswerListSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswerClone Table
    """
    question = serializers.SerializerMethodField(read_only=True)
    answer_text = serializers.SerializerMethodField(read_only=True)
    tags = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswerClone
        fields = ['question', 'answer_text', 'tags']

    def get_question(self, obj):
        dict_obj = {
            'question_id': obj.question.id,
            'question_name': obj.question.question_name,
            'time_duration': obj.question.time_duration,
            'experience_level': obj.question.experience_level
        }
        return dict_obj if obj else {}

    def get_tags(self, obj):
        question_tag_model = qt_model.QuestionTag
        tag = question_tag_model.filter_instance({'question_id': obj.question.id})
        qtag_serializer = qt_serializer.QTagListSerializer(tag, many=True)
        return qtag_serializer.data

    def get_answer_text(self, obj):
        dict_obj = {
           'answer_id': obj.id,
           'answer_text': obj.answer_text,
           'is_correct': obj.is_correct,
           'is_selected': candidate_services.is_answer_selected(obj.question.id, obj.id)
        }
        return dict_obj if obj else {}


class CandidateQuestionAnswerSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswerClone Table
    """
    question = serializers.SerializerMethodField(read_only=True)
    answer_text = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswerClone
        fields = ['question', 'answer_text']

    def get_question(self, obj):
        dict_obj = {
            'question_id': obj.question.id,
            'question_name': obj.question.question_name,
            'time_duration': obj.question.time_duration,
            'experience_level': obj.question.experience_level
        }
        return dict_obj if obj else {}

    def get_answer_text(self, obj):
        data = QuestionAnswerTextSerializer(obj, many=True).data
        return data


class CandidateQuestionsSerializer(serializers.ModelSerializer):
    """ Candidate Questions Serializer"""
    exam = serializers.SerializerMethodField(read_only=True)
    answers = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionAnswerClone
        fields = ['exam', 'answers', ]

    def get_exam(self, obj):
        question_id = self.context.get("question_id")
        exam_id = self.context.get("exam_id")
        candidate_id = self.context.get("candidate_id")
        question = models.QuestionClone.get_instance({'id': question_id})

        candidate_exam = candidate_models.CandidateExam.get_instance({'exam__id': exam_id,
                                                                      'candidate__id': candidate_id})

        candidate_models.CandidateAnswer.create_or_filter({'question': question, 'candidate_exam_id': candidate_exam})

        question = models.QuestionClone.filter_instance({'pk': question_id}).first()

        response = dict()
        response['exam_name'] = candidate_exam.exam.exam_name
        response['exam_level'] = candidate_exam.exam.experience_level
        response['exam_details'] = candidate_serializer.CandidateExamSerializer(candidate_exam).data
        response['question'] = FilterByQuestionSerializer(question).data
        response['exam_remaining_time'] = candidate_services.update_candidate_exam_timer(candidate_exam)
        response['question_remaining_time'] = candidate_services.question_timer(question_id, exam_id)
        response['disqualify_count'] = candidate_exam.disqualify_count

        return response

    def get_answers(self, obj, **kwargs):
        question_id = self.context.get("question_id")
        exam_id = self.context.get("exam_id")
        candidate_id = self.context.get("candidate_id")
        candidate_exam = candidate_models.CandidateExam.get_instance({'exam__id': exam_id, 'candidate__id': candidate_id})
        return CandidateAnswerResultDetailsSerializer(obj, live_exam=True, context={'candidate_exam': candidate_exam}, many=True).data

    def update(self, instance, validated_data):
        """ updating disqualify_count"""

        return instance


class FilterByQuestionSerializer(serializers.ModelSerializer):
    """ filter by question serializer"""

    question_name = serializers.CharField(required=False)
    department = serializers.CharField(required=False)
    experience_level = serializers.CharField(required=False)
    time_duration = serializers.IntegerField(read_only=False)
    question_status = serializers.CharField(required=False)

    class Meta:
        model = models.QuestionClone
        fields = "__all__"


class CandidateExamQuestionsListSerializer(serializers.ModelSerializer):
    """ ExamQuestion serializer"""

    question = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.ExamQuestionClone
        fields = ['question', ]

    def get_question(self, obj):
        user_id = self.context.get("user_id")
        attempted_questions = candidate_services.attempted_question_list(obj.exam_id, user_id)

        response = []

        for q in obj.question.all():
            exist = attempted_questions.filter(question=q.id).exists()
            total_answers = models.QuestionAnswerClone.filter_instance({'question': q.id, 'is_correct': True}).count()
            multiple_answers = True if total_answers > 1 else False
            response.append({"question_id": q.id, "is_attempted": exist, "multiple_answers": multiple_answers})

        return response


class CandidateAnswerResultDetailsSerializer(serializers.ModelSerializer):
    """ Retrieve details of Candidate Answers from QuestionAnswerClone table"""

    is_selected = serializers.SerializerMethodField(read_only=True)

    def __init__(self, *args, **kwargs):
        live_exam = kwargs.pop('live_exam', None)
        super(CandidateAnswerResultDetailsSerializer, self).__init__(*args, **kwargs)

        if live_exam:
            self.fields.pop('is_correct')

    class Meta:
        model = models.QuestionAnswerClone
        fields = '__all__'

    def get_is_selected(self, obj):
        candidate_exam = self.context.get("candidate_exam")
        return candidate_services.is_answer_selected(obj.question.id, obj.id, candidate_exam)


class QuestionAnswerResultSerializer(serializers.ModelSerializer):
    """ Fetch question according the question_id,
    and their all the answers from the QuestionAnswer Table
    """
    question = serializers.SerializerMethodField(read_only=True)
    answer_text = serializers.SerializerMethodField(read_only=True)
    tags = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.QuestionClone
        fields = '__all__'

    def get_question(self, obj):
        dict_obj = {
            'question_id': obj.id,
            'question_name': obj.question_name,
            'time_duration': obj.time_duration,
            'experience_level': obj.experience_level
        }
        return dict_obj if obj else {}

    def get_tags(self, obj):
        question_tag_model = qt_model.QuestionTag
        tag = question_tag_model.filter_instance({'question_id': obj.id})
        qtag_serializer = qt_serializer.QTagListSerializer(tag, many=True)
        return qtag_serializer.data

    def get_answer_text(self, obj):
        answers = models.QuestionAnswerClone.filter_instance({'question': obj})

        dict_obj = {
           'answer_id': obj.id,
           'answers': CandidateAnswerResultDetailsSerializer(answers, context={'candidate_exam': self.context.get("candidate_exam")}, many=True).data,
        }
        return dict_obj if obj else {}


class CandidateAnswerScriptSerializer(serializers.ModelSerializer):
    """ Candidate Result Serializer"""
    question = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = candidate_models.CandidateAnswer
        fields = '__all__'

    def get_question(self, obj):
        return QuestionAnswerResultSerializer(obj.question, context={'candidate_exam': obj.candidate_exam_id}).data
