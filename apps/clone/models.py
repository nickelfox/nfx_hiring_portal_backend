from django.db import models
from common.models import BaseModel
from apps.user.models import Department
from apps.candidate import models as candidate_models
from apps.exam.models import Exam
from apps.question.models import Question
from rest_framework.exceptions import ValidationError


class QuestionClone(BaseModel):
    """Hold the cloned data from question and their id,
    duration of each question, exam name and department of the examiner"""

    status = [
        ('publish', 'PUBLISH'),
        ('unpublish', 'UNPUBLISH'),
        ('draft', 'DRAFT')
    ]
    level = [
        ('senior', 'SENIOR'),
        ('junior', 'JUNIOR'),
        ('internship', 'INTERNSHIP')
    ]

    question_name = models.CharField(max_length=255, blank=False, null=False)
    department = models.ForeignKey(Department, related_name='question_department_clone', on_delete=models.CASCADE)
    experience_level = models.CharField(max_length=255, blank=False, null=False, choices=level)
    question_status = models.CharField(max_length=255, blank=True, choices=status, null=True)
    time_duration = models.IntegerField(default=60)
    candidate_exam = models.ForeignKey(to="candidate.CandidateExam", related_name='candidate_exam_question', null=True,
                                       on_delete=models.CASCADE)

    def __str__(self):
        """ String representation of Question """
        return '{}-{}-{}'.format(self.question_name, self.department, self.experience_level)

    class Meta:
        """ Verbose name and plural Verbose name"""
        verbose_name = "Question Clone"
        verbose_name_plural = "Question Clone"
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create Question model """
        try:
            return QuestionClone.objects.create(**data)
        except QuestionClone.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get Question Details"""
        try:
            return QuestionClone.objects.get(**data)
        except QuestionClone.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance_or_none(data):
        """ Get Question Details or none"""
        try:
            return QuestionClone.objects.get(**data)
        except QuestionClone.DoesNotExist as e:
            return None

    @staticmethod
    def filter_instance(filters):
        """ Filters the questions with respected to level , status etc """
        return QuestionClone.objects.filter(**filters)


class QuestionAnswerClone(BaseModel):
    """Holds answers associated with each cloned question, their id, correctness."""
    question = models.ForeignKey(QuestionClone, related_name='answer_clone', on_delete=models.CASCADE)
    answer_text = models.TextField()
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        """ String representation of Answer"""
        return "{}-{}-{}-{}".format(self.pk, self.question.question_name, self.answer_text, self.is_correct)

    class Meta:
        """
        Verbose name and Verbose names plural
        """
        verbose_name = 'Question Answer Clone'
        verbose_name_plural = 'Question Answer Clone'
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create answers for the Questions """
        try:
            return QuestionAnswerClone.objects.create(**data)
        except QuestionAnswerClone.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get QuestionAnswer data with respect to ID """
        try:
            return QuestionAnswerClone.objects.get(**data)
        except QuestionAnswerClone.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Filter the Question with their answers according to question_id,"""
        return QuestionAnswerClone.objects.filter(**filters)


class ExamQuestionClone(BaseModel):
    """ Hold Exam id and Question id """
    exam = models.ForeignKey(Exam, related_name='exam_clone', on_delete=models.CASCADE)
    question = models.ManyToManyField(QuestionClone, related_name='question_clones', editable=False,)
    candidate_exam = models.ForeignKey(to="candidate.CandidateExam", related_name='exam_candidate_exam', null=True,
                                       on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Exam Question Clone"
        verbose_name_plural = "Exam Question Clone"
        ordering = ['-created_at']

    def __str__(self):
        return "%s (%s)" % (
            self.exam,
            ", ".join(ques.question_name for ques in self.question.all()),
        )

    @staticmethod
    def create_instances(data):
        """ Create new ExamQuestion data """
        try:
            return ExamQuestionClone.objects.create(**data)
        except ExamQuestionClone.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def create_or_get_instance(data):
        """ Retrieve the ExamQuestion data by its id """
        try:
            return ExamQuestionClone.objects.get(**data)
        except ExamQuestionClone.DoesNotExist as e:
            return ExamQuestionClone.objects.create(**data)

    @staticmethod
    def get_instance(data):
        """ Retrieve the ExamQuestion data by its id """
        try:
            return ExamQuestionClone.objects.get(**data)
        except ExamQuestionClone.DoesNotExist as e:
            return False

    @staticmethod
    def count_instances(filters):
        """ Count the number of questions in an Exam """
        try:
            return ExamQuestionClone.objects.filter(**filters).count()
        except ExamQuestionClone.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):

        """ Filter the ExamQuestion data w.r.t question_id or exam_id """
        try:
            return ExamQuestionClone.objects.filter(**filters)
        except ExamQuestionClone.DoesNotExist as e:
            return ValidationError(str(e))
