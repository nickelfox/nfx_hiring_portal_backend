from django.contrib import admin
from .models import Exam, ExamQuestion

class ExamQuestionAdmin(admin.ModelAdmin):
  readonly_fields=('question',)

# Register your models here.
admin.site.register(Exam)
admin.site.register(ExamQuestion, ExamQuestionAdmin)
