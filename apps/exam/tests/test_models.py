from apps.user import models as user_models
from apps.exam import models as exam_models
from apps.question import models as question_models
from apps.exam.tests import data
from apps.user.tests import data as user_data
from rest_framework.test import APITestCase
from common.constants import Constants


class ExamModel(APITestCase):
    """Generic Exam model test setup"""

    def setUp(self):
        # create role ,department , user and exam

        self.role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')
        self.user = user_models.User.objects.create(role=self.role, department=self.department,
                                                    **user_data.DataSet.registration_data())
        self.exam = exam_models.Exam.objects.create(created_by=self.user, department=self.department,
                                                    **data.DataSet.exam_data())

    def test_create_exam_model(self):
        """test model"""
        self.assertEqual(str(self.exam.id), data.DataSet.exam_data().get("id"))

    def test_exam_count(self):
        """test count model"""

        exams = exam_models.Exam.objects.all().count()
        self.assertEqual(exams, 1)

    def test_delete_exam_count(self):
        """test delete count"""
        self.exam.delete()
        exams = exam_models.Exam.objects.all().count()
        self.assertEqual(exams, 0)


class ExamQuestionModel(APITestCase):
    """Generic ExamQuestion model test setup"""

    def setUp(self):
        # create role ,department , user, exam and question

        self.role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')
        self.user = user_models.User.objects.create(role=self.role, department=self.department,
                                                    **user_data.DataSet.registration_data())
        self.exam = exam_models.Exam.objects.create(created_by=self.user, department=self.department,
                                                    **data.DataSet.exam_data())
        self.question_one = question_models.Question.objects.create(question_name="TEST QUESTION",
                                                                    department=self.department,
                                                                    experience_level="senior",
                                                                    question_status="publish",
                                                                    time_duration=60)
        self.question_two = question_models.Question.objects.create(question_name="TEST QUESTION 2",
                                                                    department=self.department,
                                                                    experience_level="senior",
                                                                    question_status="publish",
                                                                    time_duration=60)
        self.examquestion = exam_models.ExamQuestion.objects.create(exam=self.exam, **data.DataSet.exam_question_data())
        self.examquestion.question.add(self.question_one, self.question_two)

    def test_create_exam_question_model(self):
        """test model"""
        self.assertEqual(str(self.examquestion.id), data.DataSet.exam_question_data().get("id"))

    def test_exam_question_count(self):
        """test count model"""

        exam_question = exam_models.ExamQuestion.objects.all().count()
        self.assertEqual(exam_question, 1)

    def test_delete_exam_question_count(self):
        """test delete count"""
        self.examquestion.delete()
        exam_question = exam_models.ExamQuestion.objects.all().count()
        self.assertEqual(exam_question, 0)
