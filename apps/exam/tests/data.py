from django.utils import timezone


class DataSet:
    """The Dataset"""

    @staticmethod
    def exam_data():

        """exam dataset"""

        data = {
            "id": "8a3f6bf8-6e82-431a-91c8-9c97e1a8a1aa",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "exam_name": "TESTING EXAM",
            "experience_level": "senior",
            "duration": 10,
            "status": "publish"
        }
        return data

    @staticmethod
    def exam_question_data():

        """exam_question dataset"""

        data = {
            "id": "38f4c34e-18db-4e06-8060-8fdcb6999133",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
        }
        return data