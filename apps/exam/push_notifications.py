from apps.exam import models as exam_model
from common.constants import Constant
from notification import tasks


def exam_activities(exam_instance, notification_type):
    """
    Sender: Examiner
    Receiver: Examiner, Admin, Sub-Admin
    When: Exam Process
    """

    exam_details = get_job_details(exam_instance.id)

    notify_customer = [
        Constant.JOB_ADD,
        Constant.JOB_REMOVE,
        Constant.JOB_UPDATE,
    ]
    receiver = exam_details.created_by
    notification_args = {
        'job_id': str(exam_details.id),
        'department_id': str(exam_details.department.id),
        'status': exam_details.status,
        'experience_level': exam_details.experience_level,
    }

    if receiver:
        tasks.push_notification(notification_type, receiver, notification_args)


def get_job_details(exam_id):
    return exam_model.Exam.get_instance({'id': exam_id})
