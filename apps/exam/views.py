from rest_framework.response import Response
from rest_framework import views, status, parsers, generics
from . import serializers
from . import models
from apps.candidate.models import ExamStatus, CandidateExamStatus
from common import constants, utils, mail, permissions, pagination
from apps.exam.models import Exam, ExamQuestion
from apps.question.serializers import ExamQuestionAnswerListSerializer, QuestionListSerializer
from rest_framework.filters import SearchFilter
from apps.exam import filters as exam_filter
from apps.department import filters as department_filter
from common.constants import ApplicationMessages
from django.utils import timezone
from django.db.models import Sum, Count
from apps.clone import serailizers as clone_serializers


class ExamListCreateAPIView(generics.ListCreateAPIView):
    """ Create The Exam and Retrieve Details of the Exam """
    serializer_class = serializers.ExamListSerializer
    parser_classes = (parsers.JSONParser,)
    pagination_class = pagination.DefaultPagination
    permission_classes = (permissions.IsExaminer | permissions.IsSuperAdmin,)
    filter_backends = (SearchFilter, department_filter.DepartmentFilterByID, exam_filter.ExamFilterByLevel,
                       exam_filter.ExamFilterByStatus,)
    search_fields = ['exam_name', 'experience_level', 'status', ]
    exam_model = models.Exam

    def get_queryset(self):
        """ returned exam list"""
        return self.exam_model.filter_instance({'deleted_at__isnull': True})

    def post(self, request, *args, **kwargs):
        """ Create the Exam """
        exam_serializer = serializers.ExamCreateSerializer(data=request.data)
        if exam_serializer.is_valid():
            exam_serializer.save()
            return Response(exam_serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(exam_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        """ Returns list of the exams """
        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(self.paginate_queryset(filtered_queryset), many=True,
                                               context={"request": request})
            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.exam_model.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ExamEditDeleteAPIView(generics.ListAPIView):
    """ Update and Delete Specific Exam"""
    serializer_class = serializers.ExamCreateSerializer
    get_serializer_class = serializers.ExamListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsExaminer | permissions.IsSuperAdmin,)
    exam_model = models.Exam

    def get_queryset(self, id):
        """ Returned Exam Queryset"""
        return self.exam_model.get_instance({'id': id})

    def get(self, request, id, *args, **kwargs):
        """ Returns list of the exams """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset(id=id))
            serializer = self.get_serializer_class(filtered_queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except self.exam_model.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, exam_id, *args, **kwargs):
        """ Update the Exam """
        try:
            ce = CandidateExamStatus.objects.filter(candidate_exam__exam=exam_id)
            if ce:
                candidate_exam = CandidateExamStatus.objects.filter(candidate_exam__exam=exam_id).latest('created_at')
                if candidate_exam.exam_status.status == "PASSED" or candidate_exam.exam_status.status == "FAILED" or \
                        candidate_exam.exam_status.status == "LINK_NOT_SENT" or \
                        candidate_exam.exam_status.status == "DISQUALIFY":
                    queryset = self.exam_model.objects.get(id=exam_id, deleted_at__isnull=True)
                    serializer = self.serializer_class(queryset, data=request.data, partial=True)
                    if serializer.is_valid():
                        serializer.save()
                        return Response(serializer.data, status=status.HTTP_200_OK)
                else:
                    return Response(ApplicationMessages.EXAM_N_UPDATED, status=status.HTTP_400_BAD_REQUEST)
            else:
                queryset = self.exam_model.objects.get(id=exam_id, deleted_at__isnull=True)
                serializer = self.serializer_class(queryset, data=request.data, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
        except self.exam_model.DoesNotExist:
            return Response(ApplicationMessages.EXAM_NOT_EXIST, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, exam_id, *args, **kwargs):
        """ Delete an exam"""
        try:
            exam = self.exam_model.objects.get(id=exam_id, deleted_at__isnull=True)
            exam_question = ExamQuestion.filter_instance({'exam__id': exam_id, 'deleted_at__isnull': True})
            count = exam_question.count()
            if exam_question:
                return Response(ApplicationMessages.EXAM_QUESTION_DEPEND.format(count),
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)
            exam.deleted_at = timezone.now()
            exam.save()
            return Response(ApplicationMessages.EXAM_DELETED, status=status.HTTP_200_OK)
        except self.exam_model.DoesNotExist:
            return Response(ApplicationMessages.DELETE_NOT_DONE, status=status.HTTP_400_BAD_REQUEST)


class AddExamQuestionsAPIView(views.APIView):
    """ Select Questions from ExamQuestion model for create specific Exam"""
    serializer_classes = serializers.AddExamQuestionsSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsExaminer | permissions.IsSuperAdmin,)
    exam_model = models.Exam

    def get_queryset(self, exam_id):
        """ Returned Exam Queryset"""
        return self.exam_model.get_instance({'id': exam_id})

    def patch(self, request, exam_id, *args, **kwargs):
        """ Create Exam Questions"""
        ce = CandidateExamStatus.objects.filter(candidate_exam__exam=exam_id)
        if ce:
            candidate_exam = CandidateExamStatus.objects.filter(candidate_exam__exam=exam_id).latest('created_at')
            if candidate_exam.exam_status.status == "PASSED" or candidate_exam.exam_status.status == "FAILED" or \
                    candidate_exam.exam_status.status == "LINK_NOT_SENT" or \
                    candidate_exam.exam_status.status == "DISQUALIFY":
                serializer = self.serializer_classes(self.get_queryset(exam_id), data=request.data)
                if serializer.is_valid():
                    response = serializer.save()
                    return Response(response, status=status.HTTP_200_OK)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(ApplicationMessages.EXAM_N_UPDATED, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = self.serializer_classes(self.get_queryset(exam_id), data=request.data)
            if serializer.is_valid():
                response = serializer.save()
                return Response(response, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, exam_id, *args, **kwargs):
        try:
            exam_instance = ExamQuestion.create_or_get_instance({"exam": exam_id})
            ques = ExamQuestion.objects.annotate(num_questions=Count('question'))
            questions = request.data.pop('question')
            for q in ques:
                if q.exam.id == exam_id:

                    if q.num_questions == 1 or q.num_questions == len(questions):

                        for que in questions:
                            exam_instance.question.remove(que)
                            exam_instance.deleted_at = timezone.now()
                            exam_instance.save()
                        return Response({}, status=status.HTTP_200_OK)

                    elif q.num_questions == 0 or q.num_questions == len(questions):
                        exam_instance.deleted_at = timezone.now()
                        exam_instance.save()

                        return Response({}, status=status.HTTP_200_OK)

            # questions = request.data.pop('question')

            count = 0
            for que in questions:
                exam_instance.question.remove(que)
                count = count + 1

            exam = exam_instance.exam
            question_data = QuestionListSerializer(exam_instance.question, many=True).data
            exam_data = serializers.ExamListSerializer(exam).data
            time_duration = exam_instance.question.aggregate(total_time=Sum('time_duration'))

            if exam.experience_level.upper() == 'SENIOR':
                exam.duration = time_duration.get('total_time')
                exam.save()

                # Added this to update serializer total_time as well

                exam_data['duration'] = time_duration.get('total_time')

            exam_data['questions'] = question_data

            return Response(ApplicationMessages.EXAM_QUESTION_REMOVE.format(count), status=status.HTTP_200_OK)

        except self.exam_model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("Exam"), status=status.HTTP_404_NOT_FOUND)


class ExamQuestionListAPIView(generics.ListAPIView):
    """ Get Exam Questions list based on Exam_id """
    serializer_class = ExamQuestionAnswerListSerializer
    parser_classes = (parsers.JSONParser,)
    exam_question_model = ExamQuestion

    def get_queryset(self, exam_id):
        """ will returned Exam Questions list"""
        return self.exam_question_model.filter_instance({'exam': exam_id})

    def get(self, request, exam_id, *args, **kwargs):
        ce = CandidateExamStatus.objects.filter(candidate_exam__exam=exam_id)
        if ce:
            candidate_exam = CandidateExamStatus.objects.filter(candidate_exam__exam=exam_id).latest('created_at')
            if candidate_exam.exam_status.status == "PASSED" or candidate_exam.exam_status.status == "FAILED" or \
                    candidate_exam.exam_status.status == "LINK_NOT_SENT" or \
                    candidate_exam.exam_status.status == "DISQUALIFY":
                ongoing_exam = False
            else:
                ongoing_exam = True
            filtered_queryset = (self.filter_queryset(queryset=self.get_queryset(exam_id)))
            serializer = self.serializer_class(filtered_queryset, many=True)
            if not serializer.data:
                exam = Exam.get_instance({'id': exam_id})
                exam_dict = {
                    'exam_name': exam.exam_name,
                    'exam_id': exam.id,
                    'department': exam.department.name,
                    'department_id': exam.department.id,
                    'ongoing_exam': ongoing_exam
                }
                return Response(exam_dict, status=status.HTTP_200_OK)
            # new_dict = {'ongoing_exam': ongoing_exam}
            oe = {'ongoing_exam': ongoing_exam}
            new_dict = serializer.data
            new_dict.append(oe)
            return Response(new_dict, status=status.HTTP_200_OK)
        else:
            ongoing_exam = False
            filtered_queryset = (self.filter_queryset(queryset=self.get_queryset(exam_id)))
            serializer = self.serializer_class(filtered_queryset, many=True)
            if not serializer.data:
                exam = Exam.get_instance({'id': exam_id})
                exam_dict = {
                    'exam_name': exam.exam_name,
                    'exam_id': exam.id,
                    'department': exam.department.name,
                    'department_id': exam.department.id,
                    'ongoing_exam': ongoing_exam
                }
                return Response(exam_dict, status=status.HTTP_200_OK)
            # new_dict = {'ongoing_exam': ongoing_exam}
            oe = {'ongoing_exam': ongoing_exam}
            new_dict = serializer.data
            new_dict.append(oe)
            return Response(new_dict, status=status.HTTP_200_OK)


class ExamStatusListAPIView(generics.ListAPIView):
    """ List of the Exam Status"""
    parser_classes = (parsers.JSONParser,)
    serializer_class = serializers.ExamStatusSerializer
    pagination_class = pagination.DefaultPagination
    permission_classes = (permissions.IsExaminer | permissions.IsSuperAdmin | permissions.IsCandidate,)
    exam_status_model = ExamStatus

    def get_queryset(self):
        """ returned exam status list"""
        return self.exam_status_model.filter_instance({'deleted_at__isnull': True})

    def get(self, request, *args, **kwargs):
        """ will return list of the exam status """
        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(self.paginate_queryset(filtered_queryset), many=True,
                                               context={"request": request})
            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.exam_status_model.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ExamUndoDeleteAPIView(views.APIView):
    """ Undo Delete Specific Exam"""
    serializer_class = serializers.ExamListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsExaminer | permissions.IsSuperAdmin,)
    exam_model = models.Exam

    def get_gueryset(self, exam_id):
        """ Returned Exam Queryset"""
        return self.exam_model.filter_instance(exam_id)

    def patch(self, request, exam_id, *args, **kwargs):
        """ Update the Exam deleted_at field"""
        try:
            queryset = self.exam_model.objects.get(id=exam_id, deleted_at__isnull=False)
            serializer = self.serializer_class(queryset, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except self.exam_model.DoesNotExist:
            return Response(ApplicationMessages.EXAM_NOT_EXIST, status=status.HTTP_400_BAD_REQUEST)
