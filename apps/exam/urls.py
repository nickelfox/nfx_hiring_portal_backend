from django.urls import path
from . import views
import uuid

urlpatterns = [
    path('', views.ExamListCreateAPIView.as_view(), name='list-create-exam'),
    path('<uuid:exam_id>', views.ExamEditDeleteAPIView.as_view(), name='update-remove-exam'),
    path('<uuid:exam_id>/question/map/', views.AddExamQuestionsAPIView.as_view(), name='add-questions-for-exam'),
    path('<uuid:exam_id>/questions/', views.ExamQuestionListAPIView.as_view(), name='get-questions'),
    path('exam-status/', views.ExamStatusListAPIView.as_view(), name='exam-status-list'),
    path('<uuid:exam_id>/undo/', views.ExamUndoDeleteAPIView.as_view(), name='undo-delete-exam'),

 ]
