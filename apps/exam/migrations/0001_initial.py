# Generated by Django 3.0.6 on 2021-08-06 19:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('question', '0002_auto_20210805_2122'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Exam',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('exam_name', models.CharField(max_length=255)),
                ('experience_level', models.CharField(choices=[('senior', 'senior'), ('junior', 'junior'), ('internship', 'internship')], max_length=255)),
                ('duration', models.IntegerField()),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Exam',
                'verbose_name_plural': 'Exam',
            },
        ),
        migrations.CreateModel(
            name='ExamQuestion',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('exam_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exam', to='exam.Exam')),
                ('question_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='question', to='question.Question')),
            ],
            options={
                'unique_together': {('exam_id', 'question_id')},
            },
        ),
    ]
