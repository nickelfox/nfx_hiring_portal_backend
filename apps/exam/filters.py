from rest_framework.filters import BaseFilterBackend
import datetime


class ExamFilterByLevel(BaseFilterBackend):
    """
    Exam are filtered with respect to experience level
    """
    param = 'experience_level'

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(experience_level__in=param)
        return queryset


class ExamFilterByStatus(BaseFilterBackend):
    """
    Exam are filtered with respect to status
    """
    param = 'status'

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(status__in=param)
        return queryset


class ExamFilterByDate(BaseFilterBackend):
    """
    Exam are filtered with respect to created_at date
    """
    param = 'created_at'

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(exam__created_at__in=param)
        return queryset

