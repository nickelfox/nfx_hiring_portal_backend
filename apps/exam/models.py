from django.db import models
from common.models import BaseModel
from apps.user.models import Department, User
from apps.question.models import Question
from common.constants import Constants
from rest_framework.exceptions import ValidationError


class Exam(BaseModel):
    """ Holds information about the exams like Exam name, total time allocated, number of questions """
    exam_status = [
        ('publish', 'PUBLISH'),
        ('unpublish', 'UNPUBLISH'),
        ('draft', 'DRAFT')
    ]
    level = [
        ('senior', 'SENIOR'),
        ('junior', 'JUNIOR'),
        ('internship', 'INTERNSHIP')
    ]
    exam_name = models.CharField(max_length=255, blank=False, null=False)
    department = models.ForeignKey(Department, related_name='exam_department', on_delete=models.CASCADE, null=True)
    experience_level = models.CharField(max_length=255, blank=False, null=False, choices=level)
    status = models.CharField(max_length=255, blank=False, null=True, choices=exam_status)
    created_by = models.ForeignKey(User, related_name='user', on_delete=models.CASCADE)
    duration = models.IntegerField(default=Constants.EXAM_DURATION)

    def __str__(self):
        """
        String representation of Exam
        """
        return "{}-{}".format(self.exam_name, self.id)

    class Meta:
        """ verbose name verbose plural """
        verbose_name = 'Exam'
        verbose_name_plural = 'Exam'
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create new Exam"""
        try:
            return Exam.objects.create(**data)
        except Exam.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get an exam by their exam_id"""
        try:
            return Exam.objects.get(**data)
        except Exam.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Filter the exams according to the exam level, status etc"""
        return Exam.objects.filter(**filters)


class ExamQuestion(BaseModel):
    """ Hold Exam id and Question id """
    exam = models.ForeignKey(Exam, related_name='exam', on_delete=models.CASCADE)
    question = models.ManyToManyField(Question, related_name='question', editable=False,)

    class Meta:
        verbose_name = "Exam Question"
        verbose_name_plural = "Exam Questions"
        ordering = ['-created_at']

    def __str__(self):
        return "%s (%s)" % (
            self.exam,
            ", ".join(ques.question_name for ques in self.question.all()),
        )

    @staticmethod
    def create_instances(data):
        """ Create new ExamQuestion data """
        try:
            return ExamQuestion.objects.create(**data)
        except ExamQuestion.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def create_or_get_instance(data):
        """ Retrieve the ExamQuestion data by its id """
        try:
            return ExamQuestion.objects.get(**data)
        except ExamQuestion.DoesNotExist as e:
            return ExamQuestion.objects.create(**data)

    @staticmethod
    def get_instance(data):
        """ Retrieve the ExamQuestion data by its id """
        try:
            return ExamQuestion.objects.get(**data)
        except ExamQuestion.DoesNotExist as e:
            return False

    @staticmethod
    def count_instances(filters):
        """ Count the number of questions in an Exam """
        try:
            return ExamQuestion.objects.filter(**filters).count()
        except ExamQuestion.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):

        """ Filter the ExamQuestion data w.r.t question_id or exam_id """
        try:
            return ExamQuestion.objects.filter(**filters)
        except ExamQuestion.DoesNotExist as e:
            return ValidationError(str(e))


