from rest_framework import serializers
from .models import Exam, ExamQuestion
from apps.question.models import Question, QuestionAnswer
from apps.question.serializers import QuestionListSerializer
from django.db.models import Sum
from apps.candidate.models import ExamStatus, CandidateExam, CandidateExamStatus
from common.constants import ApplicationMessages
from rest_framework.exceptions import ValidationError


class ExamListSerializer(serializers.ModelSerializer):
    """ List of the Exams Serializer"""
    department = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Exam
        fields = '__all__'

    # def update(self, instance, validated_data):
    #     department_data = validated_data.pop("department")
    #     department = instance.department
    #
    #     for attr, value in department_data.items():
    #         setattr(department, attr, value)
    #     department.save()
    #     for attr, value in validated_data.items():
    #         setattr(instance, attr, value)
    #     instance.save()
    #     return instance

    def get_department(self, obj):
        if obj.department:
            return obj.department.name


class ExamCreateSerializer(serializers.ModelSerializer):
    """ Exam Create Serializer"""

    class Meta:
        model = Exam
        fields = '__all__'

    def create(self, validated_data):
        return Exam.create_instance(validated_data)

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance


class AddExamQuestionsSerializer(serializers.ModelSerializer):
    """ Selecting questions for creating exam"""

    question = serializers.ListField(required=True)

    class Meta:
        model = ExamQuestion
        fields = ('question',)

    def update(self, instance, validated_data):
        """ Updating questions for a particular exam"""
        exam_instance = ExamQuestion.create_or_get_instance({"exam": instance})
        questions = validated_data.pop('question')

        for que in questions:
            exam_instance.question.add(que)

        exam = exam_instance.exam
        question_data = QuestionListSerializer(exam_instance.question, many=True).data
        exam_data = ExamListSerializer(exam).data
        time_duration = exam_instance.question.aggregate(total_time=Sum('time_duration'))

        # if exam.experience_level.upper() == 'SENIOR':
        exam.duration = time_duration.get('total_time')
        exam.save()
        # Added this to update serializer total_time as well
        exam_data['duration'] = time_duration.get('total_time')

        exam_data['questions'] = question_data

        return {"exam": exam_data}


class ExamStatusSerializer(serializers.ModelSerializer):
    """ Exam Status Serializer"""

    class Meta:
        model = ExamStatus
        fields = '__all__'
