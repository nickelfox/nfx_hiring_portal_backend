from django.urls import path
from apps.job import views

urlpatterns = [
    path('', views.JobListCreateAPIVIew.as_view(), name="list-create-job"),
    path('<uuid:id>', views.JobEditDeleteAPIView.as_view(), name="updated-delete-unpublish-job"),
    path('<uuid:id>/profile/', views.JobProfileAPIView.as_view(), name="profile-job"),
    path('<uuid:id>/undo/', views.JobUndoDeleteAPIView.as_view(), name="undo-delete-job"),
]
