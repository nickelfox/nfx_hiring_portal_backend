from apps.user import models as user_models
from apps.job import models as job_models
from apps.exam import models as exam_models
from apps.exam.tests import data as exam_data, data
from apps.user.tests import data as user_data
from apps.job.tests import data as job_data, data
from rest_framework.test import APITestCase
from common.constants import Constants


class JobModel(APITestCase):
    """Generic JOB model test setup"""

    def setUp(self):
        # department ,user, exam associated ,and job
        self.role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')
        self.user = user_models.User.objects.create(role=self.role, department=self.department,
                                                    **user_data.DataSet.registration_data())
        self.exam_associated_one = exam_models.Exam.objects.create(created_by=self.user, department=self.department,
                                                                   exam_name='TESTING EXAM 1',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish")
        self.exam_associated_two = exam_models.Exam.objects.create(created_by=self.user, department=self.department,
                                                                   exam_name='TESTING EXAM 2',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish"
                                                                   )
        self.job = job_models.Job.objects.create(department=self.department,
                                                 **data.DataSet.job_data())
        self.job.exam_associated.add(self.exam_associated_two, self.exam_associated_two)

    def test_create_job_model(self):
        """Job model"""
        self.assertEqual(str(self.job.id), data.DataSet.job_data().get("id"))

    def test_job_count(self):
        """test count model"""

        jobs = job_models.Job.objects.all().count()
        self.assertEqual(jobs, 1)

    def test_delete_job_count(self):
        """test delete count"""
        self.job.delete()
        jobs = job_models.Job.objects.all().count()
        self.assertEqual(jobs, 0)





