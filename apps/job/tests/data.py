from django.utils import timezone


class DataSet:
    """The Dataset"""

    @staticmethod
    def job_data():
        """Job dataset"""
        data = {
            "id": "407625fd-9936-48ba-ae4e-858474ffc24b",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "job_title": "Quality Analyst 819",
            "job_level": "senior",
            "status": "publish",
            "experience": 7,
            "is_publish": True

        }
        return data

