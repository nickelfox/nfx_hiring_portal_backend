from rest_framework.response import Response
from rest_framework import views, status, parsers, generics
from . import serializers
from . import models as job_model
from common import utils, mail, permissions, pagination
from rest_framework.filters import SearchFilter
from apps.department import filters as department_filter
from apps.job import filters as job_filter
from common.constants import Constants, ApplicationMessages
from django.utils import timezone


class JobEditDeleteAPIView(views.APIView):
    """
    A specific Job update, delete by this api
    """
    serializer_class = serializers.JobCreateSerializer
    model = job_model.Job
    permission_classes = (permissions.IsSuperAdmin, )

    def get_queryset(self, id=None):
        if id:
            return job_model.Job.get_jobs(
                {'id': id})
        else:
            return job_model.Job.get_jobs(
                {"Job__details": self.request.Job__Job_title, })

    def patch(self, request, id, *args, **kwargs):

        """
        Update specific Job opening
        """

        try:
            job = self.model.objects.get(id=id, deleted_at__isnull=True)
            serializer = self.serializer_class(job, data=request.data, context={"request": request}, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data,  status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.JOB_NOT_EXISTS, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):

        """
        Delete specific job opening
        """
        try:
            job = self.model.objects.get(id=id, deleted_at__isnull=True)
            exam = job.exam_associated
            if exam.exists():
                return Response(ApplicationMessages.JOB_EXAM_DEPEND,
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)
            job.deleted_at = timezone.now()
            job.save()
            return Response(ApplicationMessages.JOB_DELETED, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DELETE_NOT_DONE, status=status.HTTP_400_BAD_REQUEST)


class JobListCreateAPIVIew(generics.ListAPIView):
    """
    Create job List using job_title,  department,  job_level, status, experience and
    also check if job already exists or not
    """
    serializer_class = serializers.JobListSerializer
    parser_classes = (parsers.JSONParser, )
    model = job_model.Job
    permission_classes = (permissions.IsSuperAdmin,)
    filter_backends = (SearchFilter, department_filter.DepartmentFilterByID, job_filter.JobFilterByLevel,
                       job_filter.JobFilterByStatus)
    pagination_class = pagination.DefaultPagination

    search_fields = ['job_title', 'job_level', 'status', 'created_at', ]

    def get_queryset(self):
        """
        Get job query_set
        """
        return self.model.get_jobs({'deleted_at__isnull': True})

    def get(self, request, *args, **kwargs):
        """
        Get the details of the EXAMINERS
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(self.paginate_queryset(filtered_queryset), many=True)
            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("JOB"),
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        """
        Add job data to job table
        """
        job_serializer = serializers.JobCreateSerializer(data=request.data)
        if job_serializer.is_valid():
            job_serializer.save()

            return Response(job_serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(job_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class JobProfileAPIView(generics.ListAPIView):
    """
    Retrieve the details of a particular Job profile
    """
    serializer_class = serializers.JobListSerializer
    parser_classes = (parsers.JSONParser, )
    model = job_model.Job
    permission_classes = (permissions.IsSuperAdmin,)

    def get_queryset(self, id):
        """
        Get job query_set
        """
        try:
            return self.model.get_instance({'id': id, 'deleted_at__isnull': True})
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.JOB_ALREADY_DELETED,
                            status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, id, *args, **kwargs):
        """
        Get the details of the Job
        :param request:
        :param id
        :param args:
        :param kwargs:
        :return:
        """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset(id=id))
            serializer = self.serializer_class(filtered_queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.DOES_NOT_EXISTS.format("JOB"),
                            status=status.HTTP_400_BAD_REQUEST)


class JobUndoDeleteAPIView(views.APIView):
    """
    A specific deleted Job will be undone by this api
    """
    serializer_class = serializers.JobCreateSerializer
    model = job_model.Job
    permission_classes = (permissions.IsSuperAdmin, )

    def get_queryset(self, id=None):
        if id:
            return job_model.Job.get_jobs(
                {'id': id})
        else:
            return job_model.Job.get_jobs(
                {"Job__details": self.request.Job__Job_title, })

    def patch(self, request, id, *args, **kwargs):

        """
        Update specific Job opening
        """

        try:
            job = self.model.objects.get(id=id, deleted_at__isnull=False)
            serializer = self.serializer_class(job, data=request.data, context={"request": request}, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data,  status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.JOB_NOT_EXISTS, status=status.HTTP_400_BAD_REQUEST)