# Generated by Django 3.0.6 on 2021-09-22 17:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0021_auto_20210909_1310'),
        ('job', '0013_auto_20210922_1718'),
    ]

    operations = [
        migrations.AlterField(
            model_name='job',
            name='exam_associated',
            field=models.ManyToManyField(blank=True, to='exam.Exam'),
        ),
    ]
