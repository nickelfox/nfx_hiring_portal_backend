# Generated by Django 3.0.6 on 2021-09-20 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0021_auto_20210909_1310'),
        ('job', '0010_job_is_publish'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='job',
            name='exam_associated',
        ),
        migrations.AddField(
            model_name='job',
            name='exam_associated',
            field=models.ManyToManyField(blank=True, to='exam.Exam'),
        ),
    ]
