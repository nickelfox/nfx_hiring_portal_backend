# Generated by Django 3.0.6 on 2021-08-18 14:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_verificationtoken_expiry_time'),
        ('job', '0002_auto_20210805_2122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='job',
            name='department',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job_department', to='user.Department'),
        ),
    ]
