from apps.job import models as job_model
from common.constants import Constants
from notifications import tasks


def job_activities(job_instance, notification_type, user):
    """
    Sender: Examiner
    Receiver: Examiner, Admin, Sub-Admin
    When: Job Process
    """

    job_details = get_job_details(job_instance.id)

    notify_customer = [
        Constants.JOB_ADD,
        Constants.JOB_REMOVE,
        Constants.JOB_UPDATE,
    ]
    receiver = user
    notification_args = {
        'job_id': str(job_details.id),
        'department_id': str(job_details.department.id),
        'status': job_details.status,
        'experience_level': job_details.experience_level,
    }

    if receiver:
        tasks.push_notification(notification_type, receiver, notification_args)


def get_job_details(job_id):
    return job_model.Job.get_instance({'id': job_id})
