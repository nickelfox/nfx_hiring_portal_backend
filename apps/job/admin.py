from django.contrib import admin
from apps.job import models as job_model

admin.site.register(job_model.Job)
