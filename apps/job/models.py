from django.db import models
from common.models import BaseModel
from apps.user.models import Department
from apps.exam.models import Exam
from rest_framework.exceptions import ValidationError
from common.constants import ApplicationMessages
from rest_framework.response import Response
from rest_framework import status


class Job(BaseModel):
    """
    Holds information about the jobs like skills required, job level, exam associated, job title etc
    """
    select_job_level = [
        ('senior', 'SENIOR'),
        ('junior', 'JUNIOR'),
        ('internship', 'INTERNSHIP')
    ]
    job_status = [
        ('publish', 'PUBLISH'),
        ('unpublish', 'UNPUBLISH'),
        ('draft', 'DRAFT')
    ]
    job_title = models.CharField(max_length=100, blank=False, null=False)
    department = models.ForeignKey(Department, related_name='job_department', on_delete=models.CASCADE,)
    job_level = models.CharField(max_length=100, blank=False, null=False, choices=select_job_level)
    status = models.CharField(max_length=100, blank=False, null=False, choices=job_status, default='unpublish')
    experience = models.FloatField(null=True, blank=True)
    exam_associated = models.ManyToManyField(Exam, blank=True)
    is_publish = models.BooleanField(default=False)

    def __str__(self):
        """
        String representation of Job
        :return:
        """
        return "{}-{}-{}-{}".format(self.id, self.job_title,  self.job_level, self.experience)

    class Meta:
        """
        Verbose name and Verbose names plural
        """
        verbose_name = 'Job'
        verbose_name_plural = 'Job'
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """
        Static method to create Job
        """

        try:
            instance = Job.objects.create(**data)
            return instance
        except Exception as e:
            raise ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """
        Static method to get Job instance
        """
        try:
            instance = Job.objects.get(**data)
            return instance
        except:
            raise Exception(ApplicationMessages.JOB_ALREADY_DELETED)

    @staticmethod
    def get_jobs(filters):
        """
        Static method to Filter Job
        """
        try:
            queryset = Job.objects.filter(**filters)
            return queryset
        except Job.DoesNotExist as ex:
            return ValidationError(str(ex))

    @staticmethod
    def update_or_create_instance(data):
        """
        Static method to Create new Job data with exam_associated data
        """

        exams = data.pop("exam_associated", None)
        instance = Job.objects.filter(**data).first()
        if instance is None:
            instance = Job.objects.create(**data)
            instance.exam_associated.add(*exams)
        else:
            instance.exam_associated.clear()
            instance.exam_associated.add(*exams)
        return instance
