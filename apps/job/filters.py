from rest_framework.filters import BaseFilterBackend


class JobFilterByLevel(BaseFilterBackend):
    """
    Job is filtered by the job_level
    """
    param = "job_level"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(job_level__in=param)
        return queryset


class JobFilterByStatus(BaseFilterBackend):
    """
    Job is filtered by the status
    """

    param = "status"

    def filter_queryset(self, request, queryset, view):
        param = request.query_params.getlist(self.param, None)
        if param:
            return queryset.filter(status__in=param)
        return queryset
