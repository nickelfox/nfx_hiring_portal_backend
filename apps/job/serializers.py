from rest_framework import serializers
from apps.job import models as job_models
from apps.job import push_notifications
from common.constants import Constants
from apps.exam.serializers import ExamListSerializer


class JobListSerializer(serializers.ModelSerializer):
    """ JobList Serializer"""
    department = serializers.SerializerMethodField(read_only=True)
    exam_associated = ExamListSerializer(many=True, read_only=True)

    class Meta:
        model = job_models.Job
        fields = "__all__"

    def get_department(self, obj):
        """ Retrieve department name"""

        if obj.department:
            department_dict = {
                "name": obj.department.name,
                "id": obj.department.id
            }
            return department_dict
        else:
            return {}


class JobCreateSerializer(serializers.ModelSerializer):
    """ Jobs Create Serializer"""

    class Meta:
        model = job_models.Job
        fields = '__all__'

    def create(self, validated_data):
        """ Job create method"""
        return job_models.Job.update_or_create_instance(validated_data)

    def update(self, instance, validated_data):
        """ Job update method """
        exam_associated = validated_data.pop("exam_associated", None)
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        if exam_associated is not None:
            instance.exam_associated.clear()
            instance.exam_associated.add(*exam_associated)
        return instance

