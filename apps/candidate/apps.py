from django.apps import AppConfig


class CandidateConfig(AppConfig):
    name = 'apps.candidate'
