# Generated by Django 3.0.6 on 2021-09-15 19:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('candidate', '0003_auto_20210914_1057'),
    ]

    operations = [
        migrations.AddField(
            model_name='candidateanswer',
            name='candidate_exam_id',
            field=models.ForeignKey(default=123, on_delete=django.db.models.deletion.CASCADE, related_name='candidate_exam_id', to='candidate.CandidateExam'),
            preserve_default=False,
        ),
    ]
