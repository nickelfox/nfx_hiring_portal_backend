from django.utils import timezone


class DataSet:
    """The Dataset"""

    @staticmethod
    def candidate_data():

        """Candidate dataset"""

        data = {
            "id": "e1f86969-674c-44fa-8b30-1988ebb833b1",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "experience": 2
        }
        return data

    @staticmethod
    def candidate_exam_data():
        """Candidate Exam dataset"""

        data = {
            "id": "93dd4593-96b8-4435-9714-2f6e021cfcd7",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "exam_link": 'http://www.jones.com/',
            "expiry_time": "2022-02-05",
            "exam_remaining_time": 3600
        }
        return data

    @staticmethod
    def candidate_exam_status_data():
        """Candidate Exam Status dataset"""

        data = {
            "id": "f0659621-43b6-435a-a1a3-b82ed087e54d",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,

        }
        return data

    @staticmethod
    def exam_status_data():
        """Candidate Exam Status dataset"""

        data = {
            "id": "2d43ce03-8231-41da-a6d9-4e4bd74681ea",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "status": "LINK_NOT_SENT"

        }
        return data

    @staticmethod
    def candidate_answer_data():
        """Candidate Answer dataset"""

        data = {
            "id": "3db9bd73-0eee-4163-b122-22493b2533ad",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "answer_status": "UNATTEMPTED",
            "question_remaining_time": 60

        }
        return data

    @staticmethod
    def candidate_question_answer_data():
        """question_answer dataset"""

        data = {
            "id": "96716b43-a524-47f5-b52b-405e735249a1",
            "created_at": timezone.now(),
            "updated_at": timezone.now(),
            "deleted_at": None,
            "answer_text": "TEST ANSWER",
            "is_correct": True
        }
        return data
