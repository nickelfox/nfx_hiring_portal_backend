from apps.user import models as user_models
from apps.job import models as job_models
from apps.job.tests import data as jobs_data
from apps.exam import models as exam_models
from apps.question import models as question_models
from apps.question.tests import data as question_data
from apps.exam.tests import data as exam_data
from apps.candidate.tests import data as candidates_data
from apps.user.tests import data as user_data
from rest_framework.test import APITestCase
from common.constants import Constants
from apps.candidate import models as candidate_models


class CandidateModel(APITestCase):
    """ Generic candidate Model SetUp"""

    def setUp(self):
        self.user_role = user_models.Role.objects.create(name=Constants.CANDIDATE, is_active=True)
        self.examiner_role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')
        self.user = user_models.User.objects.create(role=self.user_role, department=self.department,
                                                    **user_data.DataSet.registration_data())
        self.examiner = user_models.User.objects.create(role=self.examiner_role, department=self.department,
                                                        **user_data.DataSet.examiner_data())

        self.exam_associated_one = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 1',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish")
        self.exam_associated_two = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 2',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish"
                                                                   )
        self.job = job_models.Job.objects.create(department=self.department,
                                                 **jobs_data.DataSet.job_data())
        self.job.exam_associated.add(self.exam_associated_two, self.exam_associated_two)
        self.candidate = candidate_models.Candidate.objects.create(user=self.user, examiner=self.examiner,
                                                                   job=self.job, **candidates_data.DataSet.candidate_data())

    def test_create_candidate_model(self):
        """test model"""
        self.assertEqual(str(self.candidate.id), candidates_data.DataSet.candidate_data().get("id"))

    def test_candidate_count(self):
        """test count model"""

        candidates = candidate_models.Candidate.objects.all().count()
        self.assertEqual(candidates, 1)

    def test_delete_candidate_count(self):
        """test delete count"""
        self.candidate.delete()
        candidates = candidate_models.Candidate.objects.all().count()
        self.assertEqual(candidates, 0)


class CandidateExamModel(APITestCase):
    """ Generic candidate Exam  Model SetUp"""

    def setUp(self):
        self.user_role = user_models.Role.objects.create(name=Constants.CANDIDATE, is_active=True)
        self.examiner_role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')
        self.user = user_models.User.objects.create(role=self.user_role, department=self.department,
                                                    **user_data.DataSet.registration_data())
        self.examiner = user_models.User.objects.create(role=self.examiner_role, department=self.department,
                                                        **user_data.DataSet.examiner_data())

        self.exam_associated_one = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 1',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish")
        self.exam_associated_two = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 2',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish"
                                                                   )
        self.job = job_models.Job.objects.create(department=self.department,
                                                 **jobs_data.DataSet.job_data())
        self.job.exam_associated.add(self.exam_associated_two, self.exam_associated_two)
        self.candidate = candidate_models.Candidate.objects.create(user=self.user, examiner=self.examiner,
                                                                   job=self.job,
                                                                   **candidates_data.DataSet.candidate_data())
        self.exam = exam_models.Exam.objects.create(created_by=self.user, department=self.department,
                                                    **exam_data.DataSet.exam_data())
        self.candidate_exam = candidate_models.CandidateExam.objects.create(candidate=self.candidate,
                                                                            exam=self.exam,
                                                                            **candidates_data.
                                                                            DataSet.candidate_exam_data())

    def test_create_candidate_exam_model(self):
            """test model"""
            self.assertEqual(str(self.candidate_exam.id), candidates_data.DataSet.candidate_exam_data().get("id"))

    def test_candidate_exam_count(self):
        """test count model"""

        candidates = candidate_models.CandidateExam.objects.all().count()
        self.assertEqual(candidates, 1)

    def test_delete_candidate_exam_count(self):
        """test delete count"""
        self.candidate.delete()
        candidates = candidate_models.CandidateExam.objects.all().count()
        self.assertEqual(candidates, 0)


class CandidateExamStatusModel(APITestCase):
    """ Generic candidate Model SetUp"""

    def setUp(self):
        self.user_role = user_models.Role.objects.create(name=Constants.CANDIDATE, is_active=True)
        self.examiner_role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')
        self.user = user_models.User.objects.create(role=self.user_role, department=self.department,
                                                    **user_data.DataSet.registration_data())
        self.examiner = user_models.User.objects.create(role=self.examiner_role, department=self.department,
                                                        **user_data.DataSet.examiner_data())

        self.exam_associated_one = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 1',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish")
        self.exam_associated_two = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 2',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish"
                                                                   )
        self.job = job_models.Job.objects.create(department=self.department,
                                                 **jobs_data.DataSet.job_data())
        self.job.exam_associated.add(self.exam_associated_two, self.exam_associated_two)
        self.candidate = candidate_models.Candidate.objects.create(user=self.user, examiner=self.examiner,
                                                                   job=self.job,
                                                                   **candidates_data.DataSet.candidate_data())
        self.exam = exam_models.Exam.objects.create(created_by=self.user, department=self.department,
                                                    **exam_data.DataSet.exam_data())
        self.candidate_exam = candidate_models.CandidateExam.objects.create(candidate=self.candidate,
                                                                            exam=self.exam,
                                                                            **candidates_data.
                                                                            DataSet.candidate_exam_data())
        self.exam_status = candidate_models.ExamStatus.objects.create(**candidates_data.DataSet.
                                                                      candidate_exam_status_data())
        self.candidate_exam_status = candidate_models.CandidateExamStatus.objects.create(candidate_exam=
                                                                                         self.candidate_exam,
                                                                                         exam_status=self.exam_status,
                                                                                         **candidates_data.DataSet.
                                                                                         candidate_exam_status_data()
                                                                                         )

    def test_create_candidate_exam_status_model(self):
        """test model"""
        self.assertEqual(str(self.candidate_exam_status.id), candidates_data.DataSet.candidate_exam_status_data().get("id"))

    def test_candidate_exam_status_count(self):
        """test count model"""

        candidates = candidate_models.CandidateExamStatus.objects.all().count()
        self.assertEqual(candidates, 1)

    def test_delete_candidate_exam_status_count(self):
        """test delete count"""
        self.candidate.delete()
        candidates = candidate_models.CandidateExamStatus.objects.all().count()
        self.assertEqual(candidates, 0)


class CandidateAnswerModel(APITestCase):
    """ Generic candidate Answer Model SetUp """

    def setUp(self):
        self.user_role = user_models.Role.objects.create(name=Constants.CANDIDATE, is_active=True)
        self.examiner_role = user_models.Role.objects.create(name=Constants.EXAMINER, is_active=True)
        self.department = user_models.Department.objects.create(name='HR')
        self.user = user_models.User.objects.create(role=self.user_role, department=self.department,
                                                    **user_data.DataSet.registration_data())
        self.examiner = user_models.User.objects.create(role=self.examiner_role, department=self.department,
                                                        **user_data.DataSet.examiner_data())

        self.exam_associated_one = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 1',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish")
        self.exam_associated_two = exam_models.Exam.objects.create(created_by=self.examiner, department=self.department,
                                                                   exam_name='TESTING EXAM 2',
                                                                   experience_level='senior',
                                                                   duration=3600,
                                                                   status="publish"
                                                                   )
        self.job = job_models.Job.objects.create(department=self.department,
                                                 **jobs_data.DataSet.job_data())
        self.job.exam_associated.add(self.exam_associated_two, self.exam_associated_two)
        self.candidate = candidate_models.Candidate.objects.create(user=self.user, examiner=self.examiner,
                                                                   job=self.job,
                                                                   **candidates_data.DataSet.candidate_data())
        self.exam = exam_models.Exam.objects.create(created_by=self.user, department=self.department,
                                                    **exam_data.DataSet.exam_data())
        self.candidate_exam = candidate_models.CandidateExam.objects.create(candidate=self.candidate,
                                                                            exam=self.exam,
                                                                            **candidates_data.
                                                                            DataSet.candidate_exam_data())
        self.question = question_models.Question.objects.create(department=self.department,
                                                                **question_data.DataSet.question_data())
        self.answer_choice_one = question_models.QuestionAnswer.objects.create(question=self.question,
                                                                               **question_data.DataSet.
                                                                               question_answer_data())
        self.answer_choice_two = question_models.QuestionAnswer.objects.create(question=self.question,
                                                                               **candidates_data.DataSet.
                                                                               candidate_question_answer_data())
        self.candidate_answer = candidate_models.CandidateAnswer.objects.create(candidate_exam_id=self.candidate_exam,
                                                                                question=self.question,
                                                                                **candidates_data.DataSet.
                                                                                candidate_answer_data())
        self.candidate_answer.answer_choice.add(self.answer_choice_one, self.answer_choice_two)

    def test_create_candidate_answer_model(self):
        """test model"""
        self.assertEqual(str(self.candidate_answer.id),
                         candidates_data.DataSet.candidate_answer_data().get("id"))

    def test_candidate_answer_count(self):
        """test count model"""

        candidates = candidate_models.CandidateAnswer.objects.all().count()
        self.assertEqual(candidates, 1)

    def test_delete_candidate_exam_status_count(self):
        """test delete count"""
        self.candidate.delete()
        candidates = candidate_models.CandidateAnswer.objects.all().count()
        self.assertEqual(candidates, 0)
