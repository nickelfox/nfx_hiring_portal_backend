from django.db import models
from rest_framework.exceptions import ValidationError
from common.models import BaseModel
from apps.job.models import Job
from apps.user.models import Department, User
from apps.exam.models import Exam
from apps.clone.models import QuestionClone, QuestionAnswerClone


class ExamStatus(BaseModel):
    """ for all Exam status """

    status = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        """ string representation of status"""
        return "{}".format(self.status)

    class Meta:
        """ verbose name and verbose plural name"""
        verbose_name = 'ExamStatus',
        verbose_name_plural = 'ExamStatus'
        ordering = ['-created_at']

    @staticmethod
    def get_instance(data):
        """ get All list of the exam status"""
        try:
            return ExamStatus.objects.get(**data)
        except ExamStatus.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Exam status list"""
        return ExamStatus.objects.filter(**filters)


class Candidate(BaseModel):
    """ Candidate details  """
    user = models.ForeignKey(User, related_name='candidate_user', on_delete=models.CASCADE, )
    examiner = models.ForeignKey(User, related_name='candidate_examiner', on_delete=models.CASCADE,
                                 null=True, blank=True)
    job = models.ForeignKey(Job, related_name='job', on_delete=models.CASCADE)
    experience = models.FloatField(null=True, blank=True)
    is_hired = models.BooleanField(null=True, blank=True)

    def __str__(self):
        """
        String representation of Candidate model
        """
        return "{}".format(self.user)

    class Meta:
        """ verbose name and verbose plural name"""
        verbose_name = 'Candidate',
        verbose_name_plural = 'Candidate'
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create new Candidate"""
        try:
            return Candidate.objects.create(**data)
        except Candidate.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get a candidate based on the id"""

        try:
            return Candidate.objects.get(**data)
        except Candidate.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def filter_instance(filters):
        """ Filter the candidates according to the status, department, examiner"""
        return Candidate.objects.filter(**filters)


class CandidateExam(BaseModel):
    """ CandidateExam details  """
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    exam_link = models.CharField(max_length=255, unique=True)
    expiry_time = models.DateTimeField(null=True, blank=True)
    exam_remaining_time = models.IntegerField(null=True, blank=True, default=None)
    disqualify_count = models.IntegerField(default=False)

    def __str__(self):
        """ String representation of CandidateExam  """
        return "{}-{}-{}".format(self.exam, self.candidate, self.exam_link)

    class Meta:
        """ verbose name and verbose plural name"""
        verbose_name = 'CandidateExam',
        verbose_name_plural = 'CandidateExam'

    @staticmethod
    def create_instance(data):
        """ Create new CandidateExam data"""
        try:
            return CandidateExam.objects.create(**data)
        except CandidateExam.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get a data of CandidateExam based on ID """
        try:
            return CandidateExam.objects.get(**data)
        except CandidateExam.DoesNotExist:
            return None

    @staticmethod
    def filter_instance(filters):
        """ Filter the CandidateExam model according to the status, department, examiner"""
        return CandidateExam.objects.filter(**filters)


def get_link_not_sent_status():
    return ExamStatus.objects.filter(status='LINK_NOT_SENT').first()


class CandidateExamStatus(BaseModel):
    """ CandidateExamStatus details  """
    candidate_exam = models.ForeignKey(CandidateExam, related_name='candidate_exam',
                                       on_delete=models.CASCADE, blank=True, null=True)

    exam_status = models.ForeignKey(ExamStatus, related_name='candidate_exam_status',
                                    on_delete=models.CASCADE, default=get_link_not_sent_status)

    def __str__(self):
        """ String representation of CandidateExamStatus"""
        return '{}-{}'.format(self.candidate_exam, self.exam_status)

    class Meta:
        """ verbose name and verbose plural name"""
        verbose_name = 'CandidateExamStatus',
        verbose_name_plural = 'CandidateExamStatus'
        ordering = ['-created_at']
        unique_together = ('candidate_exam', 'exam_status',)


    @staticmethod
    def create_instance(data):
        """ Create new CandidateExamStatus data"""

        try:
            return CandidateExamStatus.objects.create(**data)

        except CandidateExamStatus.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance(data):
        """ Get the list of the CandidateExamStatus data"""
        try:
            return CandidateExamStatus.objects.get(**data)
        except CandidateExamStatus.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def get_instance_or_none(data):
        """ Get the list of the CandidateExamStatus data"""
        try:
            return CandidateExamStatus.objects.get(**data)
        except CandidateExamStatus.DoesNotExist as e:
            return None

    @staticmethod
    def filter_instance(filters):
        """ filter the CandidatesExamStatus according to exam_id, candidate_id"""
        return CandidateExamStatus.objects.filter(**filters)


class CandidateAnswer(BaseModel):
    """ This model will carry the answers given by Candidate and check its validity (whether its correct or not)"""

    CORRECT = 'CORRECT'
    WRONG = 'WRONG'
    UNATTEMPTED = 'UNATTEMPTED'

    STATUS = (
        (CORRECT, 'Correct'),
        (WRONG, 'Wrong'),
        (UNATTEMPTED, 'Unattempted'),
    )

    candidate_exam_id = models.ForeignKey(CandidateExam, related_name='candidate_exam_id', on_delete=models.CASCADE)
    question = models.ForeignKey(QuestionClone, related_name='candidate_question', on_delete=models.SET_NULL, null=True)
    answer_choice = models.ManyToManyField(QuestionAnswerClone)
    answer_status = models.CharField(max_length=255, choices=STATUS, default=UNATTEMPTED)
    question_remaining_time = models.IntegerField(null=True, blank=True, default=None)

    def __str__(self):
        """ String representation of Candidate-Answer"""

        return '{}-{}'.format(self.candidate_exam_id, self.answer_choice)

    class Meta:
        """ verbose name and verbose plural name"""

        verbose_name = 'CandidateAnswer',
        verbose_name_plural = 'CandidateAnswer'
        ordering = ['-created_at']

    @staticmethod
    def create_instance(data):
        """ Create new CandidateAnswer instance"""

        try:
            return CandidateAnswer.objects.create(**data)
        except CandidateAnswer.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def update_or_create_instance(data):
        """ Create new CandidateAnswer data and update"""

        answers = data.pop("answer_choice", None)
        instance = CandidateAnswer.objects.filter(**data).first()
        if instance is None:
            instance = CandidateAnswer.objects.create(**data)
            instance.answer_choice.add(*answers)
        else:
            instance.answer_choice.clear()
            instance.answer_choice.add(*answers)
        return instance

    @staticmethod
    def get_instance(data):
        """ Get the CandidateAnswer data according to id"""
        try:
            return CandidateAnswer.objects.get(**data)
        except CandidateAnswer.DoesNotExist as e:
            return ValidationError(str(e))

    @staticmethod
    def create_or_filter(data):
        """ Get the CandidateAnswer data according to id"""
        instance = CandidateAnswer.objects.filter(**data).first()

        if instance is None:
            instance = CandidateAnswer.objects.create(**data)
        return instance

    @staticmethod
    def filter_instance(filters):
        """ Filter the CandidateAnswer according to exam_id, candidate_id"""
        return CandidateAnswer.objects.filter(**filters)
