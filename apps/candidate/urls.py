from django.urls import path
from apps.candidate import views
import uuid

urlpatterns = [
    path('', views.CandidateListAPIVIew.as_view(), name='candidate-list-search'),
    path('<uuid:id>/', views.CandidateProfileAPIView.as_view(), name='candidate-profile'),
    path('result/', views.CandidateResultListAPIVIew.as_view(), name='candidate-result-list'),
    path('exam-status/', views.CandidateExamStatusAPiVIew.as_view(), name='candidate-exam-status'),
    path('exam/', views.CandidateExamAPIView.as_view(), name='candidate-exam'),
    path('exam/<uuid:id>/', views.CandidateListByExamAPIView.as_view(), name='candidate-list-per-exam'),
    path('<uuid:candidate_id>/exam/<uuid:exam_id>/result/', views.CandidateResultAPIView.as_view(), name='candidate-result'),
    path('answers/', views.CandidateExamAnswersAPIView.as_view(), name='candidate-answers-save'),
    path('exam/<uuid:candidate_exam_id>/status/', views.CandidateExamStatusEditAPIView.as_view(),
         name='candidate-exam-status-update'),
    path('<uuid:id>/undo/', views.UndoDeleteAPIView.as_view(), name='undo-delete-candidate'),
    path('<uuid:id>/result/overview', views.CandidateAnswerScriptAPIView.as_view(), name='candidate-answer-script'),

    path('exam/<uuid:exam_id>/questions/<uuid:candidate_exam>', views.CandidateExamQuestionListAPIView.as_view(),
         name='candidate-exam-questions-list'),

    path('exam/<uuid:exam_id>/questions/', views.ExamQuestionListAPIView.as_view(),
         name='exam-questions-list'),
    path('question/<uuid:question_id>/exam/<uuid:exam_id>/', views.CandidateQuestionAPIView.as_view(),
         name='candidate-question'),
    path('question/<uuid:question_id>/exam/<uuid:exam_id>/timer/', views.CandidateQuestionExamTimerAPIView.as_view(),
         name='candidate-question'),
    path('<uuid:candidate_id>/exam/<uuid:exam_id>/', views.CandidateExamIDAPIView.as_view(), name='candidate-exam-id'),
    path('resend-link/', views.ResendExamLinkAPIView.as_view(), name='resend-exam-link'),
 ]
