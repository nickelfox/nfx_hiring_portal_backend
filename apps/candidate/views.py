from django.utils import timezone
from rest_framework import views, status, parsers, generics
from rest_framework.response import Response
from apps.candidate import serializers as candidate_serializers
from apps.candidate import models as candidate_models
from apps.candidate.models import Candidate, CandidateAnswer, CandidateExam, CandidateExamStatus
from apps.question.models import QuestionAnswer
from common import constants, permissions, pagination
from rest_framework.filters import SearchFilter
from apps.candidate import filters as candidate_filters
from rest_framework.validators import ValidationError
from apps.candidate import auth
from apps.user.models import User
from apps.candidate import services as candidate_services
from apps.exam import models as exam_models
from common.constants import ApplicationMessages, Constants
from django.db import transaction
from common import mail
from nhp import settings
from apps.clone.models import QuestionAnswerClone, QuestionClone, ExamQuestionClone
from apps.clone import serailizers as clone_serializers


class CandidateLoginAPIView(views.APIView):
    serializer_class = candidate_serializers.CandidateLoginSerializer
    candidate_models = Candidate

    def get_queryset(self, candidate_id):
        """ Will return the User instance if exist """

        try:
            candidate = Candidate.get_instance({'id': candidate_id})
            return User.get_instance({'id': candidate.user.id, 'is_active': True})
        except Exception as e:
            raise ValidationError("User with this ID Does not exist")

    def post(self, request, *args, **kwargs):
        """ Create candidate login and session"""

        request.data['candidate_id'] = request.data.get('candidate_id')
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            candidate_instance = self.get_queryset(request.data.get('candidate_id'))
            if not candidate_instance:
                return Response(ApplicationMessages.USER_NOT_ACTIVE, status=status.HTTP_401_UNAUTHORIZED)
            data = auth.IDAuthManager.login_user_id(candidate_instance,
                                                    data=serializer.validated_data)

            if data:
                return Response(data, status=status.HTTP_200_OK)
            else:
                return Response(ApplicationMessages.CANDIDATE_ID_INCORRECT,
                                status=status.HTTP_401_UNAUTHORIZED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CandidateListAPIVIew(generics.ListCreateAPIView):
    """ Get All the Candidates List and Create New Candidate """
    serializer_class = candidate_serializers.CandidateCreateSerializer
    serializer_class_list = candidate_serializers.CandidateListSerializer
    parser_classes = (parsers.JSONParser,)
    pagination_class = pagination.DefaultPagination
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    filter_backends = (SearchFilter,
                       candidate_filters.CandidateFilterDepartment,
                       candidate_filters.CandidateFilterExaminer,
                       candidate_filters.FilterByHire)
    search_fields = ['user__full_name', 'user__email', 'user__phone_number', 'updated_at',
                     'experience', 'user__department__name', 'examiner__full_name']

    candidate_model = candidate_models.Candidate

    def get_queryset(self, exam_status):

        """ Will returned list of the candidates"""
        return self.candidate_model.filter_instance({"deleted_at__isnull": True})

    def get(self, request, *args, **kwargs):
        """ Get list of the  Candidates  """
        exam_status = request.query_params.get('candidate_status', None)
        attempt_level = request.query_params.get('attempt_level', None)
        exam_level = request.query_params.get('exam_level', None)

        filtered_queryset = self.filter_queryset(queryset=self.get_queryset(exam_status))
        serializer = self.serializer_class_list(filtered_queryset, many=True, context={"request": request})
        if attempt_level and exam_status and exam_level:
            data = [result for result in serializer.data if result["attempt_level"] == attempt_level and
                    result.get("latest_status", {}).get("candidate_status") == exam_status and
                    result.get("exam", {}).get("exam_level") == exam_level]
        elif attempt_level and exam_status:
            data = [result for result in serializer.data if result["attempt_level"] == attempt_level and
                    result.get("latest_status", {}).get("candidate_status") == exam_status]
        elif exam_status and exam_level:
            data = [result for result in serializer.data
                    if result.get("latest_status", {}).get("candidate_status") == exam_status and
                    result.get("exam", {}).get("exam_level") == exam_level]
        elif attempt_level and exam_level:
            data = [result for result in serializer.data if result["attempt_level"] == attempt_level and
                    result.get("exam", {}).get("exam_level") == exam_level]
        elif attempt_level:
            data = [result for result in serializer.data if result["attempt_level"] == attempt_level]
        elif exam_status:
            data = [result for result in serializer.data if
                    result.get("latest_status", {}).get("candidate_status") == exam_status]
        elif exam_level:
            data = [result for result in serializer.data if
                    result.get("exam", {}).get("exam_level") == exam_level]
        else:
            data = serializer.data
        return Response(data=pagination.generate_pagination(data, request),
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        """ Create New candidate"""

        serializer = candidate_serializers.CandidateCreateSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CandidateProfileAPIView(views.APIView):
    """ get particular candidate profile details and Update"""
    serializers_class = candidate_serializers.CandidateListSerializer
    serializers_class_update = candidate_serializers.CandidateCreateSerializer
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer | permissions.IsCandidate,)
    parser_classes = (parsers.JSONParser,)
    candidate_model = candidate_models.Candidate

    def get(self, request, id, *args, **kwargs):
        """ Candidate Profile view """
        try:
            queryset = candidate_models.Candidate.objects.get(id=id, deleted_at__isnull=True)
            serializer = self.serializers_class(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except candidate_models.Candidate.DoesNotExist:
            return Response(constants.ApplicationMessages.CANDIDATE_NOT_EXIST, status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, id, *args, **kwargs):
        """ Edit the Candidate profile by the Admin/HR"""

        try:
            queryset = candidate_models.Candidate.objects.get(id=id, deleted_at__isnull=True)
            serializer = self.serializers_class_update(queryset, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except candidate_models.Candidate.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    @transaction.atomic()
    def delete(self, request, id, *args, **kwargs):
        """ delete specific candidate profile"""
        try:
            candidate = candidate_models.Candidate.objects.get(id=id, deleted_at__isnull=True)
            user = User.get_instance({'id': candidate.user.id})
            candidate.deleted_at = timezone.now()
            user.deleted_at = timezone.now()
            candidate.save()
            user.save()
            candidate_exam_qs = CandidateExam.filter_instance({'candidate': id})
            if candidate_exam_qs:
                for candidate_exam in candidate_exam_qs:
                    candidate_exam.deleted_at = timezone.now()
                    candidate_exam.save()
                    candidate_exam_status = CandidateExamStatus.filter_instance({'candidate_exam': candidate_exam})
                    if candidate_exam_status:
                        for candidate_es in candidate_exam_status:
                            candidate_es.deleted_at = timezone.now()
                            candidate_es.save()

            return Response(constants.ApplicationMessages.CANDIDATE_DELETED, status=status.HTTP_200_OK)
        except self.candidate_model.DoesNotExist:
            return Response(constants.ApplicationMessages.DELETE_NOT_DONE, status=status.HTTP_404_NOT_FOUND)


class CandidateResultListAPIVIew(generics.ListAPIView):
    """ Get All the Candidates List and Create New Candidate """
    serializer_class = candidate_serializers.CandidateResultListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    filter_backends = (SearchFilter,
                       candidate_filters.CandidateFilterExaminerDepartment,
                       candidate_filters.CandidateFilterByLevel,
                       )
    search_fields = ['candidate_exam__candidate__user__full_name', ]
    candidate_model = candidate_models.CandidateExamStatus

    def get_queryset(self):
        """ will returned list of the exams and the candidates attempting it """
        return self.candidate_model.filter_instance({'deleted_at__isnull': True,
                                                     'exam_status__status': Constants.CANDIDATE_EXAM_SUBMIT})

    def get(self, request, *args, **kwargs):
        """ get list of the  exams  """
        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(filtered_queryset, many=True, context={"request": request})
            data = [result for result in serializer.data if result['candidate']]
            return Response(pagination.generate_pagination(data, request), status=status.HTTP_200_OK)
        except self.candidate_model.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class CandidateExamStatusAPiVIew(generics.ListCreateAPIView):
    """ stores candidate exam status for time line"""
    serializer_class = candidate_serializers.CandidateExamStatusSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer | permissions.IsCandidate,)
    pagination_class = pagination.DefaultPagination
    filter_backends = (SearchFilter, candidate_filters.CandidateFilterByExamStatus,)
    search_fields = []
    candidate_exam_status_model = candidate_models.CandidateExamStatus

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

    def get_queryset(self):
        """ will returned list of the candidates"""
        return self.candidate_exam_status_model.filter_instance({'deleted_at__isnull': True})

    def get(self, request, *args, **kwargs):
        """ get list of the  Candidates  """
        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset())
            serializer = self.serializer_class(self.paginate_queryset(filtered_queryset), many=True,
                                               context={"request": request})
            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except self.candidate_exam_status_model.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class CandidateExamAPIView(views.APIView):
    """Create Candidate Exam """
    serializer_class = candidate_serializers.CandidateExamSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    candidate_exam_model = candidate_models.CandidateExam

    def post(self, request, *agrs, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(ApplicationMessages.CANDIDATE_EXAM_LINK, status=status.HTTP_400_BAD_REQUEST)


class CandidateListByExamAPIView(generics.ListAPIView):
    """ Retrieve Candidate list and details for a particular Exam"""
    serializers_class = candidate_serializers.CandidateListExamSerializer
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    pagination_class = pagination.DefaultPagination
    parser_classes = (parsers.JSONParser,)
    candidate_model = CandidateExam
    filter_backends = (SearchFilter,)
    search_fields = ['candidate__user__full_name', 'exam__created_at',
                     'created_at', ]

    def get_queryset(self, id):
        """ will returned list candidates for a particular exam_id """
        return self.candidate_model.filter_instance({'deleted_at__isnull': True,
                                                     'exam__id': id, 'candidate__deleted_at__isnull': True})

    def get(self, request, id, *args, **kwargs):
        """ Candidate List view """

        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset(id))
            serializer = self.serializers_class(self.paginate_queryset(filtered_queryset), context={"request": request})
            return Response(data=serializer.data, status=status.HTTP_200_OK)

        except Candidate.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class CandidateResultAPIView(generics.ListAPIView):
    """ Retrieve Candidate list and details for a particular Exam"""
    serializers_class = candidate_serializers.CandidateResultSerializer
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    pagination_class = pagination.DefaultPagination
    parser_classes = (parsers.JSONParser,)
    candidate_model = CandidateExam

    def get_queryset(self, candidate_id, exam_id):
        """ will returned details of candidates and exam for a particular candidate id """
        return self.candidate_model.filter_instance(
            {'deleted_at__isnull': True, 'candidate': candidate_id, 'exam': exam_id})

    def get(self, request, *args, **kwargs):
        """ Candidate and Exam details view """

        try:
            candidate_id = kwargs.get('candidate_id', None)
            exam_id = kwargs.get('exam_id', None)
            serializer = self.serializers_class(self.get_queryset(candidate_id=candidate_id, exam_id=exam_id),
                                                many=True,
                                                context={"request": request})
            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)
        except Candidate.DoesNotExist:
            return Response(ApplicationMessages.CANDIDATE_ID_INCORRECT, status=status.HTTP_400_BAD_REQUEST)


class CandidateExamAnswersAPIView(views.APIView):
    """ Save candidate specific Exam Answers in CandidateAnswerModel"""
    serializer_class = candidate_serializers.CandidateSpecificExamAnswersSerializer
    permission_classes = (permissions.IsCandidate,)

    def post(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class CandidateExamStatusEditAPIView(generics.ListAPIView):
    """ Update and Delete Specific Exam"""
    serializer_class = candidate_serializers.CandidateExamStatusUpdateSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsExaminer | permissions.IsSuperAdmin | permissions.IsCandidate,)
    model = candidate_models.CandidateExamStatus

    def patch(self, request, candidate_exam_id, *args, **kwargs):
        """ Update the CandidateExamStatus """
        try:
            queryset = self.model.objects.filter(candidate_exam=candidate_exam_id, deleted_at__isnull=True).first()
            serializer = self.serializer_class(queryset, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors)
        except self.model.DoesNotExist:
            return Response(ApplicationMessages.CANDIDATE_EXAM_ID_INCORRECT, status=status.HTTP_400_BAD_REQUEST)


class UndoDeleteAPIView(views.APIView):
    """
    Undo the deletion of a candidate from database.
    """
    serializer_class = candidate_serializers.CandidateListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    candidate_model = candidate_models.Candidate

    def patch(self, request, id, *args, **kwargs):
        """ Undo the delete action, change the deleted_at field to NULL again  """
        try:

            candidate = self.candidate_model.objects.get(id=id)
            user = User.get_instance({'id': candidate.user.id})
            user.deleted_at = None
            user.save()
            serializer = self.serializer_class(candidate, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
        except self.candidate_model.DoesNotExist:
            return Response(ApplicationMessages.CANDIDATE_NOT_EXIST, status=status.HTTP_404_NOT_FOUND)


class CandidateAnswerScriptAPIView(generics.ListAPIView):
    """ Retrieve Candidate list and details for a particular Exam"""
    serializers_class = clone_serializers.CandidateAnswerScriptSerializer
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    pagination_class = pagination.DefaultPagination
    parser_classes = (parsers.JSONParser,)
    candidate_answer_model = CandidateAnswer

    def get_queryset(self, id):
        """ will returned details of candidates and exam for a particular candidate id """
        return self.candidate_answer_model.filter_instance({'deleted_at__isnull': True, 'candidate_exam_id': id})

    def get(self, request, id, *args, **kwargs):
        """ Candidate and Exam details view """
        try:
            filtered_queryset = self.filter_queryset(queryset=self.get_queryset(id))
            serializer = self.serializers_class(filtered_queryset, many=True,
                                                context={"request": request})

            return Response(data=self.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)

        except Candidate.DoesNotExist:
            return Response(ApplicationMessages.CANDIDATE_ID_INCORRECT, status=status.HTTP_400_BAD_REQUEST)


class CandidateQuestionExamTimerAPIView(generics.ListAPIView):
    """ Timer for Exam and Question """
    serializer_class = candidate_serializers.CandidateQuestionsSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsCandidate,)
    can_model = CandidateAnswer

    def get_queryset(self, question_id=None, exam_id=None):
        """ will returned question and candidate exam """
        question = QuestionClone.get_instance({'id': question_id})
        return self.can_model.filter_instance({'deleted_at__isnull': True,
                                               "question": question, "candidate_exam_id__exam": exam_id, })

    def patch(self, request, *args, **kwargs):
        """ Fetch question according the question_id,
               and their all the answers from the QuestionAnswer Table."""
        question_id = kwargs.get('question_id', None)
        exam_id = kwargs.get('exam_id', None)
        filtered_queryset = self.filter_queryset(
            queryset=self.get_queryset(question_id=question_id, exam_id=exam_id)).first()
        # raise Exception(filtered_queryset)
        question_timer = candidate_services.update_question_timer_tick(filtered_queryset)
        exam_timer = candidate_services.update_exam_timer_tick(filtered_queryset)
        return Response({'question_timer': question_timer, 'exam_timer': exam_timer}, status=status.HTTP_200_OK)


class CandidateQuestionAPIView(generics.ListAPIView):
    """ loading the questions to the candidate"""
    serializer_class = clone_serializers.CandidateQuestionsSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsCandidate,)
    question_answer = QuestionAnswerClone

    def get_queryset(self, question_id=None, exam_id=None):
        """ will returned question and their answers according question_id """
        return self.question_answer.filter_instance({'deleted_at__isnull': True,
                                                     "question": question_id, })

    def get(self, request, *args, **kwargs):
        """ Fetch question according the question_id,
               and their all the answers from the QuestionAnswerClone Table."""
        question_id = kwargs.get('question_id', None)
        exam_id = kwargs.get('exam_id', None)
        candidate = Candidate.get_instance({"user__id": request.user.id})
        filtered_queryset = self.filter_queryset(queryset=self.get_queryset(question_id=question_id, exam_id=exam_id))
        serializer = self.serializer_class(filtered_queryset, context={"exam_id": exam_id, "question_id": question_id,
                                                                       "candidate_id": candidate.id})

        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        """ Fetch question according the question_id,
               and their all the answers from the QuestionAnswerClone Table."""
        exam_id = kwargs.get('exam_id', None)
        question_id = kwargs.get('question_id', None)
        exam = CandidateExam.get_instance({'exam_id': exam_id, 'candidate__user_id': request.user.id})

        if exam:
            exam.disqualify_count += 1
            exam.save()
        return Response(True, status=status.HTTP_200_OK)


class CandidateExamQuestionListAPIView(generics.ListAPIView):
    """ Get Exam Questions list based on Exam_id """
    serializer_class = clone_serializers.CandidateExamQuestionsListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer | permissions.IsCandidate,)
    exam_question_model = ExamQuestionClone

    def get_queryset(self, exam_id, candidate_exam):
        """ will returned Exam Questions list"""
        return self.exam_question_model.get_instance({'exam': exam_id, 'candidate_exam': candidate_exam})

    def get(self, request, exam_id, candidate_exam, *args, **kwargs):
        filtered_queryset = (self.filter_queryset(queryset=self.get_queryset(exam_id, candidate_exam)))
        serializer = self.serializer_class(filtered_queryset, context={"user_id": request.user.id})
        return Response(serializer.data, status=status.HTTP_200_OK)


class ExamQuestionListAPIView(generics.ListAPIView):
    """ Get Exam Questions list based on Exam_id """
    serializer_class = candidate_serializers.CandidateExamQuestionsListSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer | permissions.IsCandidate,)
    exam_question_model = exam_models.ExamQuestion

    def get_queryset(self, exam_id):
        """ will returned Exam Questions list"""
        return self.exam_question_model.get_instance({'exam': exam_id})

    def get(self, request, exam_id, *args, **kwargs):
        filtered_queryset = (self.filter_queryset(queryset=self.get_queryset(exam_id)))
        serializer = self.serializer_class(filtered_queryset, context={"user_id": request.user.id})
        return Response(serializer.data, status=status.HTTP_200_OK)


class CandidateExamIDAPIView(generics.ListAPIView):
    """ Get CandidateExam id based on Candidate id and Exam id """
    serializer_class = candidate_serializers.CandidateExamSerializer
    parser_classes = (parsers.JSONParser,)
    permission_classes = (permissions.IsCandidate,)
    candidate_model = candidate_models.CandidateExam

    def get_queryset(self, candidate_id=None, exam_id=None):
        """ will returned Candidate Exam Details"""
        return self.candidate_model.filter_instance({'candidate__id': candidate_id, 'exam__id': exam_id})

    def get(self, request, *args, **kwargs):
        candidate_id = kwargs.get('candidate_id', None)
        exam_id = kwargs.get('exam_id', None)
        filtered_queryset = self.filter_queryset(
            queryset=self.get_queryset(candidate_id=candidate_id, exam_id=exam_id, ))
        serializer = self.serializer_class(filtered_queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ResendExamLinkAPIView(views.APIView):
    """Resend the link of Candidate Exam """
    permission_classes = (permissions.IsSuperAdmin | permissions.IsExaminer,)
    candidate_exam_model = candidate_models.CandidateExam

    def post(self, request, *agrs, **kwargs):
        flag = True
        candidate_exam = CandidateExam.get_instance(request.data)
        attempt_level = CandidateExam.filter_instance({'candidate': candidate_exam.candidate.id,
                                                       'exam': candidate_exam.exam.id}).count()
        if attempt_level < 2:
            flag = False
        if candidate_exam:
            BASE_URL = settings.FRONTEND_BASE_URL
            mail.InviteCandidateMail({'BASE_URL': BASE_URL, 'user': candidate_exam.candidate.user.full_name,
                                      'exam_link': candidate_exam.exam_link,
                                      'expiry_time': candidate_exam.expiry_time,
                                      'email': candidate_exam.candidate.user.email,
                                      'job': candidate_exam.candidate.job.job_title,
                                      'attempt_level': attempt_level - 1,
                                      'flag': flag
                                      })
            return Response(ApplicationMessages.EMAIL_SENT, status=status.HTTP_200_OK)
        else:
            return Response(ApplicationMessages.INVITE_NOT_SENT, status=status.HTTP_400_BAD_REQUEST)
