from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from apps.candidate.models import Candidate, CandidateAnswer, CandidateExam, CandidateExamStatus
from apps.user.serializers import UserSerializer, SessionSerializer
from apps.user.models import User, Department, Role
from apps.exam.models import ExamQuestion, Exam
from apps.job.models import Job
from apps.question.models import QuestionAnswer, Question
from apps.question.serializers import QuestionAnswerListSerializer, CandidateAnswerDetailsSerializer, \
    FilterByQuestionSerializer, QuestionAnswerResultSerializer
from apps.candidate import services as candidate_services
from apps.exam.serializers import ExamListSerializer
from apps.candidate import push_notifications
from common.constants import ApplicationMessages, Constants
from nhp import settings
from common import mail
from apps.clone import models as clone_models, serailizers as clone_serializers


class CandidateLoginSerializer(serializers.Serializer):
    """ candidate login serializer"""
    candidate_id = serializers.CharField(required=True)
    device = SessionSerializer(write_only=True, required=False)

    def validate(self, attrs):
        """ Will check candidate user id i.e (user id) of the particular candidate """
        attrs["candidate_id"] = attrs.get("candidate_id")
        return attrs


class CandidateExamStatusSerializer(serializers.ModelSerializer):
    """ CandidateExamStatus Serializer """

    class Meta:
        model = CandidateExamStatus
        fields = '__all__'

    def create(self, validated_data):
        """ Create a CandidateExamStatus data """

        candidate_status = CandidateExamStatus.create_instance(validated_data)

        # Calling of notifications
        sender = candidate_status.candidate_exam.candidate.user.id
        push_notifications.candidate_activities(candidate_exam_instance=validated_data['candidate_exam'], sender=sender,
                                                notification_type=str(candidate_status.exam_status.status))

        if candidate_status.exam_status.status == 'EXAM_SUBMITTED':
            candidate_services.detect_correct_answer(validated_data['candidate_exam'])

        if candidate_status.exam_status.status == 'PASSED' or candidate_status.exam_status.status == 'FAILED':
            can_ex_stat = CandidateExamStatus.get_instance({'candidate_exam': candidate_status.candidate_exam,
                                                            'exam_status__status': Constants.CANDIDATE_EXAM_SUBMIT})
            if can_ex_stat:
                can_ex_stat.deleted_at = timezone.now()
                can_ex_stat.save()

        return candidate_status


class CandidateCreateSerializer(serializers.ModelSerializer):
    """ Candidate Create Serializer"""
    candidate_user_id = serializers.CharField(required=False, source="user.id")
    email = serializers.EmailField(required=True, source="user.email")
    job = serializers.UUIDField(required=True)
    examiner = serializers.UUIDField(required=True)
    full_name = serializers.CharField(required=True, source="user.full_name")
    phone_number = serializers.CharField(required=False, source='user.phone_number')
    role = serializers.CharField(required=False, source="user.role_id")

    class Meta:
        model = Candidate
        exclude = ("user",)

    def create(self, validated_data):
        """ Create a Candidate data """
        user_data = validated_data.pop("user")
        existing_data = User.filter_instance({'email': user_data['email']})
        if existing_data.exists():
            raise ValidationError(ApplicationMessages.EMAIL_ALREADY_EXIST)
        user = User.create_instance(user_data)
        user.role = Role.get_instance({'name': 'CANDIDATE'})
        user.save()
        validated_data['user'] = user

        validated_data['job'] = Job.get_instance({"id": validated_data['job']})
        validated_data['examiner'] = User.get_instance({"id": validated_data['examiner']})

        return Candidate.create_instance(validated_data)

    def update(self, instance, validated_data):
        """ Update a Candidate data """
        if validated_data.get("job", None):
            validated_data["job"] = Job.get_instance({"id": validated_data.get("job")})

        if validated_data.get("examiner", None):
            validated_data["examiner"] = User.get_instance({"id": validated_data.get("examiner")})

        user_data = validated_data.pop("user")
        user = instance.user
        # raise Exception(user)
        for attr, value in user_data.items():
            setattr(user, attr, value)
        user.save()

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance


class CandidateAnswerScriptSerializer(serializers.ModelSerializer):
    """ Candidate Result Serializer"""
    question = QuestionAnswerResultSerializer(read_only=True)

    class Meta:
        model = CandidateAnswer
        fields = '__all__'


class CandidateListExamSerializer(serializers.ModelSerializer):
    """ CandidateExamStatus List Serializer for candidate list of a particular exam """
    exam = serializers.SerializerMethodField(read_only=True)
    candidate = serializers.SerializerMethodField(read_only=True)
    exam_link = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CandidateExam
        fields = '__all__'

    def get_exam(self, obj):
        return obj[0].exam.exam_name if obj else []

    def get_exam_link(self, obj):
        return None

    def get_candidate(self, obj):
        # question = CandidateAnswer.filter_instance({'candidate_exam_id': obj[0].id})[0]
        if obj:
            request = self.context.get("request")
            status = request.GET.get('status', None)
            percent = request.GET.get('percent', None)
            filter_by_status = ['EXAM_SUBMITTED', 'FAILED', 'PASSED', 'DISQUALIFY']

            if status:
                filter_by_status = [status]

            candidate_status_list = CandidateExamStatus.objects.filter(
                candidate_exam__candidate__deleted_at__isnull=True,
                candidate_exam__exam_id=obj[0].exam.id,
                exam_status__status__in=filter_by_status).order_by('-created_at')

            can = set()
            candidate_list = []

            for candidate_status in candidate_status_list:

                if candidate_status.candidate_exam.candidate.id not in can:
                    can.add(candidate_status.candidate_exam.candidate.id)
                    candidate_percentage = candidate_services.percentage_correct_answer(
                        candidate_status.candidate_exam.candidate.id, obj[0].exam.id)

                    candidate_list.append({
                        'candidate_id': candidate_status.candidate_exam.candidate.id,
                        'candidate_name': candidate_status.candidate_exam.candidate.user.full_name,
                        'exam_status': {
                            'id': candidate_status.exam_status.id,
                            'status': candidate_status.exam_status.status
                        },
                        'percent': candidate_percentage,
                        'total_question': candidate_services.total_number_question(obj[0].exam.id),
                        'total_correct_answer': candidate_services.count_correct_answer(
                            candidate_status.candidate_exam.candidate.id, obj[0].exam.id),
                        'questions_attempted': candidate_services.attempted_question(
                            candidate_status.candidate_exam.candidate.id,
                            obj[0].exam.id,
                        ),
                        'exam_link': candidate_status.candidate_exam.exam_link,
                        'attempted_date': candidate_status.created_at
                    })

                # Filtering Candidates based on Percentage Param
                if percent is not None and float(candidate_percentage) <= float(percent):
                    candidate_list.pop()

            return candidate_list if obj else []

        return []


class CandidateExamSerializer(serializers.ModelSerializer):
    """ Candidate Exam Serializer"""

    class Meta:
        model = CandidateExam
        fields = "__all__"

    def create(self, validated_data):
        flag = True
        validated_data['expiry_time'] = timezone.now() + timezone.timedelta(hours=Constants.EMAIL_LINK_EXPIRY)
        candidate_exam = CandidateExam.create_instance(validated_data)
        candidate_exam.exam_remaining_time = candidate_services.update_candidate_exam_timer(candidate_exam)
        candidate_exam.save()
        attempt_level = CandidateExam.filter_instance({'candidate': candidate_exam.candidate.id,
                                                       'exam': candidate_exam.exam.id}).count()
        exam_question = ExamQuestion.objects.get(exam=candidate_exam.exam.id)
        question_list = []
        if exam_question:
            for question in exam_question.question.all():
                question_data = {
                    "created_at": question.created_at, "deleted_at": question.deleted_at,
                    "question_name": question.question_name, "department": question.department,
                    "experience_level": question.experience_level, "question_status": question.question_status,
                    "time_duration": question.time_duration, "candidate_exam": candidate_exam
                }
                question_clone = clone_models.QuestionClone.objects.create(**question_data)
                question_list.append(question_clone.id)
                question_answer = QuestionAnswer.objects.filter(question__id=question.id)
                for answer in question_answer:
                    answers_data = {
                        "created_at": answer.created_at, "deleted_at": answer.deleted_at, "question": question_clone,
                        "answer_text": answer.answer_text, "is_correct": answer.is_correct,
                    }
                    clone_models.QuestionAnswerClone.objects.create(**answers_data)
            exam_question_clone = clone_models.ExamQuestionClone.create_instances({'exam': candidate_exam.exam,
                                                                                  'candidate_exam': candidate_exam})
            for que in question_list:
                exam_question_clone.question.add(que)

        if attempt_level < 2:
            flag = False
        BASE_URL = settings.FRONTEND_BASE_URL
        mail.InviteCandidateMail({'BASE_URL': BASE_URL, 'user': candidate_exam.candidate.user.full_name,
                                  'job': candidate_exam.candidate.job.job_title,
                                  'exam_link': candidate_exam.exam_link,
                                  'expiry_time': candidate_exam.expiry_time,
                                  'email': candidate_exam.candidate.user.email,
                                  'attempt_level': attempt_level - 1,
                                  'flag': flag})
        return candidate_exam


class CandidateResultSerializer(serializers.ModelSerializer):
    """ Candidate Result Serializer"""
    candidate = serializers.SerializerMethodField(read_only=True)
    exam = serializers.SerializerMethodField(read_only=True)
    correct_answer = serializers.SerializerMethodField(read_only=True)
    attempted_question = serializers.SerializerMethodField(read_only=True)
    unattempted_question = serializers.SerializerMethodField(read_only=True)
    total_question = serializers.SerializerMethodField(read_only=True)
    wrong_answer = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CandidateExam
        fields = "__all__"

    def get_candidate(self, obj):
        return CandidateListSerializer(obj.candidate).data if obj.candidate else {}

    def get_exam(self, obj):
        return ExamListSerializer(obj.exam).data if obj.exam else {}

    def get_correct_answer(self, obj):
        return candidate_services.count_correct_answer(obj.candidate.id, obj.exam) if obj.candidate else {}

    def get_attempted_question(self, obj):
        return candidate_services.attempted_question(obj.candidate.id, obj.exam.id) if obj.candidate else {}

    def get_unattempted_question(self, obj):
        return candidate_services.unattempted_question(obj.candidate.id, obj.exam.id) if obj.candidate else {}

    def get_total_question(self, obj):
        return candidate_services.total_question(obj.candidate.id, obj.exam.id) if obj.candidate else {}

    def get_wrong_answer(self, obj):
        return candidate_services.count_wrong_answer(obj.candidate.id, obj.exam.id) if obj.candidate else {}


class CandidateSpecificExamAnswersSerializer(serializers.ModelSerializer):
    """ CandidateAnswer for a specific exam serializer """

    class Meta:
        model = CandidateAnswer
        fields = ['candidate_exam_id', 'question', 'answer_choice']

    def create(self, validated_data):
        """ Create a CandidateAnswer data """
        candidate_answer = CandidateAnswer.update_or_create_instance(validated_data)
        return candidate_answer


class CandidateExamStatusUpdateSerializer(serializers.ModelSerializer):
    """ CandidateExamStatus Serializer """

    class Meta:
        model = CandidateExamStatus
        fields = '__all__'

    def update(self, instance, validated_data):
        # This function is deprecated using create function only
        pass

        """ Update a CandidateExamStatus data """
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        if instance.exam_status.status == 'EXAM_SUBMITTED':
            candidate_services.detect_correct_answer(instance.candidate_exam.exam.id)
        return instance


class CandidateQuestionsSerializer(serializers.ModelSerializer):
    """ Candidate Questions Serializer"""
    exam = serializers.SerializerMethodField(read_only=True)
    answers = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = QuestionAnswer
        fields = ['exam', 'answers', ]

    def get_exam(self, obj):
        question_id = self.context.get("question_id")
        exam_id = self.context.get("exam_id")
        candidate_id = self.context.get("candidate_id")

        candidate_exam = CandidateExam.get_instance({'exam__id': exam_id, 'candidate__id': candidate_id})
        # raise Exception(candidate_exam)

        CandidateAnswer.create_or_filter({'question__id': question_id, 'candidate_exam_id': candidate_exam})

        question = Question.filter_instance({'pk': question_id}).first()  # TODO

        response = dict()
        response['exam_name'] = candidate_exam.exam.exam_name
        response['exam_level'] = candidate_exam.exam.experience_level
        response['exam_details'] = CandidateExamSerializer(candidate_exam).data
        response['question'] = FilterByQuestionSerializer(question).data
        response['exam_remaining_time'] = candidate_services.update_candidate_exam_timer(candidate_exam)
        response['question_remaining_time'] = candidate_services.question_timer(question_id, exam_id)
        response['disqualify_count'] = candidate_exam.disqualify_count

        return response

    def get_answers(self, obj):
        question_id = self.context.get("question_id")
        exam_id = self.context.get("exam_id")
        candidate_id = self.context.get("candidate_id")
        candidate_exam = CandidateExam.get_instance({'exam__id': exam_id, 'candidate__id': candidate_id})
        return CandidateAnswerDetailsSerializer(obj, context={'candidate_exam': candidate_exam}, many=True).data

    def update(self, instance, validated_data):
        """ updating disqualify_count"""

        return instance


class CandidateExamQuestionsListSerializer(serializers.ModelSerializer):
    """ ExamQuestion serializer"""

    question = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ExamQuestion
        fields = ['question', ]

    def get_question(self, obj):
        user_id = self.context.get("user_id")
        attempted_questions = candidate_services.attempted_question_list(obj.exam_id, user_id)

        response = []

        for q in obj.question.all():
            exist = attempted_questions.filter(question=q.id).exists()
            total_answers = QuestionAnswer.filter_instance({'question': q, 'is_correct': True}).count()
            multiple_answers = True if total_answers > 1 else False
            response.append({"question_id": q.id, "is_attempted": exist, "multiple_answers": multiple_answers})
            # response.append({"question_id": q.id, "is_attempted": exist})

        return response


class CandidateResultListSerializer(serializers.ModelSerializer):
    """ CandidateAnswer List Serializer """
    candidate = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CandidateExamStatus
        exclude = ('created_at', 'deleted_at', 'updated_at', 'candidate_exam', 'exam_status', 'id')

    def get_candidate(self, obj):
        candidate_list = []
        if obj:
            request = self.context.get("request")
            percent = request.GET.get('percentage', None)
            level = request.GET.get('attempt_level', None)
            examiner = request.GET.get('examiner', None)

            candidate_exam_list = CandidateExamStatus.filter_instance(
                {'exam_status__status': Constants.CANDIDATE_EXAM_SUBMIT, 'deleted_at__isnull': True,
                 'candidate_exam__id': obj.candidate_exam.id})

            for candidate_exam in candidate_exam_list:
                candidate_exam_lists = CandidateExam.filter_instance({'deleted_at__isnull': True,
                                                                      'exam__id': obj.candidate_exam.exam.id,
                                                                      'candidate__id': obj.candidate_exam.candidate.id})

                count = CandidateExam.filter_instance({'candidate': obj.candidate_exam.candidate.id}).count()
                # if count == 0:
                #     count = 1
                candidate_percentage = candidate_services.percentage_correct_answer(
                    candidate_exam.candidate_exam.candidate.id,
                    candidate_exam.candidate_exam.exam.id)
                attempt_level = str('L' + str(count))
                examiner_id = candidate_exam.candidate_exam.candidate.examiner.id
                data = {
                    'candidate_id': candidate_exam.candidate_exam.candidate.id,
                    'candidate_full_name': candidate_exam.candidate_exam.candidate.user.full_name,
                    'examiner': examiner_id,
                    'exam_name': candidate_exam.candidate_exam.exam.exam_name,
                    'exam_id': candidate_exam.candidate_exam.exam.id,
                    'department_id': candidate_exam.candidate_exam.candidate.examiner.department.id,
                    'department_name': candidate_exam.candidate_exam.candidate.examiner.department.name,
                    'experience_level': candidate_exam.candidate_exam.exam.experience_level,
                    'total_questions': candidate_services.total_question(candidate_exam.candidate_exam.candidate.id,
                                                                         candidate_exam.candidate_exam.exam.id),
                    'percentage': candidate_percentage,
                    'attempt_level': attempt_level
                }
                if percent and level and examiner:
                    if float(candidate_percentage) >= float(percent) and attempt_level in level and str(
                            examiner_id) in examiner:
                        candidate_list.append(data)
                    else:
                        continue
                elif percent and level:
                    if float(candidate_percentage) >= float(percent) and attempt_level in level:
                        candidate_list.append(data)
                    else:
                        continue
                elif percent and examiner:
                    if float(candidate_percentage) >= float(percent) and str(examiner_id) in examiner:
                        candidate_list.append(data)
                    else:
                        continue
                elif level and examiner:
                    if attempt_level in level and str(examiner_id) in examiner:
                        candidate_list.append(data)
                    else:
                        continue
                elif percent:
                    if float(candidate_percentage) >= float(percent):
                        candidate_list.append(data)
                    else:
                        continue
                elif level:
                    if attempt_level in level:
                        candidate_list.append(data)
                    else:
                        continue
                elif examiner:
                    if str(examiner_id) in examiner:
                        candidate_list.append(data)
                    else:
                        continue
                else:
                    candidate_list.append(data)
        return candidate_list


class CandidateListSerializer(serializers.ModelSerializer):
    """ Candidate List Serializer"""
    user = serializers.SerializerMethodField(read_only=True)
    job = serializers.SerializerMethodField(read_only=True)
    examiner = serializers.SerializerMethodField(read_only=True)
    exam = serializers.SerializerMethodField(read_only=True)
    exam_status = serializers.SerializerMethodField(read_only=True)
    latest_status = serializers.SerializerMethodField(read_only=True)
    attempt_level = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Candidate
        fields = '__all__'

    def get_user(self, obj):

        obj_dict = {
            'candidate_user_id': obj.user.id,
            'full_name': obj.user.full_name,
            'email': obj.user.email,
            'phone_number': obj.user.phone_number,
            'created_at': obj.user.created_at

        }
        return obj_dict if obj.user else {}

    def get_job(self, obj):
        return obj.job.job_title if obj.job else []

    def get_examiner(self, obj):
        if obj.examiner:
            obj_dict = {
                'examiner_name': obj.examiner.full_name,
                'examiner_id': obj.examiner.id,
                'examiner_department': obj.examiner.department.name,
                'examiner_department_id': obj.examiner.department.id,
            }
            return obj_dict

    def get_exam(self, obj):

        candidate_obj = CandidateExam.filter_instance({'candidate': obj}).order_by('created_at').last()
        if candidate_obj is None:
            return []
        return {'exam_id': candidate_obj.exam.id, 'exam_level': candidate_obj.exam.experience_level} if obj else []

    def get_latest_status(self, obj):

        candidate_obj = CandidateExamStatus.filter_instance({'candidate_exam__candidate': obj})
        if candidate_obj.exists():
            candidate_obj = candidate_obj.latest('created_at')
        else:
            return {}
        return {'id': candidate_obj.exam_status.id, 'candidate_status': candidate_obj.exam_status.status} if obj else []

    def get_exam_status(self, obj):
        can_exam = CandidateExam.filter_instance({'candidate': obj})
        exam_list = []

        for key, exam in enumerate(can_exam):

            exam_status_obj = CandidateExamStatus.filter_instance({'candidate_exam': exam}).order_by('created_at')

            my_list = []

            for status in exam_status_obj:

                exam_status = {
                    'id': status.exam_status.id,
                    'status': status.exam_status.status,
                    'created_at': status.created_at,
                    'deleted_at': status.deleted_at
                }

                if status.exam_status.status == 'LINK_SENT':
                    can_exam_status = CandidateExamStatus.get_instance({'id': status.id})
                    exam_details = CandidateExam.get_instance({'id': can_exam_status.candidate_exam.id})
                    exam_status['expiry_time'] = exam_details.expiry_time
                    exam_status['exam_link'] = exam_details.exam_link
                    exam_status['exam_id'] = exam_details.exam.id

                my_list.append(exam_status)
            exam_list.append({'L' + str(key + 1): my_list})

        return [status for status in exam_list] if can_exam else []

    def get_attempt_level(self, obj):
        can = CandidateExam.filter_instance({'candidate': obj, }).count()
        if can == 0:
            can = 1
        return "L" + str(can)
