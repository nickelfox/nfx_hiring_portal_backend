from django.contrib import admin
from .models import Candidate
from .models import CandidateExam, CandidateExamStatus, ExamStatus, CandidateAnswer
# Register your models here.
admin.site.register(Candidate)
admin.site.register(CandidateExam)
admin.site.register(CandidateExamStatus)
admin.site.register(ExamStatus)
admin.site.register(CandidateAnswer)