from apps.user import models as user_model
from apps.candidate import models as candidate_model
from common.constants import Constants
from notifications import tasks
from django.db.models import Q


def candidate_activities(candidate_exam_instance, sender, notification_type):
    """
    Receiver: Admin, Sub-Admin
    When: Candidate Exam Status Change
    """

    candidate_exam_details = get_candidate_exam_status_details(candidate_exam_instance.id)

    if notification_type in (Constants.CANDIDATE_EXAM_DISQUALIFIED,
                             Constants.CANDIDATE_EXAM_LINK_EXPIRE,
                             Constants.CANDIDATE_EXAM_ATTEMPTED,
                             Constants.CANDIDATE_EXAM_SUBMIT):
        receiver = list(get_admin_subadmin_list())
    else:
        receiver = ""

    # raise Exception(receiver)
    notification_args = {
        'candidate_id': str(candidate_exam_details.candidate.id),
        'candidate_name': str(candidate_exam_details.candidate.user.full_name),
        'exam_id': str(candidate_exam_details.exam.id),
        'exam_name': str(candidate_exam_details.exam.exam_name),
        'exam_link': str(candidate_exam_details.exam_link),
        'link_expiry_time': str(candidate_exam_details.expiry_time),
    }

    if receiver:
        tasks.push_notification(notification_type, sender, receiver, notification_args)


def get_admin_subadmin_list():
    return user_model.User.objects.filter(Q(role__name=Constants.SUBADMIN) | Q(role__name=Constants.ADMIN)).values_list("id", flat=True)


def get_candidate_exam_status_details(candidate_exam_id):
    return candidate_model.CandidateExam.get_instance({'id': candidate_exam_id})
