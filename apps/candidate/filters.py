from rest_framework.filters import BaseFilterBackend

from apps import exam
from apps.candidate.models import CandidateExamStatus, CandidateExam
from apps.exam import models as exam_models
from django.db.models import Q

from apps.exam.models import Exam


# class CandidateFilterByExamDept(BaseFilterBackend):
# 	"""Filter a candidate by exam department"""
#
# 	param = "department"
#
# 	def filter_queryset(self, request, queryset, view):
# 		param = request.query_params.getlist(self.param, None)
# 		if param:
# 			return queryset.filter(department__name__in=param)
# 		return queryset


class ExamDepartmentFilterByID(BaseFilterBackend):
	"""Filter department by its ID"""

	param = "department"

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)
		if param:
			return queryset.filter(department__in=param)
		return queryset


class CandidateFilterByDate(BaseFilterBackend):
	"""Filter a candidate by  created_at date range"""

	param_one = 'from'
	param_two = 'to'

	def filter_queryset(self, request, queryset, view):
		param_one = request.query_params.get(self.param_one, None)
		param_two = request.query_params.get(self.param_two, None)
		if param_one and param_two:
			return queryset.filter(created_at__date__range=[param_one, param_two])
		return queryset


class CandidateFilterByExamLevel(BaseFilterBackend):
	"""Filter a candidate by experience level"""

	param = 'experience_level'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)
		if param:
			return queryset.filter(experience_level__in=param)
		return queryset


class CandidateFilterByExamStatus(BaseFilterBackend):
	"""Filter a candidate by Exam Status"""

	param = 'candidate_status'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)

		if param:
			return queryset.filter(candidateexam__candidate_exam_status__exam_status__status__in=param)

		return queryset


class CandidateFilterBySpecificExamStatus(BaseFilterBackend):
	"""Filter a candidate by Exam Status"""

	param = 'status'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.get(self.param, None)

		if param:
			return queryset.filter(candidate_exam__status=param)
		return queryset


class CandidateFilterByDepartmentName(BaseFilterBackend):
	"""Filter a candidate by department ID"""

	param = 'department'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)
		if param:
			return queryset.filter(user__department__in=param)

		return queryset


class CandidateFilterExaminer(BaseFilterBackend):
	"""Filter a candidate by examiner ID"""

	param = 'examiner'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)
		if param:
			return queryset.filter(examiner__id__in=param)
		return queryset


class CandidateFilterExaminerDepartment(BaseFilterBackend):
	"""Filter a candidate by examiner department"""

	param = 'department'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)
		if param:
			return queryset.filter(candidate_exam__candidate__examiner__department__id__in=param)
		return queryset


class CandidateFilterByLevel(BaseFilterBackend):
	"""Filter a candidate by experience level"""

	param = 'experience_level'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)
		if param:
			return queryset.filter(candidate_exam__exam__experience_level__in=param)
		return queryset


class CandidateFilterDepartment(BaseFilterBackend):
	"""Filter a candidate by examiner department"""

	param = 'department'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.getlist(self.param, None)
		if param:
			return queryset.filter(examiner__department__id__in=param)
		return queryset


class FilterByExamLevel(BaseFilterBackend):
	"""Filter a candidate by experience level"""

	param = 'exam_level'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.get(self.param, None)
		if param:
			# print(queryset.filter(candidateexam__exam__experience_level__iexact="internship"))
			return queryset.filter(candidateexam__exam__experience_level__iexact=param.lower())
		return queryset


class FilterByAttemptLevel(BaseFilterBackend):
	"""Filter a candidate by Attempt Level"""

	param = 'attempt_level'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.get(self.param, None)
		if param:
			return queryset.filter(attempt_level__in=param)
		return queryset


class FilterByHire(BaseFilterBackend):
	"""Filter a candidate by Hire/Reject"""

	param = 'is_hired'

	def filter_queryset(self, request, queryset, view):
		param = request.query_params.get(self.param, None)
		if param:
			# print(queryset.filter(candidateexam__exam__experience_level__iexact="internship"))
			return queryset.filter(candidateexam__candidate__is_hired=param)
		return queryset
