from apps.candidate import models as candidate_models
from apps.exam import models as exam_models
from apps.candidate.models import CandidateExam, CandidateAnswer, CandidateExamStatus
from django.utils import timezone
from apps.question import models as question_models
from common.constants import Constants
from apps.clone import models as clone_models


def total_number_question(exam_id=None):
    """
    Counts the total number of questions in an exam
    """
    total_questions = 0
    all_questions = exam_models.ExamQuestion.filter_instance({"exam": exam_id, "question__question_status": "PUBLISH"}).first()

    if all_questions:
        total_questions = all_questions.question.all().count()

    return total_questions


def total_question(candidate_id=None, exam_id=None):
    """
    Counts the total number of questions in an exam
    """

    total_questions = candidate_models.CandidateAnswer.objects.filter(
        candidate_exam_id__candidate=candidate_id,
        candidate_exam_id__exam=exam_id)
    total_question = total_questions.count()
    return total_question


def percentage_correct_answer(candidate_id=None, exam_id=None):
    """
    Return the percentage of correct answer
    """
    res = (count_correct_answer(candidate_id, exam_id))
    total_questions = total_question(candidate_id, exam_id)
    if res != 0 and total_questions != 0:
        percentage_answer = float((res/total_questions) * 100)
        return round(percentage_answer, 2)
    return 0


def count_correct_answer(candidate_id=None, exam_id=None):
    """
    This service call is used to detect the number of correct answers given by a candidate
    """
    count = candidate_models.CandidateAnswer.objects.filter(
        candidate_exam_id__candidate=candidate_id,
        candidate_exam_id__exam=exam_id,
        answer_status='CORRECT').count()
    return count


def count_wrong_answer(candidate_id=None, exam_id=None):
    """
    This service call is used to detect the number of wong answers given by a candidate
    """
    count = candidate_models.CandidateAnswer.objects.filter(
        candidate_exam_id__candidate=candidate_id,
        candidate_exam_id__exam=exam_id,
        answer_status='WRONG').count()
    return count


def detect_correct_answer(can_exam_id=None):
    """
    This service will detect the correct answer and store the status accordingly
    """
    candidate_answers = CandidateAnswer.filter_instance({'candidate_exam_id': can_exam_id})

    question_ids = candidate_answers.values_list('question__id', flat=True)

    question_answers = clone_models.QuestionAnswerClone.filter_instance({'question__in': question_ids})

    for ca in candidate_answers:

        attempted_answers = ca.answer_choice.all()
        exam_correct_answers = question_answers.filter(question=ca.question, is_correct=True)
        attempted_correct_answers = attempted_answers.filter(is_correct=True)

        if attempted_answers.count() == 0:
            ca.answer_status = 'UNATTEMPTED'
        elif attempted_correct_answers.count() != exam_correct_answers.count():
            ca.answer_status = 'WRONG'
        elif attempted_correct_answers.count() == exam_correct_answers.count():
            ca.answer_status = 'CORRECT'

        ca.save()


def attempted_question(candidate_id=None, exam_id=None):
    """
    Counts the attempted questions by a candidate in an exam
    """

    total_questions = candidate_models.CandidateAnswer.objects.filter(
        candidate_exam_id__candidate=candidate_id,
        candidate_exam_id__exam=exam_id)
    attempted_questions = total_questions.exclude(answer_status='UNATTEMPTED').count()
    return attempted_questions


def unattempted_question(candidate_id=None, exam_id=None):
    """
    Counts the unattempted questions by a candidate in an exam
    """

    total_questions = candidate_models.CandidateAnswer.objects.filter(
        candidate_exam_id__candidate=candidate_id,
        candidate_exam_id__exam=exam_id)
    unattempted_questions = total_questions.filter(answer_status='UNATTEMPTED').count()
    return unattempted_questions


def avg_score_exam(exam_id=None):
    """
    Counts the average score of success in an exam (in percentage)
    """

    total_question = candidate_models.CandidateAnswer.filter_instance({'candidate_exam_id__exam': exam_id}).count()
    correct_answer = candidate_models.CandidateAnswer.filter_instance({'candidate_exam_id__exam': exam_id,
                                                                       'answer_status': 'CORRECT'}).count()

    if total_question == 0:
        avg_percent = 0.0
    else:
        avg_percent = (correct_answer / total_question) * 100

    return round(avg_percent, 2)


def exam_timer(exam_id=None):
    """
    Set the timer for each CandidateExam
    """

    candidate = CandidateExam.objects.filter(exam_id=exam_id).first()
    candidate.exam_remaining_time = candidate.exam.duration
    candidate.save()
    return candidate.exam_remaining_time


def question_timer(question_id, exam_id):
    """
    Set the timer for each Each Question
    """
    can_ans = CandidateAnswer.create_or_filter({'question_id': question_id, 'candidate_exam_id__exam_id': exam_id})

    if not can_ans.question_remaining_time:
        can_ans.question_remaining_time = can_ans.question.time_duration
        can_ans.save()
    can_ans.question_remaining_time = can_ans.question.time_duration
    can_ans.save()
    return can_ans.question_remaining_time


def update_exam_timer_tick(instance=None):
    """ Deduct 15 from the exam time every 15 seconds when u receive EXAM_ATTEMPTED flag"""
    ans = candidate_models.CandidateExamStatus.filter_instance(
        {'candidate_exam__exam': instance.candidate_exam_id.exam,

         'candidate_exam__candidate': instance.candidate_exam_id.candidate}).first()

    if ans.exam_status.status == "EXAM_ATTEMPTED":
        current_remaining_time = instance.candidate_exam_id.exam_remaining_time - 15
        instance.candidate_exam_id.exam_remaining_time = 0 if current_remaining_time <= 0 else current_remaining_time
        instance.candidate_exam_id.save()
    return instance.candidate_exam_id.exam_remaining_time


def update_question_timer_tick(candidate_question=None):
    """ Deduct 15 from the question time every 15 seconds  """
    instance = candidate_question

    ans = candidate_models.CandidateExamStatus.filter_instance(
        {'candidate_exam__exam': instance.candidate_exam_id.exam,

         'candidate_exam__candidate': instance.candidate_exam_id.candidate}).first()

    if ans.exam_status.status == "EXAM_ATTEMPTED":
        current_remaining_time = candidate_question.question_remaining_time - 15
        candidate_question.question_remaining_time = 0 if current_remaining_time <= 0 else current_remaining_time
        candidate_question.save()

        return candidate_question.question_remaining_time


def update_candidate_exam_timer(cand_exam):
    """
    This service call is used to initializer the candidate exam timer to the exam duration time
    """
    candidate_exam = candidate_models.CandidateExam.get_instance({'pk': cand_exam.id})
    if not candidate_exam.exam_remaining_time:
        candidate_exam.exam_remaining_time = candidate_exam.exam.duration
        candidate_exam.save()
    return candidate_exam.exam_remaining_time


def is_answer_selected(questions=None, answers=None, candidate_exam=None):
    can_tick = CandidateAnswer.filter_instance(
        {'question__id': questions, 'answer_choice': answers, 'candidate_exam_id': candidate_exam})
    if can_tick:
        return True
    return False


def is_answers_selected(questions=None, answers=None, candidate_exam=None):
    can_tick = CandidateAnswer.filter_instance(
        {'question__id': questions, 'answer_choice': answers})
    if can_tick:
        return True
    return False


def attempted_question_list(exam_id, user_id):
    attempted_list = CandidateAnswer.filter_instance(
        {'candidate_exam_id__exam': exam_id, 'candidate_exam_id__candidate__user': user_id})
    return attempted_list


def candidate_validation(candidate_id=None):
    """
    Service will check whether a
    candidate is disqualified,
    its exam link has expired,
    its exam timer has run out,
    its question timer has expired or not
    """

    expiry_time = 0
    candidate_disqualify = candidate_models.CandidateExamStatus.filter_instance(
        {'candidate_exam__candidate': candidate_id,
         'exam_status__status': 'DISQUALIFY'})
    question_expiry = candidate_models.CandidateAnswer.filter_instance({'candidate_exam_id__candidate': candidate_id,
                                                                        'question_remaining_time': expiry_time})
    exam_expiry = candidate_models.CandidateExam.filter_instance({'candidate': candidate_id,
                                                                  'exam_remaining_time': expiry_time})
    exam_link_expiry = candidate_models.CandidateExam.filter_instance({'candidate': candidate_id,
                                                                       'expiry_time__lte': timezone.now()})

    if candidate_disqualify or question_expiry or exam_expiry or exam_link_expiry:
        return False
    return True


def candidate_exam_attemted_date(candidate_exam_id=None):
    candidate_exam_status = CandidateExamStatus.filter_instance(
        {'candidate_exam__exam_id': candidate_exam_id, 'exam_status__status': Constants.CANDIDATE_EXAM_ATTEMPTED})

    if candidate_exam_status:
        for candidate in candidate_exam_status:
            return candidate.created_at
    return []
