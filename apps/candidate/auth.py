from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework_simplejwt.token_blacklist.models import BlacklistedToken, OutstandingToken
from apps.candidate.models import Candidate
from common import constants
from common import utils
from common.constants import Constants, ApplicationMessages
from django.utils import timezone
from apps.user.models import User
from apps.user import models as user_models


def create_user_session(device, user):
    """get data and create session"""
    try:
        access_token = OutstandingToken.objects.filter(user=user).order_by("-created_at")[0]

        user_session_info = {
            # 'ip': socket.gethostbyname(socket.gethostname()),
            'user': user,
            'token': access_token
        }
        if device:
            user_session_info['device_token'] = device.get("device_token")
            user_session_info['device_type'] = device.get("device_type")
            user_session_info['is_safari'] = device.get("is_safari")
        user_models.Session.create_instance(user_session_info)
        session_response = {
            'device_token': user_session_info['device_token'],
            'device_type': user_session_info['device_type'],
            'is_safari': user_session_info['is_safari'],
        }
        return session_response
    except Exception as ex:
        raise ValidationError(ApplicationMessages.SOMETHING_WENT_WRONG)


class IDAuthManager:
    """
    This class deals with all type of users for Login and SignUp
     Login function will return the token once user get Authenticated and they are static as well
    """
    model = User

    @staticmethod
    def login_user_id(candidate_instance, data):
        """ User login via email and password and return token """
        if not candidate_instance.is_active:
            raise ValidationError(constants.ApplicationMessages.USER_NOT_ACTIVE, status.HTTP_401_UNAUTHORIZED)

        candidate_instance.last_login = timezone.now()
        token = candidate_instance.tokens()
        candidate_instance.save()
        user_session = create_user_session(data.get("device"), candidate_instance)
        response = {
            "message": ApplicationMessages.SUCCESS,
            "tokens": token,
            "email": candidate_instance.email,
            "full_name": candidate_instance.full_name,
            "id": candidate_instance.id,
            "role": candidate_instance.role.name,
            "session": user_session,
        }

        return response



