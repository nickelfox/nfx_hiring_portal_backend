#!/bin/bash
# activate the virutal environment
source ./venv/bin/activate
pip install -r requirements.txt
python manage.py migrate

# collect static filespython manage.py collectstatic --noinput
python manage.py collectstatic --noinput

# migrate and collect static
python3 manage.py loaddata fixtures/roles.json
python3 manage.py loaddata fixtures/departments.json
python3 manage.py loaddata fixtures/users.json
python3 manage.py loaddata fixtures/examstatus.json
python3 manage.py loaddata fixtures/permission.json
