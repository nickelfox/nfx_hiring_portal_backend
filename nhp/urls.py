from django.contrib import admin
from django.urls import path
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('apps.user.urls')),
    path('api/v1/job/', include('apps.job.urls')),
    path('api/v1/question/', include('apps.question.urls')),
    path('api/v1/exam/', include('apps.exam.urls')),
    path('api/v1/candidate/', include('apps.candidate.urls')),
    path('api/v1/department/', include('apps.department.urls')),
    path('api/v1/tag/', include('apps.tag.urls')),
    path('api/v1/qtag/', include('apps.questiontag.urls')),
    path('api/v1/dashboard/', include('apps.dashboard.urls')),
    path('api/v1/notifications/', include('notifications.urls')),
 ]
