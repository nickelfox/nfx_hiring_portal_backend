#!/bin/bash
# activate the virutal environment
source /home/ubuntu/projects/hiring-portal/develop/venv/bin/activate
# gunicorn application server
# Add gunicorn when serve using nginx
# gunicorn --config gomble/gunicorn.py psychscope.wsgi:application
python manage.py runserver 0.0.0.0:8000